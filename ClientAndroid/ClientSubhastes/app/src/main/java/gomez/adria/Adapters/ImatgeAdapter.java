package gomez.adria.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import gomez.adria.clientsubhastes.R;
import gomez.adria.lazyloading.ImageLoader;
import info.infomila.model.Usuari;
import info.infomila.servidor.paquets.DadesSubhasta;

/**
 * Created by Adria on 27/05/2016.
 */
public class ImatgeAdapter extends ArrayAdapter<String> {

    private List<String> mLlistaURLs;
    private ImageLoader imageLoader;

    public ImatgeAdapter(Context context, List<String> objects) {
        super(context, R.layout.list_item_imatge, objects);
        mLlistaURLs = objects;
        imageLoader = new ImageLoader(context);
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parentListView) {

        final LinearLayout llyFila;
        if(convertView != null){
            llyFila = (LinearLayout) convertView;
        }else{
            int idLayoutFila = R.layout.list_item_imatge;
            LayoutInflater factory = LayoutInflater.from(this.getContext());
            llyFila = (LinearLayout) factory.inflate(idLayoutFila,parentListView,false);

        }
        ContenidorLlista holder = (ContenidorLlista) llyFila.getTag();
        if(holder == null){
            ImageView imatge = (ImageView) llyFila.findViewById(R.id.imvImatgeProd);
            holder = new ContenidorLlista(imatge);
        }

        String url = mLlistaURLs.get(position);
        if(url != null){
            imageLoader.DisplayImage(url, holder.getImatge());
        }else{
            holder.getImatge().setImageResource(R.drawable.subastas);
        }

        final ListView listView = (ListView) parentListView;
        llyFila.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkItem(listView, position);
            }
        });

        return llyFila;
    }

    private boolean checkItem(ListView listView, int position) {
        boolean isChecked = listView.isItemChecked(position);
        listView.setItemChecked(position, !isChecked);
        return !isChecked;
    }

    class ContenidorLlista{
        private ImageView imatge;


        public ContenidorLlista(ImageView imatge) {
            this.imatge = imatge;
        }

        public ImageView getImatge() {
            return imatge;
        }

        public void setImatge(ImageView imatge) {
            this.imatge = imatge;
        }
    }
}
