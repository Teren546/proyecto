package gomez.adria.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import gomez.adria.clientsubhastes.R;
import info.infomila.servidor.paquets.InfoOferta;

/**
 * Created by Adria on 27/05/2016.
 */
public class OfertaAdapter extends ArrayAdapter<InfoOferta> {

    private List<InfoOferta> mLlistaOfertes;
    private String aliasMeu;

    public OfertaAdapter(Context context, List<InfoOferta> objects, String alias) {
        super(context, R.layout.list_item_oferta, objects);
        mLlistaOfertes = objects;
        aliasMeu = alias;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parentListView) {

        final LinearLayout llyFila;
        if(convertView != null){
            llyFila = (LinearLayout) convertView;
        }else{
            int idLayoutFila = R.layout.list_item_oferta;
            LayoutInflater factory = LayoutInflater.from(this.getContext());
            llyFila = (LinearLayout) factory.inflate(idLayoutFila,parentListView,false);

        }
        ContenidorLlista holder = (ContenidorLlista) llyFila.getTag();
        if(holder == null){
            TextView importe = (TextView) llyFila.findViewById(R.id.txvimporte);
            TextView alies = (TextView) llyFila.findViewById(R.id.txvaliasofertante);
            holder = new ContenidorLlista(importe,alies);
        }

        InfoOferta of = mLlistaOfertes.get(position);

        holder.getAlies().setText(of.getAliesOfertant().toString());
        holder.getImporte().setText(of.getImporte().toString());

        if(aliasMeu.equals(of.getAliesOfertant())){
            llyFila.setBackgroundColor(Color.YELLOW);
        }else{
            llyFila.setBackgroundColor(Color.TRANSPARENT);
        }

        return llyFila;
    }

    private boolean checkItem(ListView listView, int position) {
        boolean isChecked = listView.isItemChecked(position);
        listView.setItemChecked(position, !isChecked);
        return !isChecked;
    }

    class ContenidorLlista{
        private TextView importe;
        private TextView alies;

        public ContenidorLlista(TextView importe, TextView alies) {
            this.importe = importe;
            this.alies = alies;
        }

        public TextView getImporte() {
            return importe;
        }

        public void setImporte(TextView importe) {
            this.importe = importe;
        }

        public TextView getAlies() {
            return alies;
        }

        public void setAlies(TextView alies) {
            this.alies = alies;
        }
    }
}
