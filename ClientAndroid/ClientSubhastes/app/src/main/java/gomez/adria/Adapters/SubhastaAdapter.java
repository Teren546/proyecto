package gomez.adria.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import gomez.adria.clientsubhastes.R;
import gomez.adria.lazyloading.ImageLoader;
import info.infomila.model.Foto;
import info.infomila.model.Oferta;
import info.infomila.model.ProducteCataleg;
import info.infomila.model.Subhasta;
import info.infomila.model.Usuari;
import info.infomila.servidor.paquets.DadesSubhasta;

/**
 * Created by Adria on 27/05/2016.
 */
public class SubhastaAdapter extends ArrayAdapter<DadesSubhasta> {

    private List<DadesSubhasta> mLlistaSubhastes;
    private Usuari mUsuariApplicacio;
    private ImageLoader imageLoader;

    public SubhastaAdapter(Context context, List<DadesSubhasta> objects, Usuari yo) {
        super(context, R.layout.list_item_subhasta, R.id.txvAliesUltimaOferta, objects);
        mLlistaSubhastes = objects;
        mUsuariApplicacio = yo;
        imageLoader = new ImageLoader(context);
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parentListView) {

        final LinearLayout llyFila;
        if(convertView != null){
            llyFila = (LinearLayout) convertView;
        }else{
            int idLayoutFila = R.layout.list_item_subhasta;
            LayoutInflater factory = LayoutInflater.from(this.getContext());
            llyFila = (LinearLayout) factory.inflate(idLayoutFila,parentListView,false);

        }
        ContenidorLlista holder = (ContenidorLlista) llyFila.getTag();
        if(holder == null){
            ImageView imatge = (ImageView) llyFila.findViewById(R.id.imvImatgeProd);
            TextView nomProducte = (TextView) llyFila.findViewById(R.id.nomProd);
            TextView aliesOfertant = (TextView) llyFila.findViewById(R.id.txvAliesUltimaOferta);
            TextView ultimaOferta = (TextView) llyFila.findViewById(R.id.txvUltimaofertaValue);
            TextView dataFinalitzacio = (TextView) llyFila.findViewById(R.id.txvDataFinalitzacio);
            ImageView esGuanyador = (ImageView) llyFila.findViewById(R.id.imvAreYouWinner);
            holder = new ContenidorLlista(imatge,nomProducte,aliesOfertant,ultimaOferta,dataFinalitzacio,esGuanyador);
        }

        DadesSubhasta sb = mLlistaSubhastes.get(position);

        BigDecimal maxOferta = sb.getmMaxOfertaValue();
        if(maxOferta == null){
            holder.getUltimaOferta().setText(sb.getmPreuSortida().toString());
            holder.getAliesOfertant().setText("No hi ha ofertants");
        }else{
            holder.getUltimaOferta().setText(maxOferta.toString());
            holder.getAliesOfertant().setText(sb.getAliesOfertant());
        }
        Date dFinalitzacio = sb.getDataFi();
        holder.getDataFinalitzacio().setText(dFinalitzacio.toString());

        ImageView imgGuanyador = holder.getEsGuanyador();
        if(sb.getAliesOfertant() != null) {
            if (dFinalitzacio.compareTo(new Date()) < 0 && sb.getAliesOfertant().equals(mUsuariApplicacio.getAlias())){
                imgGuanyador.setVisibility(View.VISIBLE);
            }else{
                imgGuanyador.setVisibility(View.INVISIBLE);
            }
        }else{
            imgGuanyador.setVisibility(View.INVISIBLE);
        }

        String pc = sb.getNomProducte();
        holder.getNomProducte().setText(pc);

        String url = sb.getUrlImatge();
        if(url != null){
            imageLoader.DisplayImage(url, holder.getImatge());
        }

        final ListView listView = (ListView) parentListView;
        llyFila.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkItem(listView, position);
            }
        });

        return llyFila;
    }

    private boolean checkItem(ListView listView, int position) {
        boolean isChecked = listView.isItemChecked(position);
        listView.setItemChecked(position, !isChecked);
        return !isChecked;
    }

    class ContenidorLlista{
        private ImageView imatge;
        private TextView nomProducte;
        private TextView aliesOfertant;
        private TextView ultimaOferta;
        private TextView dataFinalitzacio;
        private ImageView esGuanyador;

        public ContenidorLlista(ImageView imatge, TextView nomProducte, TextView aliesOfertant, TextView ultimaOferta, TextView dataFinalitzacio, ImageView esGuanyador) {
            this.imatge = imatge;
            this.nomProducte = nomProducte;
            this.aliesOfertant = aliesOfertant;
            this.ultimaOferta = ultimaOferta;
            this.dataFinalitzacio = dataFinalitzacio;
            this.esGuanyador = esGuanyador;
        }

        public ImageView getImatge() {
            return imatge;
        }

        public void setImatge(ImageView imatge) {
            this.imatge = imatge;
        }

        public TextView getNomProducte() {
            return nomProducte;
        }

        public void setNomProducte(TextView nomProducte) {
            this.nomProducte = nomProducte;
        }

        public TextView getAliesOfertant() {
            return aliesOfertant;
        }

        public void setAliesOfertant(TextView aliesOfertant) {
            this.aliesOfertant = aliesOfertant;
        }

        public TextView getUltimaOferta() {
            return ultimaOferta;
        }

        public void setUltimaOferta(TextView ultimaOferta) {
            this.ultimaOferta = ultimaOferta;
        }

        public TextView getDataFinalitzacio() {
            return dataFinalitzacio;
        }

        public void setDataFinalitzacio(TextView dataFinalitzacio) {
            this.dataFinalitzacio = dataFinalitzacio;
        }

        public ImageView getEsGuanyador() {
            return esGuanyador;
        }

        public void setEsGuanyador(ImageView esGuanyador) {
            this.esGuanyador = esGuanyador;
        }
    }
}
