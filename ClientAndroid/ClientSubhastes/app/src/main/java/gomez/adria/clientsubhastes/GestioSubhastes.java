package gomez.adria.clientsubhastes;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadFactory;

import gomez.adria.Adapters.SubhastaAdapter;
import gomez.adria.gestorBD.DatabaseHelper;
import gomez.adria.gomez.adria.asyncTasks.RecuperarSubhastesActives;
import gomez.adria.gomez.adria.asyncTasks.RecuperarSubhastesDelUsuari;
import info.infomila.model.TipusTarja;
import info.infomila.model.Usuari;
import info.infomila.servidor.paquets.DadesSubhasta;

/**
 * Created by Adria on 27/05/2016.
 */
public class GestioSubhastes extends AppCompatActivity {

    private Usuari mUsuari;
    private ListView mLsvActives;
    private ListView mLsvMeves;
    private Button mBtnInfoActiva;
    private Button mBtnInfoMeva;
    private List<DadesSubhasta> mLlSubhastesActives;
    private List<DadesSubhasta> mLlSubhastesMeves;
    private ActualitzarSubhastes mThreadActualitzador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gestio_subhastes);

        mUsuari = getUsuariActual();

        agafarObjectesLayout();

        addActionListeners();

        //Iniciem el thread que actualitzara les comandes
        mThreadActualitzador = new ActualitzarSubhastes();
        Thread t = new Thread(mThreadActualitzador);
        t.start();
    }

    private void addActionListeners() {
        mBtnInfoActiva.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mLlSubhastesActives == null){
                    Toast.makeText(GestioSubhastes.this,"No hi han subhastes actives",Toast.LENGTH_SHORT).show();
                    return;
                }
                int pos = mLsvActives.getCheckedItemPosition();
                if(pos >= 0 && pos < mLlSubhastesActives.size()){
                    Intent mainIntent = new Intent().setClass(GestioSubhastes.this, PreviewProducte.class);
                    DadesSubhasta ds = mLlSubhastesActives.get(pos);
                    mainIntent.putExtra("codiSubhasta",ds.getmCodi());
                    mainIntent.putExtra("esActiva",true);
                    startActivity(mainIntent);
                }else{
                    Toast.makeText(GestioSubhastes.this,"No hi ha res seleccionat",Toast.LENGTH_SHORT).show();
                }
            }
        });

        mBtnInfoMeva.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mLlSubhastesMeves == null){
                    Toast.makeText(GestioSubhastes.this,"No hi han subhastes teves",Toast.LENGTH_SHORT).show();
                    return;
                }
                int pos = mLsvMeves.getCheckedItemPosition();
                if(pos >= 0 && pos < mLlSubhastesMeves.size()){
                    Intent mainIntent = new Intent().setClass(GestioSubhastes.this, PreviewProducte.class);
                    DadesSubhasta ds = mLlSubhastesMeves.get(pos);
                    mainIntent.putExtra("codiSubhasta",ds.getmCodi());
                    if(ds.getDataFi().compareTo(new Date()) <= 0) {
                        mainIntent.putExtra("esActiva", false);
                    }else{
                        mainIntent.putExtra("esActiva", true);
                    }
                    startActivity(mainIntent);
                }else{
                    Toast.makeText(GestioSubhastes.this,"No hi ha res seleccionat",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void ferAsyncTask() {
        RecuperarSubhastesActives rc = new RecuperarSubhastesActives(this);
        rc.execute();
        RecuperarSubhastesDelUsuari ru = new RecuperarSubhastesDelUsuari(this,mUsuari);
        ru.execute();
    }

    public void carregarDadesSubhastaActives(List<DadesSubhasta> ll){
        if(ll == null){
            Toast.makeText(this,"Error en recuperar les dades", Toast.LENGTH_LONG).show();
        }else {
            mLlSubhastesActives = ll;
            SubhastaAdapter adapter = new SubhastaAdapter(this, ll, mUsuari);
            mLsvActives.setAdapter(adapter);
            mLsvActives.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        }
    }

    public void carregarDadesSubhastaDeUsuari(List<DadesSubhasta> ll) {
        if(ll == null){
            Toast.makeText(this,"Error en recuperar les dades", Toast.LENGTH_LONG).show();
        }else {
            mLlSubhastesMeves = ll;
            SubhastaAdapter adapter = new SubhastaAdapter(this, ll, mUsuari);
            mLsvMeves.setAdapter(adapter);
            mLsvMeves.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        }
    }


    private void agafarObjectesLayout() {
        mBtnInfoActiva = (Button) findViewById(R.id.btnVeureInfoSubActiva);
        mBtnInfoMeva = (Button) findViewById(R.id.btnVeureInfoSubParticipada);
        mLsvActives = (ListView) findViewById(R.id.lsvSubhastesActives);
        mLsvMeves = (ListView) findViewById(R.id.lsvSubhastesParticipades);
    }

    public Usuari getUsuariActual() {
        DatabaseHelper dbh = new DatabaseHelper(GestioSubhastes.this,"BD",1);
        SQLiteDatabase db = dbh.getReadableDatabase();

        Cursor c = db.rawQuery("select * from usuari",null);
        Usuari u = null;
        while(c.moveToNext()){
            int id = c.getInt(c.getColumnIndex("id"));
            String alies = c.getString(c.getColumnIndex("alies"));
            String nom = c.getString(c.getColumnIndex("nom"));
            String cognom = c.getString(c.getColumnIndex("cognom"));
            String titular = c.getString(c.getColumnIndex("titular"));
            TipusTarja[] tt = TipusTarja.values();
            int ordinal = c.getInt(c.getColumnIndex("tipus_tarja"));
            TipusTarja tipus = null;
            for(TipusTarja t:tt){
                if(ordinal == t.ordinal()){
                    tipus = t;
                }
            }
            String num_tarja = c.getString(c.getColumnIndex("num_tarja"));
            Long caducitat = c.getLong(c.getColumnIndex("caducitat"));
            Date dCaducitat = new Date(caducitat);
            Integer CVV = c.getInt(c.getColumnIndex("CVV"));
            u = new Usuari(alies,num_tarja,titular,CVV,dCaducitat,tipus,nom,cognom);
            u.setCodi(id);
        }
        db.close();
        return u;
    }

    class ActualitzarSubhastes implements Runnable{

        private boolean pararThread = false;

        @Override
        public void run() {

            while(!pararThread){
                try {
                    GestioSubhastes.this.ferAsyncTask();
                    Thread.sleep(120000);  //2 minuts
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (Exception ex){
                    Log.e(">>","",ex);
                }
            }
        }

        public void aturarThread(){
            pararThread = true;
        }

        public void engegarThread(){ pararThread = false;}
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mThreadActualitzador == null) return;
        mThreadActualitzador.aturarThread();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(mThreadActualitzador == null) return;
        mThreadActualitzador.aturarThread();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mThreadActualitzador == null) return;
        mThreadActualitzador.aturarThread();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mThreadActualitzador != null) {
            mThreadActualitzador.engegarThread();
            Thread t = new Thread(mThreadActualitzador);
            t.start();
        }
    }
}