package gomez.adria.clientsubhastes;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by Adria on 26/05/2016.
 */
public class MenuPrincipal extends AppCompatActivity {

    private ImageView mBtnGestioSubhastes,mBtnSettings,mBtnReporting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);

        mBtnGestioSubhastes = (ImageView) findViewById(R.id.btnGestioSubhastes);
        mBtnSettings = (ImageView) findViewById(R.id.btnSettings);
        mBtnReporting = (ImageView) findViewById(R.id.btnRepoting);

        mBtnGestioSubhastes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent().setClass(
                        MenuPrincipal.this, GestioSubhastes.class);
                startActivity(mainIntent);
            }
        });

        mBtnReporting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent().setClass(
                        MenuPrincipal.this, Reporting.class);
                startActivity(mainIntent);
            }
        });

        mBtnSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent().setClass(
                        MenuPrincipal.this, Registre.class);
                mainIntent.putExtra("esUpgrade",true);
                startActivity(mainIntent);
            }
        });
    }

}
