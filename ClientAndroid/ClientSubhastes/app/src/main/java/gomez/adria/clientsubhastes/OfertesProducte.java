package gomez.adria.clientsubhastes;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.math.BigDecimal;
import java.security.KeyStore;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadFactory;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import gomez.adria.Adapters.OfertaAdapter;
import gomez.adria.gestorBD.DatabaseHelper;
import gomez.adria.gomez.adria.asyncTasks.InsercioDeOferta;
import gomez.adria.gomez.adria.asyncTasks.RecuperarOfertesDeSubhasta;
import info.infomila.model.TipusTarja;
import info.infomila.model.Usuari;
import info.infomila.servidor.paquets.InfoOferta;

/**
 * Created by Adria on 27/05/2016.
 */
public class OfertesProducte extends AppCompatActivity {

    private Button mBtnAddOferta;
    private EditText mEdtOferta;
    private ListView mLsvOfertes;
    private List<InfoOferta> mLlOfertes;
    private Integer mCodiSubhasta;
    private Usuari mUsuari;
    private Boolean mEstaActiva;
    private ActuaOfertes mThreadActualitzador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ofertes_producte);

        mCodiSubhasta = getIntent().getExtras().getInt("codiSubhasta");
        mEstaActiva = getIntent().getExtras().getBoolean("esActiva",false);
        mUsuari = createOrReadDB();

        carregarUI();

        ferAsyncTaskDescarregarOfertes();

        addActionListeners();

        mBtnAddOferta.setEnabled(mEstaActiva);

        if(mEstaActiva) {
            mThreadActualitzador = new ActuaOfertes();
            Thread t = new Thread(mThreadActualitzador);
            t.start();
        }
    }

    private void addActionListeners() {
        mBtnAddOferta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BigDecimal bg = importCorrecte();
                if(bg != null){
                    InsercioDeOferta io = new InsercioDeOferta(OfertesProducte.this,mCodiSubhasta,bg ,mUsuari);
                    io.execute();
                    setEnabledButtonOfertes(false);
                }else{
                    Toast.makeText(OfertesProducte.this,"Error import incorrecte", Toast.LENGTH_SHORT).show();
                }
            }

            private BigDecimal importCorrecte() {
                try {
                    BigDecimal bg = new BigDecimal(mEdtOferta.getText().toString());
                    return bg;
                }catch (Exception ex) {
                    return null;
                }
            }
        });
    }

    public void ferAsyncTaskDescarregarOfertes() {
        RecuperarOfertesDeSubhasta rf = new RecuperarOfertesDeSubhasta(this,mCodiSubhasta,mEstaActiva,mUsuari);
        rf.execute();
    }

    private void carregarUI() {
        mBtnAddOferta = (Button) findViewById(R.id.btnFerOferta);
        mEdtOferta = (EditText) findViewById(R.id.edtOferta);
        mLsvOfertes = (ListView) findViewById(R.id.lsvOfertes);
    }

    public void omplirListViewOfertes(List<InfoOferta> infoOfertas) {
        if(infoOfertas == null){
            mostrarError(-1);
        }else {
            Usuari u = createOrReadDB();

            mLlOfertes = infoOfertas;
            OfertaAdapter of = new OfertaAdapter(this, infoOfertas, u.getAlias());
            mLsvOfertes.setAdapter(of);
            mLsvOfertes.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        }
    }

    private Usuari createOrReadDB() {
        //Crear db if don't exists
        DatabaseHelper dbh = new DatabaseHelper(OfertesProducte.this,"BD",1);
        SQLiteDatabase db = dbh.getReadableDatabase();

        Usuari u = readDB(db);
        db.close();
        return u;
    }

    private Usuari readDB(SQLiteDatabase db) {
        Cursor c = db.rawQuery("select * from usuari",null);
        Usuari u = null;
        while(c.moveToNext()){
            int id = c.getInt(c.getColumnIndex("id"));
            String alies = c.getString(c.getColumnIndex("alies"));
            String nom = c.getString(c.getColumnIndex("nom"));
            String cognom = c.getString(c.getColumnIndex("cognom"));
            String titular = c.getString(c.getColumnIndex("titular"));
            TipusTarja[] tt = TipusTarja.values();
            int ordinal = c.getInt(c.getColumnIndex("tipus_tarja"));
            TipusTarja tipus = null;
            for(TipusTarja t:tt){
                if(ordinal == t.ordinal()){
                    tipus = t;
                }
            }
            String num_tarja = c.getString(c.getColumnIndex("num_tarja"));
            Long caducitat = c.getLong(c.getColumnIndex("caducitat"));
            Date dCaducitat = new Date(caducitat);
            Integer CVV = c.getInt(c.getColumnIndex("CVV"));
            u = new Usuari(alies,num_tarja,titular,CVV,dCaducitat,tipus,nom,cognom);
            u.setCodi(id);
        }
        return u;
    }

    public void mostrarError(Integer res) {
        if(res == null){
            Toast.makeText(this,"Error en recuperar les dades de ofertes", Toast.LENGTH_SHORT).show();
            setEnabledButtonOfertes(true);
        }else{
            switch (res){
                case -1:
                    Toast.makeText(this,"Error en recuperar les dades de ofertes", Toast.LENGTH_SHORT).show();
                    break;
                case -2:
                    Toast.makeText(this,"Error, no s'ha trobat la subhasta/usuari", Toast.LENGTH_SHORT).show();
                    break;
                case -3:
                    Toast.makeText(this,"Error, la subhasta esta tancada", Toast.LENGTH_SHORT).show();
                    setEnabledButtonOfertes(false);
                    break;
                case -4:
                    Toast.makeText(this,"Error, el import es masa baix", Toast.LENGTH_SHORT).show();
                    setEnabledButtonOfertes(true);
                    break;
                default:
                    setEnabledButtonOfertes(true);
                    setEnabledButtonOfertes(true);
                    break;
            }
        }
    }

    public void setEnabledButtonOfertes(boolean enabledButtonOfertes) {
        if(!enabledButtonOfertes) Log.d(">>","El poso a false");
        mBtnAddOferta.setEnabled(enabledButtonOfertes);
    }

    public void showInserted() {
        Toast.makeText(this,"Oferta inserida", Toast.LENGTH_SHORT).show();
    }

    class ActuaOfertes implements Runnable {
        private boolean pararThread = false;

        @Override
        public void run() {
            while(!pararThread)
                try{
                    Thread.sleep(10000);
                    if(!pararThread) {
                        ferAsyncTaskDescarregarOfertes();
                    }
                }catch (Exception ex){

                }
        }

        public void pararThread(){pararThread = true;}
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(mThreadActualitzador == null) return;
        mThreadActualitzador.pararThread();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mThreadActualitzador == null) return;
        mThreadActualitzador.pararThread();
    }

}