package gomez.adria.clientsubhastes;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import gomez.adria.Adapters.ImatgeAdapter;
import gomez.adria.gomez.adria.asyncTasks.RecuperarInformacioDeSubhasta;
import info.infomila.model.Subhasta;
import info.infomila.servidor.paquets.InformacioProducte;

/**
 * Created by Adria on 27/05/2016.
 */
public class PreviewProducte extends AppCompatActivity {

    private Integer mCodiSubhasta;
    private TextView mTxvNomProducte;
    private ListView mLsvFotos;
    private Button mBtnVeureOfertes;
    private WebView mWVDescripcio;
    private Boolean mEstaActiva;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_producte);

        mCodiSubhasta = getIntent().getExtras().getInt("codiSubhasta");
        mEstaActiva = getIntent().getExtras().getBoolean("esActiva", false);

        agafarUI();
        WebChromeClient cc = new WebChromeClient(){
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
            }
        };
        mWVDescripcio.setWebChromeClient(cc);

        crearAsyncTask();

        addActionListeners();
    }

    private void addActionListeners() {
        mBtnVeureOfertes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent().setClass(
                        PreviewProducte.this, OfertesProducte.class);
                mainIntent.putExtra("codiSubhasta",mCodiSubhasta);
                mainIntent.putExtra("esActiva",mEstaActiva);
                startActivity(mainIntent);
            }
        });
    }

    private void crearAsyncTask() {
        RecuperarInformacioDeSubhasta rc = new RecuperarInformacioDeSubhasta(this,mCodiSubhasta);
        rc.execute();
    }

    private void agafarUI() {
        mTxvNomProducte = (TextView) findViewById(R.id.txvNomProd);
        mBtnVeureOfertes = (Button) findViewById(R.id.btnVeureTotesOfertesSubhasta);
        mWVDescripcio = (WebView) findViewById(R.id.wvDescripcio);
        mLsvFotos = (ListView) findViewById(R.id.lsvImatges);
    }

    public void omplirDadesSubhasta(InformacioProducte info) {
        if(info == null){
            Toast.makeText(this,"Error en recuperar la informacio del producte", Toast.LENGTH_SHORT).show();
            return;
        }
        mTxvNomProducte.setText(info.getmNom());

        mWVDescripcio.loadData(info.getmDescHTML(), "text/html", "UTF-8");

        if(info.getmURLsImatges() != null && info.getmURLsImatges().size() > 0) {
            ImatgeAdapter adapter = new ImatgeAdapter(this, info.getmURLsImatges());
            mLsvFotos.setAdapter(adapter);
            mLsvFotos.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        }
    }
}
