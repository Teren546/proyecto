package gomez.adria.clientsubhastes;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import gomez.adria.gestorBD.DatabaseHelper;
import gomez.adria.gomez.adria.asyncTasks.RegistrarUsuariAServidor;
import info.infomila.model.TipusTarja;
import info.infomila.model.Usuari;

/**
 * Created by Adria on 26/05/2016.
 */
public class Registre extends AppCompatActivity {

    private EditText mEdtAlies;
    private EditText mEdtNom;
    private EditText mEdtCognom;
    private EditText mEdtTitular;
    private Spinner mSpnTipusTarja;
    private EditText mEdtNumTarja;
    private DatePicker mDtpDataCaducitat;
    private EditText mEdtCVV;
    private Button mBtnRegistrar;
    private LinearLayout mLLydialegError;
    private TextView mLblMissatgeEmergent;
    private ProgressBar mPgbRegistre;
    private Boolean esUpgrade;
    private Button mTancarDialeg;

    private Usuari mUsuari;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registre);

        esUpgrade = getIntent().getExtras().getBoolean("esUpgrade");


        carregarUI();

        actionsListeners();

        omplirSpinner();

        if(esUpgrade){
            loadDadesIntoForm();
        }
    }

    private void loadDadesIntoForm() {
        Usuari u = createOrReadDB();
        if(u != null){
            mEdtAlies.setText(u.getAlias());
            mEdtNom.setText(u.getNom());
            mEdtCognom.setText(u.getCognoms());
            mEdtTitular.setText(u.getTitular());
            TipusTarja[] tt = TipusTarja.values();
            int cont = 0;
            for(TipusTarja t : tt){
                if(t.equals(u.getTipus())){
                    break;
                }
                cont++;
            }
            mSpnTipusTarja.setSelection(cont);
            mEdtNumTarja.setText(u.getNumero_tarja());
            setDateToDatePicker(mDtpDataCaducitat,u.getCaducitat());
            mEdtCVV.setText(u.getCVV().toString());
        }
    }

    private void omplirSpinner() {
        ArrayAdapter<TipusTarja> adapter = new ArrayAdapter<TipusTarja>(this,
                android.R.layout.simple_spinner_item, TipusTarja.values());
        mSpnTipusTarja.setAdapter(adapter);
    }

    private void actionsListeners() {
        mBtnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(campsCorrectes()){
                    Usuari u = crearUsuariAPartirDeCamps();
                    if(u == null){
                        Toast.makeText(Registre.this,"Camps incorrectes",Toast.LENGTH_SHORT).show();
                    }else {
                        if(esUpgrade){
                           u.setCodi(mUsuari.getCodi());
                        }
                        //Començar ASyncTask
                        RegistrarUsuariAServidor registrar = new RegistrarUsuariAServidor(Registre.this, u, esUpgrade);
                        registrar.execute();
                    }
                }else{
                    Toast.makeText(Registre.this,"Camps incorrectes",Toast.LENGTH_SHORT).show();
                }
            }

            private Usuari crearUsuariAPartirDeCamps() {
                Usuari u = null;
                try {
                    String alies = mEdtAlies.getText().toString();
                    String nom = mEdtNom.getText().toString();
                    String cognom = mEdtCognom.getText().toString();
                    String numTarja = mEdtNumTarja.getText().toString();
                    String titular = mEdtTitular.getText().toString();
                    TipusTarja tipus = (TipusTarja) mSpnTipusTarja.getSelectedItem();
                    Integer cvv = new Integer(mEdtCVV.getText().toString());
                    Date d = getDateFromDatePicker(mDtpDataCaducitat);
                    u = new Usuari(alies,numTarja,titular,cvv,d,tipus,nom,cognom);
                    return u;
                }catch (Exception ex){
                    return null;
                }
            }

            private boolean campsCorrectes() {
                Boolean estanBe = true;
                if("".equals(mEdtAlies.getText().toString())){
                    mEdtAlies.setBackgroundColor(Color.RED);
                    estanBe = false;
                }else{
                    mEdtAlies.setBackgroundColor(Color.TRANSPARENT);
                }
                if("".equals(mEdtNom.getText().toString())){
                    mEdtNom.setBackgroundColor(Color.RED);
                    estanBe = false;
                }else{
                    mEdtNom.setBackgroundColor(Color.TRANSPARENT);
                }
                if("".equals(mEdtCognom.getText().toString())){
                    mEdtCognom.setBackgroundColor(Color.RED);
                    estanBe = false;
                }else{
                    mEdtCognom.setBackgroundColor(Color.TRANSPARENT);
                }
                if("".equals(mEdtNumTarja.getText().toString()) || mEdtNumTarja.getText().toString().length() != 16){
                    mEdtNumTarja.setBackgroundColor(Color.RED);
                    estanBe = false;
                }else{
                    mEdtNumTarja.setBackgroundColor(Color.TRANSPARENT);
                }
                if(mSpnTipusTarja.getSelectedItemPosition() < 0){
                    mSpnTipusTarja.setBackgroundColor(Color.RED);
                    estanBe = false;
                }else{
                    mSpnTipusTarja.setBackgroundColor(Color.TRANSPARENT);
                }
                if("".equals(mEdtCVV.getText().toString()) || mEdtCVV.getText().toString().length() > 3){
                    mEdtCVV.setBackgroundColor(Color.RED);
                    estanBe = false;
                }else{
                    mEdtCVV.setBackgroundColor(Color.TRANSPARENT);
                }
                if("".equals(mEdtTitular.getText().toString())){
                    mEdtTitular.setBackgroundColor(Color.RED);
                    estanBe = false;
                }else{
                    mEdtTitular.setBackgroundColor(Color.TRANSPARENT);
                }
                return estanBe;
            }
        });

        mTancarDialeg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLLydialegError.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void carregarUI() {
        mEdtAlies = (EditText) findViewById(R.id.edtAlies);
        mEdtNom = (EditText) findViewById(R.id.edtNom);
        mEdtCognom = (EditText) findViewById(R.id.edtCognom);
        mSpnTipusTarja = (Spinner) findViewById(R.id.spnTipusTarja);
        mEdtNumTarja = (EditText) findViewById(R.id.edtNumTarja);
        mDtpDataCaducitat = (DatePicker) findViewById(R.id.dtpDataCaducitat);
        mEdtCVV = (EditText) findViewById(R.id.edtCVV);
        mBtnRegistrar = (Button) findViewById(R.id.btnRegistrar);
        mLLydialegError = (LinearLayout) findViewById(R.id.llydialegError);
        mLblMissatgeEmergent = (TextView) findViewById(R.id.lblMissatgeEmergent);
        mEdtTitular = (EditText) findViewById(R.id.edtTitular);
        mPgbRegistre = (ProgressBar) findViewById(R.id.pgbRegistre);
        mTancarDialeg = (Button) findViewById(R.id.btnTancarFinestra);
    }

    public java.util.Date getDateFromDatePicker(DatePicker datePicker){
        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth();
        int year =  datePicker.getYear();

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);

        return calendar.getTime();
    }

    public void setDateToDatePicker(DatePicker datePicker, Date d){
        Calendar c = Calendar.getInstance();
        c.setTime(d);

        Log.d(">>","" + d);
        Log.d(">>","" + c);
        datePicker.updateDate(c.get(Calendar.YEAR),c.get(Calendar.MONTH),c.get(Calendar.DAY_OF_MONTH));
    }

    public void mostrarProgressBar(int visibilitat) {
        mPgbRegistre.setVisibility(visibilitat);
    }

    public void pasarActivity() {
        Toast.makeText(Registre.this,"Usuari registrat",Toast.LENGTH_SHORT).show();
        Intent mainIntent = new Intent().setClass(
                Registre.this, MenuPrincipal.class);
        startActivity(mainIntent);
        finish();
    }

    public void mostrarError(int codiError) {
        if(codiError == -1){
            mLblMissatgeEmergent.setText("Error al registrarte, el alies esta en us");
        }else{
            mLblMissatgeEmergent.setText("Error al registrarte");
        }
        mLLydialegError.setVisibility(View.VISIBLE);
    }

    public void inserirUsuariABD(Usuari u) {
        //Crear db if don't exists
        DatabaseHelper dbh = new DatabaseHelper(Registre.this,"BD",1);
        SQLiteDatabase db = dbh.getReadableDatabase();


        Object[] args = new Object[]{
                u.getCodi(),
                u.getAlias(),
                u.getNom(),
                u.getCognoms(),
                u.getTipus().ordinal(),
                u.getNumero_tarja(),
                u.getTitular(),
                u.getCaducitat().getTime(),
                u.getCVV()
        };

        db.execSQL("Insert into usuari(id,alies,nom,cognom,tipus_tarja,num_tarja,titular,caducitat,CVV) " +
                "values " +
                "(?,?,?,?,?,?,?,?,?);",args);
        db.close();
    }

    private Usuari createOrReadDB() {
        //Crear db if don't exists
        DatabaseHelper dbh = new DatabaseHelper(Registre.this,"BD",1);
        SQLiteDatabase db = dbh.getReadableDatabase();

        mUsuari = readDB(db);
        db.close();
        return mUsuari;
    }

    private Usuari readDB(SQLiteDatabase db) {
        Cursor c = db.rawQuery("select * from usuari",null);
        Usuari u = null;
        while(c.moveToNext()){
            int id = c.getInt(c.getColumnIndex("id"));
            String alies = c.getString(c.getColumnIndex("alies"));
            String nom = c.getString(c.getColumnIndex("nom"));
            String cognom = c.getString(c.getColumnIndex("cognom"));
            String titular = c.getString(c.getColumnIndex("titular"));
            TipusTarja[] tt = TipusTarja.values();
            int ordinal = c.getInt(c.getColumnIndex("tipus_tarja"));
            TipusTarja tipus = null;
            for(TipusTarja t:tt){
                if(ordinal == t.ordinal()){
                    tipus = t;
                }
            }
            String num_tarja = c.getString(c.getColumnIndex("num_tarja"));
            Long caducitat = c.getLong(c.getColumnIndex("caducitat"));
            Date dCaducitat = new Date(caducitat);
            Integer CVV = c.getInt(c.getColumnIndex("CVV"));
            u = new Usuari(alies,num_tarja,titular,CVV,dCaducitat,tipus,nom,cognom);
            u.setCodi(id);
        }
        return u;
    }

    public void actualitzarUsuariABD(Usuari u) {
        DatabaseHelper dbh = new DatabaseHelper(Registre.this,"BD",1);
        SQLiteDatabase db = dbh.getReadableDatabase();


        Object[] args = new Object[]{
                u.getAlias(),
                u.getNom(),
                u.getCognoms(),
                u.getTipus().ordinal(),
                u.getNumero_tarja(),
                u.getTitular(),
                u.getCaducitat().getTime(),
                u.getCVV(),
                u.getCodi()
        };

        db.execSQL("update usuari set " +
                "alies=?," +
                "nom=?," +
                "cognom=?," +
                "tipus_tarja=?," +
                "num_tarja=?," +
                "titular=?," +
                "caducitat=?," +
                "CVV=? where id = ?",args);
        db.close();
    }
}
