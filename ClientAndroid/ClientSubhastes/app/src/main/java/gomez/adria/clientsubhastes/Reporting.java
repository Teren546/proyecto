package gomez.adria.clientsubhastes;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.HttpAuthHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.Date;

import gomez.adria.gestorBD.DatabaseHelper;
import gomez.adria.gomez.adria.asyncTasks.RecuperarReportProdXCat;
import gomez.adria.gomez.adria.asyncTasks.RecuperarSubhastesGunyadesReport;
import info.infomila.model.TipusTarja;
import info.infomila.model.Usuari;

/**
 * Created by Adria on 27/05/2016.
 */
public class Reporting extends AppCompatActivity {

    private Button mBtnPerCategoria;
    private EditText mEdtCategoria;
    private Button mBtnProductesGuanyats;
    private WebView mWVReport;
    private ProgressBar mPGB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reporting);

        mBtnPerCategoria = (Button) findViewById(R.id.btnProdXCat);
        mBtnProductesGuanyats = (Button) findViewById(R.id.btnSubhWinnedReport);
        mEdtCategoria = (EditText) findViewById(R.id.edtCategoriaABuscar);
        mWVReport = (WebView) findViewById(R.id.wbReporting);
        mPGB = (ProgressBar) findViewById(R.id.pgbReporting);
        mWVReport.setWebViewClient(new WebViewClient()
        {
            @Override
            public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, String realm) {
                handler.proceed("m2-agomez","48235541X");
            }
        });
        WebChromeClient cc = new WebChromeClient(){
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                if(newProgress == 100){
                    mPGB.setVisibility(View.INVISIBLE);
                    mBtnPerCategoria.setEnabled(true);
                    mBtnProductesGuanyats.setEnabled(true);
                }else{
                    mBtnPerCategoria.setEnabled(false);
                    mBtnProductesGuanyats.setEnabled(false);
                    mPGB.setVisibility(View.VISIBLE);
                }
            }
        };

        mWVReport.getSettings().setAppCacheEnabled(false);
        mWVReport.getSettings().setJavaScriptEnabled(true);
        mWVReport.setWebChromeClient(cc);

        mBtnProductesGuanyats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                asynktaskPerGuanyats();
            }

            private void asynktaskPerGuanyats() {

                try{
                    Usuari u  = createOrReadDB();
                    if(u == null) throw new RuntimeException("error en recuperar el usuari");
                    mWVReport.loadUrl("http://92.222.27.83:8080/jasperserver/rest_v2/reports/m2.agomez/Proyecte/subhastesguanyades.html?alies="+u.getAlias());
                    /*
                    if(u!=null) {
                        RecuperarSubhastesGunyadesReport guany = new RecuperarSubhastesGunyadesReport(Reporting.this,u.getAlias());
                        guany.execute();
                    }else{
                        throw new RuntimeException("error, usuari null");
                    }*/
                }catch (Exception ex){
                    Reporting.this.mostrarError();
                }
            }
        });
        mBtnPerCategoria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                asyntaskPerCategoria();
            }

            private void asyntaskPerCategoria() {
                try {
                    mWVReport.loadUrl("http://92.222.27.83:8080/jasperserver/rest_v2/reports/m2.agomez/Proyecte/ProductesPerCategoriaProj.html?categoria="+mEdtCategoria.getText().toString());

                    /*
                    RecuperarReportProdXCat pxc = new RecuperarReportProdXCat(Reporting.this, mEdtCategoria.getText().toString());
                    Log.e(">>","Hago la asynctask");
                    pxc.execute();
                    */
                }catch (Exception ex){
                    Log.e(">>","",ex);
                    Reporting.this.mostrarError();
                }
            }
        });
    }


    public void carregarReportAlView(String s) {
        mWVReport.loadDataWithBaseURL("http://92.222.27.83:8080",s,"text/html", "utf-8", null);
    }

    public void setActivePGB(boolean activePGB) {
        if(activePGB) {
            mPGB.setVisibility(View.VISIBLE);
        }else{
            mPGB.setVisibility(View.INVISIBLE);
        }
    }

    public void mostrarError() {
        Toast.makeText(this,"Error al recuperar el report",Toast.LENGTH_SHORT).show();
    }

    public void enableButtons(boolean b) {
        mBtnPerCategoria.setEnabled(b);
        mBtnProductesGuanyats.setEnabled(b);
    }

    private Usuari createOrReadDB() {
        //Crear db if don't exists
        DatabaseHelper dbh = new DatabaseHelper(Reporting.this,"BD",1);
        SQLiteDatabase db = dbh.getReadableDatabase();

        Usuari u = readDB(db);
        db.close();
        return u;
    }

    private Usuari readDB(SQLiteDatabase db) {
        Cursor c = db.rawQuery("select * from usuari",null);
        Usuari u = null;
        while(c.moveToNext()){
            int id = c.getInt(c.getColumnIndex("id"));
            String alies = c.getString(c.getColumnIndex("alies"));
            String nom = c.getString(c.getColumnIndex("nom"));
            String cognom = c.getString(c.getColumnIndex("cognom"));
            String titular = c.getString(c.getColumnIndex("titular"));
            TipusTarja[] tt = TipusTarja.values();
            int ordinal = c.getInt(c.getColumnIndex("tipus_tarja"));
            TipusTarja tipus = null;
            for(TipusTarja t:tt){
                if(ordinal == t.ordinal()){
                    tipus = t;
                }
            }
            String num_tarja = c.getString(c.getColumnIndex("num_tarja"));
            Long caducitat = c.getLong(c.getColumnIndex("caducitat"));
            Date dCaducitat = new Date(caducitat);
            Integer CVV = c.getInt(c.getColumnIndex("CVV"));
            u = new Usuari(alies,num_tarja,titular,CVV,dCaducitat,tipus,nom,cognom);
            u.setCodi(id);
        }
        return u;
    }
}