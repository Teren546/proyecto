package gomez.adria.clientsubhastes;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import gomez.adria.gestorBD.DatabaseHelper;
import gomez.adria.gomez.adria.asyncTasks.ProbarConexion;
import info.infomila.model.TipusTarja;
import info.infomila.model.Usuari;

public class Splash_Screen extends AppCompatActivity {

    // Set the duration of the splash screen
    private static final long SPLASH_SCREEN_DELAY = 1000;

    private ProgressBar mPGB;
    private Boolean connSuccesful;
    private LinearLayout mLLydialegError;
    private Button mBtnTancarFinestra;
    //Sockets


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        mPGB = (ProgressBar) findViewById(R.id.pgb);
        mPGB.setVisibility(View.INVISIBLE);
        mLLydialegError = (LinearLayout) findViewById(R.id.llydialegError);
        mBtnTancarFinestra = (Button) findViewById(R.id.btnTancarFinestra);
        mBtnTancarFinestra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Si no ens conectem tanquem conexio
                Splash_Screen.this.finish();
            }
        });

        TimerTask task = new TimerTask() {
            @Override
            public void run() {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ProbarConexion pc = new ProbarConexion(Splash_Screen.this);
                        pc.execute();
                    }
                });

                //Intentar conectar amb el servidor
                while(connSuccesful == null){

                }
                if(!connSuccesful){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mLLydialegError.setVisibility(View.VISIBLE);
                        }
                    });
                    return;
                }

                Usuari u = createOrReadDB();
                // Start the next activity
                if(u == null){
                    //Start Registre
                    Intent mainIntent = new Intent().setClass(
                            Splash_Screen.this, Registre.class);
                    mainIntent.putExtra("esUpgrade",false);
                    startActivity(mainIntent);
                }else{
                    //Pasar a menu principal
                    Intent mainIntent = new Intent().setClass(
                            Splash_Screen.this, MenuPrincipal.class);
                    startActivity(mainIntent);
                }


                // Close the activity so the user won't able to go back this
                // activity pressing Back button
                finish();
            }
        };



        // Simulate a long loading process on application startup.
        Timer timer = new Timer();
        timer.schedule(task, SPLASH_SCREEN_DELAY);
    }

    private Usuari createOrReadDB() {
        //Crear db if don't exists
        DatabaseHelper dbh = new DatabaseHelper(Splash_Screen.this,"BD",1);
        SQLiteDatabase db = dbh.getReadableDatabase();

        Usuari u = readDB(db);
        db.close();
        return u;
    }

    private Usuari readDB(SQLiteDatabase db) {
        Cursor c = db.rawQuery("select * from usuari",null);
        Usuari u = null;
        while(c.moveToNext()){
            int id = c.getInt(c.getColumnIndex("id"));
            String alies = c.getString(c.getColumnIndex("alies"));
            String nom = c.getString(c.getColumnIndex("nom"));
            String cognom = c.getString(c.getColumnIndex("cognom"));
            String titular = c.getString(c.getColumnIndex("titular"));
            TipusTarja[] tt = TipusTarja.values();
            int ordinal = c.getInt(c.getColumnIndex("tipus_tarja"));
            TipusTarja tipus = null;
            for(TipusTarja t:tt){
                if(ordinal == t.ordinal()){
                    tipus = t;
                }
            }
            String num_tarja = c.getString(c.getColumnIndex("num_tarja"));
            Long caducitat = c.getLong(c.getColumnIndex("caducitat"));
            Date dCaducitat = new Date(caducitat);
            Integer CVV = c.getInt(c.getColumnIndex("CVV"));
            u = new Usuari(alies,num_tarja,titular,CVV,dCaducitat,tipus,nom,cognom);
            u.setCodi(id);
        }
        return u;
    }

    public void mostrarProgressBar() {
        mPGB.setVisibility(View.VISIBLE);
    }

    public void amagarProgressBar() {
        mPGB.setVisibility(View.INVISIBLE);
    }

    public void setCorrecteConexio(Boolean o) {
        connSuccesful = o;
    }
}
