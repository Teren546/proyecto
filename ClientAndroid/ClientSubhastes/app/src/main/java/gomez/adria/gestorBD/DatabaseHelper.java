package gomez.adria.gestorBD;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Adria on 26/05/2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    public DatabaseHelper(Context context, String name, int version) {
        super(context, name, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try{
            //CreateTables
            createTable(db);

        }catch (Exception ex){
            Log.e("SplashScreen","Error al crear db: ", ex);
            throw ex;
        }
    }

    private void createTable(SQLiteDatabase db) {
        db.execSQL("Create table usuari(" +
                "id integer primary key,"+
                "alies text," +
                "nom text,"+
                "cognom text,"+
                "tipus_tarja integer,"+
                "num_tarja text,"+
                "titular text,"+
                "caducitat integer,"+
                "CVV int"+
                ");"
                );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
