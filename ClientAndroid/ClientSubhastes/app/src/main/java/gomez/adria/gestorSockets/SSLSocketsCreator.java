package gomez.adria.gestorSockets;

import android.app.Activity;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import gomez.adria.clientsubhastes.R;

/**
 * Created by Adria on 31/05/2016.
 */
public class SSLSocketsCreator {

    /**
     * Retorna un socket SSL conectat a la ip del servidor de subhastes
     * @param mActivity
     * @return
     * @throws CertificateException
     * @throws NoSuchAlgorithmException
     * @throws IOException
     * @throws KeyStoreException
     * @throws UnrecoverableKeyException
     * @throws KeyManagementException
     */
    public static SSLSocket getSocketToConnectServer(Activity mActivity) throws CertificateException, NoSuchAlgorithmException, IOException, KeyStoreException, UnrecoverableKeyException, KeyManagementException {
        KeyStore keyStore = KeyStore.getInstance("BKS");
        keyStore.load(mActivity.getResources().openRawResource(R.raw.clientkey),
                "clientpass".toCharArray());

        KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        kmf.init(keyStore, "clientpass".toCharArray());

        KeyStore trustedStore = KeyStore.getInstance("BKS");
        trustedStore.load(mActivity.getResources().openRawResource(R.raw.clienttrustedcerts), "clientpass"
                .toCharArray());

        TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        tmf.init(trustedStore);

        SSLContext sc = SSLContext.getInstance("TLS");
        TrustManager[] trustManagers = tmf.getTrustManagers();
        KeyManager[] keyManagers = kmf.getKeyManagers();
        sc.init(keyManagers, trustManagers, null);

        SSLSocketFactory mFactory = sc.getSocketFactory();
        //SSLSocket mConexio = (SSLSocket) mFactory.createSocket("10.180.0.116", 1234);
        SSLSocket mConexio = (SSLSocket) mFactory.createSocket("192.168.0.105", 1234);
        return mConexio;
    }

}
