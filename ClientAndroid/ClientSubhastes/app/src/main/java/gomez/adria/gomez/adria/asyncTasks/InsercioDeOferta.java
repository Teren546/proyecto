package gomez.adria.gomez.adria.asyncTasks;

import android.content.Intent;
import android.os.AsyncTask;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.security.KeyStore;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import gomez.adria.clientsubhastes.OfertesProducte;
import gomez.adria.clientsubhastes.R;
import gomez.adria.gestorSockets.SSLSocketsCreator;
import info.infomila.model.Usuari;
import info.infomila.servidor.paquets.OpcionsServidor;

/**
 * Created by Adria on 28/05/2016.
 */
public class InsercioDeOferta extends AsyncTask<String,Void,Integer> {

    private OfertesProducte mActivity;
    private SSLSocket mConexio;
    private SSLSocketFactory mFactory;
    private Integer mCodiSubhasta;
    private BigDecimal mImport;
    private Usuari mUsuari;

    public InsercioDeOferta(OfertesProducte mActivity, Integer codiSubhasta, BigDecimal importe, Usuari u) {
        this.mActivity = mActivity;
        this.mCodiSubhasta = codiSubhasta;
        this.mImport = importe;
        this.mUsuari = u;
    }

    @Override
    protected Integer doInBackground(String... params) {
        try{
            mConexio = SSLSocketsCreator.getSocketToConnectServer(mActivity);
            mConexio.startHandshake();

            ObjectOutputStream writer = new ObjectOutputStream(mConexio.getOutputStream());
            ObjectInputStream reader=new ObjectInputStream(mConexio.getInputStream());


            writer.writeObject(OpcionsServidor.AFEGIR_OFERTA);

            writer.writeObject(mCodiSubhasta);

            Integer res = (Integer) reader.readObject();
            if(res != 0){
                return res;
            }

            writer.writeObject(mImport);
            writer.writeObject(mUsuari);

            res = (Integer) reader.readObject();
            if(res!=0){
                return res;
            }
            return 0;
        }catch (Exception ex){
            return -1;
        }catch(Throwable ex){
            return -1;
        }
    }

    @Override
    protected void onPostExecute(Integer res) {
        super.onPostExecute(res);
        if(res!=0){
            mActivity.mostrarError(res);
        }else{
            mActivity.showInserted();
            mActivity.ferAsyncTaskDescarregarOfertes();
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }
}
