package gomez.adria.gomez.adria.asyncTasks;

import android.os.AsyncTask;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.KeyStore;
import java.util.Date;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import gomez.adria.clientsubhastes.R;
import gomez.adria.clientsubhastes.Splash_Screen;
import gomez.adria.gestorSockets.SSLSocketsCreator;
import info.infomila.model.TipusTarja;
import info.infomila.model.Usuari;
import info.infomila.servidor.paquets.OpcionsServidor;

/**
 * Created by Adria on 26/05/2016.
 */
public class ProbarConexion extends AsyncTask<String,Void,Boolean> {

    private Splash_Screen mActivity;
    private SSLSocket mConexio;
    private SSLSocketFactory mFactory;

    public ProbarConexion(Splash_Screen sp){
        mActivity = sp;
    }

    @Override
    protected Boolean doInBackground(String... params) {
        try {
            mConexio = SSLSocketsCreator.getSocketToConnectServer(mActivity);
            mConexio.startHandshake();

            ObjectOutputStream writer = new ObjectOutputStream(mConexio.getOutputStream());
            ObjectInputStream reader=new ObjectInputStream(mConexio.getInputStream());

            writer.writeObject(OpcionsServidor.SORTIR);

            return true;
        }catch (Exception ex){
            ex.printStackTrace();
            return false;
        }catch(Throwable ex){
            return false;
        }finally {
            if(mConexio != null){
                try {
                    mConexio.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mActivity.mostrarProgressBar();
    }

    @Override
    protected void onPostExecute(Boolean o) {
        super.onPostExecute(o);
        mActivity.amagarProgressBar();
        mActivity.setCorrecteConexio(o);
    }
}
