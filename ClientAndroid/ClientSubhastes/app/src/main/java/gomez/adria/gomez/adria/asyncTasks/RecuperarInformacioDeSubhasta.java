package gomez.adria.gomez.adria.asyncTasks;

import android.os.AsyncTask;
import android.util.Log;


import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.KeyStore;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import gomez.adria.clientsubhastes.PreviewProducte;
import gomez.adria.clientsubhastes.R;
import gomez.adria.gestorSockets.SSLSocketsCreator;
import info.infomila.model.Subhasta;
import info.infomila.servidor.paquets.InformacioProducte;
import info.infomila.servidor.paquets.OpcionsServidor;

/**
 * Created by Adria on 27/05/2016.
 */
public class RecuperarInformacioDeSubhasta extends AsyncTask<String,Void,InformacioProducte> {

    private SSLSocket mConexio;
    private SSLSocketFactory mFactory;
    private PreviewProducte mActivity;
    private Integer mCodiSubhasta;

    public RecuperarInformacioDeSubhasta(PreviewProducte mActivity, int codiSubhasta){
        this.mActivity = mActivity;
        this.mCodiSubhasta = codiSubhasta;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(InformacioProducte info) {
        super.onPostExecute(info);
        mActivity.omplirDadesSubhasta(info);
    }

    @Override
    protected InformacioProducte doInBackground(String... params) {
        try{
            mConexio = SSLSocketsCreator.getSocketToConnectServer(mActivity);
            mConexio.startHandshake();

            ObjectOutputStream writer = new ObjectOutputStream(mConexio.getOutputStream());
            ObjectInputStream reader=new ObjectInputStream(mConexio.getInputStream());


            writer.writeObject(OpcionsServidor.INFO_PRODUCTE);

            writer.writeObject(mCodiSubhasta);

            InformacioProducte info = (InformacioProducte) reader.readObject();

            return info;
        }catch (Exception ex){
            Log.e(">>","",ex);
            return null;
        }catch(Throwable ex){
            return null;
        }finally {
            if(mConexio != null){
                try {
                    mConexio.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
