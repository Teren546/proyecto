package gomez.adria.gomez.adria.asyncTasks;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.KeyStore;
import java.util.List;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import gomez.adria.clientsubhastes.OfertesProducte;
import gomez.adria.clientsubhastes.PreviewProducte;
import gomez.adria.clientsubhastes.R;
import gomez.adria.gestorSockets.SSLSocketsCreator;
import info.infomila.model.Usuari;
import info.infomila.servidor.paquets.InfoOferta;
import info.infomila.servidor.paquets.InformacioProducte;
import info.infomila.servidor.paquets.OpcionsServidor;

/**
 * Created by Adria on 27/05/2016.
 */
public class RecuperarOfertesDeSubhasta extends AsyncTask<String,Void,List<InfoOferta>> {

    private SSLSocket mConexio;
    private SSLSocketFactory mFactory;
    private final OfertesProducte mActivity;
    private final int mCodiSubhasta;
    private boolean estaActiva = true;
    private Usuari mUsuari;

    public RecuperarOfertesDeSubhasta(OfertesProducte mActivity, int codiSubhasta, boolean estaActiva, Usuari u){
        this.mActivity = mActivity;
        this.mCodiSubhasta = codiSubhasta;
        this.estaActiva = estaActiva;
        mUsuari = u;
    }

    @Override
    protected List<InfoOferta> doInBackground(String... params) {
        try{
            mConexio = SSLSocketsCreator.getSocketToConnectServer(mActivity);
            mConexio.startHandshake();

            ObjectOutputStream writer = new ObjectOutputStream(mConexio.getOutputStream());
            ObjectInputStream reader=new ObjectInputStream(mConexio.getInputStream());


            writer.writeObject(OpcionsServidor.GET_INFO_OFERTES);

            writer.writeObject(mCodiSubhasta);

            List<InfoOferta> info = (List<InfoOferta>) reader.readObject();

            return info;
        }catch (Exception ex){
            Log.e(">>","",ex);
            return null;
        }catch(Throwable ex){
            return null;
        }finally {
            if(mConexio != null){
                try {
                    mConexio.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected void onPostExecute(List<InfoOferta> infoOfertas) {
        super.onPostExecute(infoOfertas);
        mActivity.omplirListViewOfertes(infoOfertas);
        mActivity.setEnabledButtonOfertes(estaActiva);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mActivity.setEnabledButtonOfertes(false);
    }
}
