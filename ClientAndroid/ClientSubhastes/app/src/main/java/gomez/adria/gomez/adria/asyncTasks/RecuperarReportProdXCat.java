package gomez.adria.gomez.adria.asyncTasks;

import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import gomez.adria.clientsubhastes.Reporting;

/**
 * Created by Adria on 30/05/2016.
 */
public class RecuperarReportProdXCat extends AsyncTask<String,Void,String> {

    private final Reporting mActivity;
    private final String mCategoria;

    public RecuperarReportProdXCat(Reporting activity, String categoria){
        this.mActivity = activity;
        this.mCategoria = categoria;
        Log.e(">>","Constructor!!! Asynctask");
    }

    @Override
    protected String doInBackground(String... params) {
        String html = "";
        Log.e(">>","Do in background babe");
        try {
            String url = "http://92.222.27.83:8080/jasperserver/rest_v2/reports/m2.agomez/Proyecte/ProductesPerCategoriaProj.html";
            if(!"".equals(mCategoria) && mCategoria != null){
                url += "?categoria="+mCategoria;
            }
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            String autenticacio = Base64.encodeToString(("m2-agomez" + ":" + "48235541X").getBytes(),Base64.DEFAULT);
            con.setRequestProperty("Authorization", "Basic " + autenticacio);
            int responseCode = con.getResponseCode();
            Log.e(">>","Response code= " + responseCode);
            if (responseCode == HttpURLConnection.HTTP_OK) {
                // Obrim InputStream des de HTTP connection
                BufferedReader inputStream = new BufferedReader(new InputStreamReader(con.getInputStream()));

                // Llegim i escrivim
                String aux = null;
                while ((aux = inputStream.readLine()) != null) {
                    html += aux;
                }
                inputStream.close();
            }
            con.disconnect();
        } catch (MalformedURLException ex) {
            Log.e(">>","",ex);
            return null;
        } catch (IOException ex) {
            Log.e(">>","",ex);
            return null;
        }catch(Throwable ex){
            return null;
        }
        return html;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mActivity.enableButtons(false);
        mActivity.setActivePGB(true);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if(s == null){
            mActivity.mostrarError();
        }else {
            mActivity.carregarReportAlView(s);
        }
        mActivity.setActivePGB(false);
        mActivity.enableButtons(true);
    }
}
