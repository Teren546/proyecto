package gomez.adria.gomez.adria.asyncTasks;

import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import gomez.adria.clientsubhastes.GestioSubhastes;
import gomez.adria.clientsubhastes.R;
import gomez.adria.clientsubhastes.Registre;
import gomez.adria.gestorSockets.SSLSocketsCreator;
import info.infomila.model.Usuari;
import info.infomila.servidor.paquets.DadesSubhasta;
import info.infomila.servidor.paquets.OpcionsServidor;

/**
 * Created by Adria on 26/05/2016.
 */
public class RecuperarSubhastesActives extends AsyncTask<String,Void,List<DadesSubhasta>> {

    private GestioSubhastes mActivity;
    private SSLSocket mConexio;
    private SSLSocketFactory mFactory;

    public RecuperarSubhastesActives(GestioSubhastes activity){
        mActivity = activity;
    }

    @Override
    protected List<DadesSubhasta> doInBackground(String... params) {
        try{
            mConexio = SSLSocketsCreator.getSocketToConnectServer(mActivity);
            mConexio.startHandshake();

            ObjectOutputStream writer = new ObjectOutputStream(mConexio.getOutputStream());
            ObjectInputStream reader=new ObjectInputStream(mConexio.getInputStream());



            //Diem que volem la llista de subhastes actives
            writer.writeObject(OpcionsServidor.Llista_Subhastes_Actives);

            //Recuperem la llista
            List<DadesSubhasta> ll = new ArrayList<>();

            while(true) {
                Object obj = reader.readObject();
                if(obj instanceof DadesSubhasta){
                    ll.add((DadesSubhasta) obj);
                }else{
                    Integer codeResult = (Integer) obj;
                    if(codeResult == 0) {
                        break;
                    }else{
                        throw new Exception("Error");
                    }
                }
            }

            return ll;
        }catch(Exception ex) {
            Log.e(">>", "", ex);
            ex.printStackTrace();
            return null;
        }catch(Throwable ex){
            return null;
        }finally {
            try {
                mConexio.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onPostExecute(List<DadesSubhasta> ll) {
        super.onPostExecute(ll);
        mActivity.carregarDadesSubhastaActives(ll);
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }
}
