package gomez.adria.gomez.adria.asyncTasks;

import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.KeyStore;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import gomez.adria.clientsubhastes.R;
import gomez.adria.clientsubhastes.Registre;
import gomez.adria.gestorBD.DatabaseHelper;
import gomez.adria.gestorSockets.SSLSocketsCreator;
import info.infomila.model.Usuari;
import info.infomila.servidor.paquets.OpcionsServidor;

/**
 * Created by Adria on 26/05/2016.
 */
public class RegistrarUsuariAServidor extends AsyncTask<String,Void,Integer> {

    private Registre mActivity;
    private SSLSocket mConexio;
    private SSLSocketFactory mFactory;
    private Usuari mUsuari;
    private Boolean esUpgrade;

    public RegistrarUsuariAServidor(Registre activity, Usuari u, Boolean esUpgrade){
        mActivity = activity;
        mUsuari = u;
        this.esUpgrade = esUpgrade;
    }

    @Override
    protected Integer doInBackground(String... params) {
        try{
            mConexio = SSLSocketsCreator.getSocketToConnectServer(mActivity);
            mConexio.startHandshake();

            ObjectOutputStream writer = new ObjectOutputStream(mConexio.getOutputStream());
            ObjectInputStream reader=new ObjectInputStream(mConexio.getInputStream());

            if(!esUpgrade) {
                //Enviem solicitud de insercio de usuari
                writer.writeObject(OpcionsServidor.REGISTRAR_USUARI);
            }else{
                //Enviem solicitud de actualitzacio de usuari
                writer.writeObject(OpcionsServidor.ACTUALITZAR_USUARI);
            }



            //Enviem el usuari
            writer.writeObject(mUsuari);

            //Llegim codi
            Integer codiUsuari = (Integer) reader.readObject();
            if(codiUsuari >= 0){
                mUsuari.setCodi(codiUsuari);
            }

            mConexio.close();
            return codiUsuari;
        }catch(Exception ex){
            return null;
        }catch(Throwable ex){
            return null;
        }
    }

    @Override
    protected void onPostExecute(Integer codi_usuari) {
        super.onPostExecute(codi_usuari);
        mActivity.mostrarProgressBar(View.INVISIBLE);
        if(codi_usuari == null){
            mActivity.mostrarError(codi_usuari);
        }else {
            if (codi_usuari >= 0) {
                Log.d(">>", "" + mUsuari);
                if (!esUpgrade) {
                    mActivity.inserirUsuariABD(mUsuari);
                } else {
                    mActivity.actualitzarUsuariABD(mUsuari);
                }
                mActivity.pasarActivity();
            } else {
                mActivity.mostrarError(codi_usuari);
            }
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mActivity.mostrarProgressBar(View.VISIBLE);
    }
}
