package gomez.adria.lazyloading;

import android.content.Context;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import java.io.File;

public class FileCache {
    
    private File cacheDir;
    
    public FileCache(Context context){
        //Find the dir to save cached images

        Log.e(">>ADRI", Environment.getExternalStorageState());
        //if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
        //    cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"personatges");
        //else
            cacheDir=context.getCacheDir();
        Log.e(">>ADRI", cacheDir.getAbsolutePath());
        Log.e(">>ADRI", cacheDir.exists() + "");
        if(!cacheDir.exists())
            cacheDir.mkdirs();

        MediaScannerConnection.scanFile(context, new String[]{cacheDir.toString()}, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                        Log.i("ExternalStorage", "Scanned " + path + ":");
                        Log.i("ExternalStorage", "-> uri=" + uri);
                    }
                });
    }
    
    public File getFile(String url){
        //I identify images by hashcode. Not a perfect solution, good for the demo.
        //String filename= String.valueOf(url.hashCode());
        //Another possible solution (thanks to grantland)
        String filename = url.substring(url.lastIndexOf("/")+1);
        File f = new File(cacheDir, filename);
        return f;
        
    }
    
    public void clear(){
        File[] files=cacheDir.listFiles();
        if(files==null)
            return;
        for(File f:files)
            f.delete();
    }

}