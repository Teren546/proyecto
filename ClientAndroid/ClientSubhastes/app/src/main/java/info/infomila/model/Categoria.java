package info.infomila.model;

import android.annotation.TargetApi;
import android.os.Build;

import java.io.Serializable;
import java.util.Objects;

/**
 * Created by Adria on 26/05/2016.
 */
public class Categoria implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer mCodi;
    private String mNom;
    private Categoria mCategoriaPare;

    protected Categoria() {
    }

    public Categoria(String nom, Categoria categoriapare) {
        setNom(nom);
        setCategoriapare(categoriapare);
    }

    public Integer getCodi() {
        return mCodi;
    }

    protected void setCodi(Integer codi) {
        this.mCodi = codi;
    }

    public String getNom() {
        return mNom;
    }

    public final void setNom(String nom) {
        if(nom == null) throw new SubhastaException("Error, el nom no pot ser null");
        if(nom.length() < 1 || nom.length() > 63) throw new SubhastaException("Error, el nom ha de estar entre 1 i 64 caracters");
        this.mNom = nom;
    }

    public Categoria getCategoriapare() {
        return mCategoriaPare;
    }

    public final void setCategoriapare(Categoria categoriapare) {
        this.mCategoriaPare = categoriapare;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + Objects.hashCode(this.mCodi);
        hash = 47 * hash + Objects.hashCode(this.mNom);
        hash = 47 * hash + Objects.hashCode(this.mCategoriaPare);
        return hash;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Categoria other = (Categoria) obj;
        if (!Objects.equals(this.mCodi, other.mCodi)) {
            return false;
        }
        if (!Objects.equals(this.mNom, other.mNom)) {
            return false;
        }
        if (!Objects.equals(this.mCategoriaPare, other.mCategoriaPare)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Categoria{" + "codi=" + mCodi + ", nom=" + mNom + ", categoriapare=" + mCategoriaPare + '}';
    }


}
