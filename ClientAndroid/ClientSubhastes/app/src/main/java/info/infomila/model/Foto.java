package info.infomila.model;

import android.annotation.TargetApi;
import android.os.Build;

import java.io.Serializable;
import java.util.Objects;

/**
 * Created by Adria on 26/05/2016.
 */
public class Foto implements Serializable {
    private static final long serialVersionUID = 1L;

    private String mURL;
    private String mCaption;
    private ProducteCataleg mProducteCataleg;

    protected Foto() {
    }

    public Foto(String url, String caption) {
        setUrl(url);
        setCaption(caption);
    }

    public String getUrl() {
        return mURL;
    }

    public final void setUrl(String url) {
        if(url == null) throw new SubhastaException("Error, la url no pot ser null");
        if(url.length() < 1 || url.length() > 2048) throw new SubhastaException("Error, la url no esta en el rang permes (1 - 2048) de caracters");
        this.mURL = url;
    }

    public String getCaption() {
        return mCaption;
    }

    public final void setCaption(String caption) {
        if(caption == null) throw new SubhastaException("Error, el caption no pot ser null");
        this.mCaption = caption;
    }

    public ProducteCataleg getProducteCataleg() {
        return mProducteCataleg;
    }

    public void setProducteCataleg(ProducteCataleg mProducteCataleg) {
        if(mProducteCataleg == null) throw new SubhastaException("Error, el producte cataleg no pot ser null");
        this.mProducteCataleg = mProducteCataleg;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.mURL);
        return hash;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Foto other = (Foto) obj;
        if (!Objects.equals(this.mURL, other.mURL)) {
            return false;
        }
        return true;
    }
}
