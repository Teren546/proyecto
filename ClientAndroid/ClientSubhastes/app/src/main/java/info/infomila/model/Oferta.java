package info.infomila.model;

import android.annotation.TargetApi;
import android.os.Build;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * Created by Adria on 26/05/2016.
 */
public class Oferta implements Serializable {
        private static final long serialVersionUID = 1L;

        private OfertaPK ofertaPK;

        private BigDecimal mImport;
        private CanalOferta mCanalOferta;
        private Usuari mUsuari;

        protected Oferta(){

        }

        public Oferta(BigDecimal import1, CanalOferta canalOferta, Subhasta subhasta, Usuari usuariCod) {
            this.mImport = import1;
            this.mCanalOferta = canalOferta;
            this.mUsuari = usuariCod;
            ofertaPK = new OfertaPK(subhasta,(subhasta.getNumOfertes() + 1));
            subhasta.addOferta(this);
            usuariCod.addOferta(this);
        }

        public BigDecimal getImport1() {
            return mImport;
        }

        public void setImport1(BigDecimal import1) {
            if(import1 == null) throw new SubhastaException("Error, el import no pot ser null");
            if(import1.compareTo(new BigDecimal(0)) <= 0) throw new SubhastaException("Error, el import ha de ser estrictament positiu");
            this.mImport = import1;
        }

        public CanalOferta getCanalOferta() {
            return mCanalOferta;
        }

        public void setCanalOferta(CanalOferta canalOferta) {
            if(canalOferta == null) throw new SubhastaException("Error, el canal de la oferta no pot ser null");
            this.mCanalOferta = canalOferta;
        }

        public Subhasta getSubhasta() {
            return ofertaPK.getmSubhasta();
        }

        public Usuari getUsuariCod() {
            return mUsuari;
        }

        @Override
        public String toString() {
            return "Oferta{" + "import1=" + mImport + ", canalOferta=" + mCanalOferta + ", subhasta=" + ofertaPK.getmSubhasta() + ", usuariCod=" + mUsuari + '}';
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        public int hashCode() {
            int hash = 5;
            hash = 19 * hash + Objects.hashCode(this.mImport);
            hash = 19 * hash + Objects.hashCode(this.mCanalOferta);
            hash = 19 * hash + Objects.hashCode(this.ofertaPK);
            hash = 19 * hash + Objects.hashCode(this.mUsuari);
            return hash;
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Oferta other = (Oferta) obj;
            if (!Objects.equals(this.ofertaPK, other.ofertaPK)) {
                return false;
            }
            return true;
        }
}
