package info.infomila.model;

import android.annotation.TargetApi;
import android.os.Build;

import java.io.Serializable;
import java.util.Objects;

/**
 * Created by Adria on 26/05/2016.
 */
public class OfertaPK implements Serializable {

    private Subhasta mSubhasta;
    private int mCodi;

    public OfertaPK(Subhasta subhasta, int i) {
        setmCodi(i);
        setmSubhasta(subhasta);
    }

    protected OfertaPK() {
    }

    public Subhasta getmSubhasta() {
        return mSubhasta;
    }

    public int getmCodi() {
        return mCodi;
    }

    public final void setmSubhasta(Subhasta mSubhasta) {
        if(mSubhasta == null) throw new SubhastaException("Error, la subhasta no pot ser null");
        this.mSubhasta = mSubhasta;
    }

    public final void setmCodi(int mCodi) {
        if(mCodi<=0) throw new SubhastaException("Error, el codi ha de ser estrictament positu");
        this.mCodi = mCodi;
    }



    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + Objects.hashCode(this.mSubhasta);
        hash = 37 * hash + this.mCodi;
        return hash;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OfertaPK other = (OfertaPK) obj;
        if (!Objects.equals(this.mSubhasta, other.mSubhasta)) {
            return false;
        }
        if (this.mCodi != other.mCodi) {
            return false;
        }
        return true;
    }


}
