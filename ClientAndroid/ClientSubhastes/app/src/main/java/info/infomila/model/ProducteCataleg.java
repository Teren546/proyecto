package info.infomila.model;

import android.annotation.TargetApi;
import android.os.Build;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by Adria on 26/05/2016.
 */
public class ProducteCataleg implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer mNumeroLoteria;
    private String mNom;
    private String mDescHTML;
    private BigDecimal mValorEstimat;
    private List<Foto> mLlistaFotos = new ArrayList<Foto>();
    private Categoria mCategoria;



    private Subhasta mSubhasta;

    protected ProducteCataleg() {
    }

    public ProducteCataleg(String nom, String deschtml, BigDecimal valorestimat, Categoria c) {
        setNom(nom);
        setDeschtml(deschtml);
        setValorestimat(valorestimat);
        setCategoria(c);
    }

    public Integer getNumerolot() {
        return mNumeroLoteria;
    }

    protected void setNumerolot(Integer numerolot) {
        this.mNumeroLoteria = numerolot;
    }

    public String getNom() {
        return mNom;
    }

    public final void setNom(String nom) {
        if(nom == null) throw new SubhastaException("Error, el nom no pot ser null");
        if(nom.length() < 1 || nom.length() > 63) throw new SubhastaException("Error, el nom ha de estar entre 1 i 64 caracters");
        this.mNom = nom;
    }

    public String getDeschtml() {
        return mDescHTML;
    }

    public final void setDeschtml(String deschtml) {
        this.mDescHTML = deschtml;
    }

    public BigDecimal getValorestimat() {
        return mValorEstimat;
    }

    public final void setValorestimat(BigDecimal valorestimat) {
        if(valorestimat == null) throw new SubhastaException("Error, el valor estimat no pot ser null");
        if(valorestimat.compareTo(new BigDecimal(0)) <= 0) throw new SubhastaException("Error, el valor estimat ha de ser estrictament positiu");
        this.mValorEstimat = valorestimat;
    }

    public Categoria getCategoria() {
        return mCategoria;
    }

    public final void setCategoria(Categoria categoria) {
        if(categoria == null) throw new SubhastaException("Error, categoria no pot ser null");
        this.mCategoria = categoria;
    }

    public void addFoto(Foto f){
        if(f == null) throw new SubhastaException("Error, intent d'afegir foto null");
        f.setProducteCataleg(this);
        this.mLlistaFotos.add(f);
    }

    public Subhasta getmSubhasta() {
        return mSubhasta;
    }

    public void setmSubhasta(Subhasta mSubhasta) {
        this.mSubhasta = mSubhasta;
    }

    public Foto getFotoAt(int index){
        if(index < 0) return null;
        if(index >= mLlistaFotos.size()) return null;
        return mLlistaFotos.get(index);
    }

    public int getNumFotos(){
        return mLlistaFotos.size();
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.mNumeroLoteria);
        hash = 71 * hash + Objects.hashCode(this.mNom);
        return hash;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProducteCataleg other = (ProducteCataleg) obj;
        if (!Objects.equals(this.mNumeroLoteria, other.mNumeroLoteria)) {
            return false;
        }
        if (!Objects.equals(this.mNom, other.mNom)) {
            return false;
        }
        return true;
    }


}
