package info.infomila.model;

/**
 * Created by Adria on 26/05/2016.
 */
public class SubhastaException extends RuntimeException {

    public SubhastaException(String message) {
        super(message);
    }

    public SubhastaException(String message, Throwable cause) {
        super(message, cause);
    }
}
