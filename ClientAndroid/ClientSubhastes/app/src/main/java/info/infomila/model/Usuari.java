package info.infomila.model;

import android.annotation.TargetApi;
import android.os.Build;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Created by Adria on 26/05/2016.
 */
public class Usuari implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer mCodi;
    private String mAlias;
    private String mNom;
    private String mCognoms;
    private String mNumeroTarja;
    private String mTitular;
    private Integer mCVV;
    private Date mCaducitat;
    private TipusTarja mTipusTarja;
    private List<Oferta> mLlistaOfertes = new ArrayList<Oferta>();

    protected Usuari() {
    }

    public Usuari(String alias, String numero_tarja, String titular, Integer CVV, Date caducitat, TipusTarja tipus, String nom, String cognoms) {
        setAlias(alias);
        setNumero_tarja(numero_tarja);
        setTitular(titular);
        setCVV(CVV);
        setCaducitat(caducitat);
        setTipus(tipus);
        setNom(nom);
        setCognoms(cognoms);
    }

    public Integer getCodi() {
        return mCodi;
    }
    public void setCodi(int codi) {
        mCodi = codi;
    }

    /**
     *
     * @param codi
     */
    protected final void setCodi(Integer codi) {
        this.mCodi = codi;
    }

    public String getAlias() {
        return mAlias;
    }

    public final void setAlias(String alias) {
        if(alias == null) throw new SubhastaException("Error el alias no pot ser null");
        if(alias.length() == 0 || alias.length() > 64) throw new SubhastaException("Error el alias ha de estar entre 1 y 64 caracters");
        this.mAlias = alias;
    }

    public String getNumero_tarja() {
        return mNumeroTarja;
    }

    public final void setNumero_tarja(String numero_tarja) {
        if(numero_tarja == null) throw new SubhastaException("Error numero de tarja no pot ser null");
        if(numero_tarja.length() != 16) throw new SubhastaException("El numero de tarja es de 16 digits");
        this.mNumeroTarja = numero_tarja;
    }

    public String getTitular() {
        return mTitular;
    }

    public final void setTitular(String titular) {
        if(titular == null) throw new SubhastaException("Error el titular no pot ser null");
        if(mAlias.length() == 0 || mAlias.length() > 64) throw new SubhastaException("Error el alias ha de estar entre 1 y 64 caracters");
        this.mTitular = titular;
    }

    public Integer getCVV() {
        return mCVV;
    }

    public final void setCVV(Integer CVV) {
        if(CVV == null) throw new SubhastaException("Error el CVV no pot ser null");
        if(CVV < 0 || CVV > 999) throw new SubhastaException("Error el CVV ha de estar entre 0 i 999");
        this.mCVV = CVV;
    }

    public Date getCaducitat() {
        return mCaducitat;
    }

    public final void setCaducitat(Date caducitat) {
        if(caducitat == null) throw new SubhastaException("Error la caducitat no pot ser null");
        this.mCaducitat = caducitat;
    }

    public TipusTarja getTipus() {
        return mTipusTarja;
    }

    public final void setTipus(TipusTarja tipus) {
        if(tipus == null) throw new SubhastaException("Error el tipus de tarja no pot ser null");
        this.mTipusTarja = tipus;
    }

    public String getNom() {
        return mNom;
    }

    public final void setNom(String nom) {
        if(nom == null) throw new SubhastaException("Error nom no pot ser null");
        if(nom.length() < 1 || nom.length() > 63) throw new SubhastaException("El nom no esta en els rang permesos 1 - 63");
        this.mNom = nom;
    }

    public String getCognoms() {
        return mCognoms;
    }

    public final void setCognoms(String cognoms) {
        if(cognoms == null) throw new SubhastaException("Error cognoms no pot ser null");
        if(cognoms.length() < 1 || cognoms.length() > 63) throw new SubhastaException("Els cognoms no esta en els rang permesos 1 - 63");
        this.mCognoms = cognoms;
    }

    public void addOferta(Oferta of){
        if(of == null) throw new RuntimeException("Error");
        this.mLlistaOfertes.add(of);
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.mCodi);
        hash = 53 * hash + Objects.hashCode(this.mAlias);
        return hash;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Usuari other = (Usuari) obj;
        if (!Objects.equals(this.mCodi, other.mCodi)) {
            return false;
        }
        if (!Objects.equals(this.mAlias, other.mAlias)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Usuari{" + "mCodi=" + mCodi + ", mAlias=" + mAlias + ", mNom=" + mNom + ", mCognoms=" + mCognoms + ", mNumeroTarja=" + mNumeroTarja + ", mTitular=" + mTitular + ", mCVV=" + mCVV + ", mCaducitat=" + mCaducitat + ", mTipusTarja=" + mTipusTarja + ", mLlistaOfertes=" + mLlistaOfertes + '}';
    }

    public boolean containsOferta(Oferta of) {
        return this.mLlistaOfertes.contains(of);
    }



}
