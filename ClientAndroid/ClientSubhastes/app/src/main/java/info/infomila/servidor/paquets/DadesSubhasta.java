/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.infomila.servidor.paquets;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Adria
 */
public class DadesSubhasta implements Serializable{
    private static final long serialVersionUID = 1L;

    private Integer mCodi;
    private BigDecimal mPreuSortida;
    private BigDecimal mMaxOfertaValue;
    private String urlImatge;
    private String nomProducte;
    private String aliesOfertant;
    private Date dataFi;

    public DadesSubhasta(Integer codi,BigDecimal mPreuSortida,BigDecimal mMaxOfertaValue, String urlImatge, String nomProducte, String aliesOfertant, Date d) {
        this.mCodi = codi;
        this.mPreuSortida = mPreuSortida;
        this.mMaxOfertaValue = mMaxOfertaValue;
        this.urlImatge = urlImatge;
        this.nomProducte = nomProducte;
        this.aliesOfertant = aliesOfertant;
        this.dataFi = d;
    }

    public BigDecimal getmMaxOfertaValue() {
        return mMaxOfertaValue;
    }

    public void setmMaxOfertaValue(BigDecimal mMaxOfertaValue) {
        this.mMaxOfertaValue = mMaxOfertaValue;
    }

    public String getUrlImatge() {
        return urlImatge;
    }

    public void setUrlImatge(String urlImatge) {
        this.urlImatge = urlImatge;
    }

    public String getNomProducte() {
        return nomProducte;
    }

    public void setNomProducte(String nomProducte) {
        this.nomProducte = nomProducte;
    }

    public String getAliesOfertant() {
        return aliesOfertant;
    }

    public void setAliesOfertant(String aliesOfertant) {
        this.aliesOfertant = aliesOfertant;
    }

    public Integer getmCodi() {
        return mCodi;
    }

    public void setmCodi(Integer mCodi) {
        this.mCodi = mCodi;
    }

    public BigDecimal getmPreuSortida() {
        return mPreuSortida;
    }

    public void setmPreuSortida(BigDecimal mPreuSortida) {
        this.mPreuSortida = mPreuSortida;
    }

    public Date getDataFi() {
        return dataFi;
    }

    public void setDataFi(Date dataFi) {
        this.dataFi = dataFi;
    }


}
