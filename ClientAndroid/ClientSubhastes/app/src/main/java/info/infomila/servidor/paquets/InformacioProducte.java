/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.infomila.servidor.paquets;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Adria
 */
public class InformacioProducte implements Serializable {
    private static final long serialVersionUID = 1L;

    private String mNom;
    private List<String> mURLsImatges;
    private String mDescHTML;

    public InformacioProducte(String mNom, List<String> mURLsImatges, String mDescHTML) {
        this.mNom = mNom;
        this.mURLsImatges = mURLsImatges;
        this.mDescHTML = mDescHTML;
    }

    public String getmNom() {
        return mNom;
    }

    public void setmNom(String mNom) {
        this.mNom = mNom;
    }

    public List<String> getmURLsImatges() {
        return mURLsImatges;
    }

    public void setmURLsImatges(List<String> mURLsImatges) {
        this.mURLsImatges = mURLsImatges;
    }

    public String getmDescHTML() {
        return mDescHTML;
    }

    public void setmDescHTML(String mDescHTML) {
        this.mDescHTML = mDescHTML;
    }


}
