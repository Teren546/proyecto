/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.infomila.modeldedades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Adria
 */
@Entity
@Table(name = "foto")
@NamedQueries({
    @NamedQuery(name = "Foto.findAll", query = "SELECT f FROM Foto f"),
    @NamedQuery(name = "Foto.findByProductecatalegCod", query = "SELECT f FROM Foto f WHERE f.fotoPK.productecatalegCod = :productecatalegCod"),
    @NamedQuery(name = "Foto.findByCodi", query = "SELECT f FROM Foto f WHERE f.fotoPK.codi = :codi"),
    @NamedQuery(name = "Foto.findByUrl", query = "SELECT f FROM Foto f WHERE f.url = :url"),
    @NamedQuery(name = "Foto.findByCaption", query = "SELECT f FROM Foto f WHERE f.caption = :caption")})
public class Foto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FotoPK fotoPK;
    @Basic(optional = false)
    @Column(name = "url")
    private String url;
    @Basic(optional = false)
    @Column(name = "caption")
    private String caption;
    @JoinColumn(name = "productecataleg_cod", referencedColumnName = "numerolot", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Productecataleg productecataleg;

    public Foto() {
    }

    public Foto(FotoPK fotoPK) {
        this.fotoPK = fotoPK;
    }

    public Foto(FotoPK fotoPK, String url, String caption) {
        this.fotoPK = fotoPK;
        this.url = url;
        this.caption = caption;
    }

    public Foto(Integer productecatalegCod, Integer codi) {
        this.fotoPK = new FotoPK(productecatalegCod);
    }

    public FotoPK getFotoPK() {
        return fotoPK;
    }

    public void setFotoPK(FotoPK fotoPK) {
        this.fotoPK = fotoPK;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public Productecataleg getProductecataleg() {
        return productecataleg;
    }

    public void setProductecataleg(Productecataleg productecataleg) {
        this.productecataleg = productecataleg;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fotoPK != null ? fotoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Foto)) {
            return false;
        }
        Foto other = (Foto) object;
        if ((this.fotoPK == null && other.fotoPK != null) || (this.fotoPK != null && !this.fotoPK.equals(other.fotoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.infomila.modeldedades.Foto[ fotoPK=" + fotoPK + " ]";
    }
    
}
