/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.infomila.modeldedades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Adria
 */
@Embeddable
public class FotoPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "productecataleg_cod")
    private Integer productecatalegCod;
    @Basic(optional = false)
    @Column(name = "codi")
    private Integer codi;

    public FotoPK() {
    }

    public FotoPK(Integer productecatalegCod) {
        this.productecatalegCod = productecatalegCod;
    }
    
    public FotoPK(Integer productecatalegCod, int codifoto) {
        this.productecatalegCod = productecatalegCod;
        this.codi = codifoto;
    }

    public int getProductecatalegCod() {
        return productecatalegCod;
    }

    public void setProductecatalegCod(int productecatalegCod) {
        this.productecatalegCod = productecatalegCod;
    }

    public int getCodi() {
        return codi;
    }

    public void setCodi(int codi) {
        this.codi = codi;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) productecatalegCod;
        hash += (int) codi;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FotoPK)) {
            return false;
        }
        FotoPK other = (FotoPK) object;
        if (this.productecatalegCod != other.productecatalegCod) {
            return false;
        }
        if (this.codi != other.codi) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.infomila.modeldedades.FotoPK[ productecatalegCod=" + productecatalegCod + ", codi=" + codi + " ]";
    }
    
}
