/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.infomila.modeldedades;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Adria
 */
@Entity
@Table(name = "oferta")
@NamedQueries({
    @NamedQuery(name = "Oferta.findAll", query = "SELECT o FROM Oferta o"),
    @NamedQuery(name = "Oferta.findByNumero", query = "SELECT o FROM Oferta o WHERE o.ofertaPK.numero = :numero"),
    @NamedQuery(name = "Oferta.findByImport1", query = "SELECT o FROM Oferta o WHERE o.import1 = :import1"),
    @NamedQuery(name = "Oferta.findByCanalOferta", query = "SELECT o FROM Oferta o WHERE o.canalOferta = :canalOferta"),
    @NamedQuery(name = "Oferta.findBySubhastaCod", query = "SELECT o FROM Oferta o WHERE o.ofertaPK.subhastaCod = :subhastaCod")})
public class Oferta implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected OfertaPK ofertaPK;
    @Basic(optional = false)
    @Column(name = "import")
    private BigInteger import1;
    @Basic(optional = false)
    @Column(name = "canal_oferta")
    @Enumerated(EnumType.STRING)
    private CanalOferta canalOferta;
    @JoinColumn(name = "subhasta_cod", referencedColumnName = "codi", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Subhasta subhasta;
    @JoinColumn(name = "usuari_cod", referencedColumnName = "codi")
    @ManyToOne(optional = false)
    private Usuari usuariCod;

    public Oferta() {
    }

    public Oferta(OfertaPK ofertaPK) {
        this.ofertaPK = ofertaPK;
    }

    public Oferta(OfertaPK ofertaPK, BigInteger import1, CanalOferta canalOferta) {
        this.ofertaPK = ofertaPK;
        this.import1 = import1;
        this.canalOferta = canalOferta;
    }

    public Oferta(int numero, int subhastaCod) {
        this.ofertaPK = new OfertaPK(numero, subhastaCod);
    }

    public OfertaPK getOfertaPK() {
        return ofertaPK;
    }

    public void setOfertaPK(OfertaPK ofertaPK) {
        this.ofertaPK = ofertaPK;
    }

    public BigInteger getImport1() {
        return import1;
    }

    public void setImport1(BigInteger import1) {
        this.import1 = import1;
    }

    public CanalOferta getCanalOferta() {
        return canalOferta;
    }

    public void setCanalOferta(CanalOferta canalOferta) {
        this.canalOferta = canalOferta;
    }

    public Subhasta getSubhasta() {
        return subhasta;
    }

    public void setSubhasta(Subhasta subhasta) {
        this.subhasta = subhasta;
    }

    public Usuari getUsuariCod() {
        return usuariCod;
    }

    public void setUsuariCod(Usuari usuariCod) {
        this.usuariCod = usuariCod;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ofertaPK != null ? ofertaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Oferta)) {
            return false;
        }
        Oferta other = (Oferta) object;
        if ((this.ofertaPK == null && other.ofertaPK != null) || (this.ofertaPK != null && !this.ofertaPK.equals(other.ofertaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.infomila.modeldedades.Oferta[ ofertaPK=" + ofertaPK + " ]";
    }
    
}
