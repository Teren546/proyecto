/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.infomila.probes;

import info.infomila.modeldedades.Categoria;
import info.infomila.modeldedades.Foto;
import info.infomila.modeldedades.FotoPK;
import info.infomila.modeldedades.Productecataleg;
import info.infomila.modeldedades.TipusTarja;
import info.infomila.modeldedades.Usuari;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Adria
 */
public class putamierda {
    public static void main(String[] args) {
        String up = "DefinicioDeClassesPU";
        EntityManagerFactory emf = null;
        EntityManager em = null;
        try {
            em = null;
            emf = null;
            System.out.println("Intent amb " + up);
            emf = Persistence.createEntityManagerFactory(up);
            System.out.println("EntityManagerFactory creada");
            em = emf.createEntityManager();
            System.out.println("EntityManager creat");
            
//            Categoria c = new Categoria();
//            c.setNom("cagunlostia");
//            
//            em.getTransaction().begin();
//            em.persist(c);
//            em.getTransaction().commit();
//            
//            System.out.println(c);
//            
//            Productecataleg pc = new Productecataleg();
//            pc.setCategoria(c);
//            pc.setNom("blablabla");
//            pc.setDeschtml("DDDD");
//            pc.setValorestimat(new BigDecimal(20));
//            
//            em.getTransaction().begin();
//            em.persist(pc);
//            em.getTransaction().commit();
//            
//            System.out.println(pc);
//            
//            Foto f = new Foto(new FotoPK(pc.getNumerolot(),1));
//            f.setCaption("AFDDS");
//            f.setUrl("sadfa");
//            System.out.println(f);
//            
//            em.getTransaction().begin();
//            em.persist(f);
//            em.getTransaction().commit();
            
            Usuari u = new Usuari("alias", "8383838", "Marc Zapata", "433", new Date(), TipusTarja.VISA);
            
            em.getTransaction().begin();
            em.persist(u);
            em.getTransaction().commit();
        } catch (Exception ex) {
            System.out.println("Exception: " + ex.getMessage());
            System.out.print(ex.getCause() != null ? "Caused by:" + ex.getCause().getMessage() + "\n" : "");
            System.out.println("Traça:");
            ex.printStackTrace();
        } finally {
            if (em != null) {
                em.close();
                System.out.println("EntityManager tancat");
            }
            if (emf != null) {
                emf.close();
                System.out.println("EntityManagerFactory tancada");
            }
        }
    }
}
