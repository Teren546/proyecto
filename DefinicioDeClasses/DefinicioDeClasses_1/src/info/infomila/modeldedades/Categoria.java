/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.infomila.modeldedades;

import info.infomila.modeldedades.exception.SubhastesException;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Adria
 */
@Entity
@Table(name = "categoria")
@NamedQueries({
    @NamedQuery(name = "Categoria.findAll", query = "SELECT c FROM Categoria c"),
    @NamedQuery(name = "Categoria.findByCodi", query = "SELECT c FROM Categoria c WHERE c.codi = :codi"),
    @NamedQuery(name = "Categoria.findByNom", query = "SELECT c FROM Categoria c WHERE c.nom = :nom")})
public class Categoria implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "codi",nullable=false)
    private Integer codi;
    @Basic(optional = false)
    @Column(name = "nom", nullable=false)
    private String nom;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "categoria", fetch = FetchType.LAZY)
//    private List<Productecataleg> productecatalegList;
//    @OneToMany(mappedBy = "categoriapare", fetch = FetchType.LAZY)
//    private List<Categoria> categoriaList;
    @JoinColumn(name = "categoriapare", referencedColumnName = "codi")
    @ManyToOne(fetch = FetchType.LAZY)
    private Categoria categoriapare;

    protected Categoria() {
    }
/*
    public Categoria(Integer codi) {
        this.codi = codi;
    }
*/
/*
    public Categoria(Integer codi, String nom) {
        setCodi(codi);
        setNom(nom);
    }
*/
    public Categoria(String nom) {
        setNom(nom);
    }

    public Integer getCodi() {
        return codi;
    }

    protected final void setCodi(Integer codi) {
        if(codi == null) throw new SubhastesException("Error la clau primaria no pot ser null");
        this.codi = codi;
    }

    public String getNom() {
        return nom;
    }

    public final void setNom(String nom) {
        if(nom == null) throw new SubhastesException("Error la clau primaria no pot ser null");
        this.nom = nom;
    }

//    protected List<Productecataleg> getProductecatalegList() {
//        return productecatalegList;
//    }
//
//    protected void setProductecatalegList(List<Productecataleg> productecatalegList) {
//        this.productecatalegList = productecatalegList;
//    }
//    
//    public Iterator<Productecataleg> getProducteCatalegIterator(){
//        if(productecatalegList == null) return null;
//        return productecatalegList.iterator();
//    }
//    
//    public Productecataleg getProductecatalegAt(int i){
//        if(productecatalegList == null) return null;
//        return productecatalegList.get(i);
//    }
//
//    protected List<Categoria> getCategoriaList() {
//        return categoriaList;
//    }
//
//    protected void setCategoriaList(List<Categoria> categoriaList) {
//        this.categoriaList = categoriaList;
//    }
//    
//    public Iterator<Categoria> getIteratorCaregoriesFilles(){
//        if(categoriaList == null) return null;
//        return categoriaList.iterator();
//    }
//    
//    public Categoria getCategoriaPareAt(int i){
//        if(categoriaList == null) return null;
//        return categoriaList.get(i);
//    }

    public Categoria getCategoriapare() {
        return categoriapare;
    }

    public void setCategoriapare(Categoria categoriapare) {
        this.categoriapare = categoriapare;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codi != null ? codi.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Categoria)) {
            return false;
        }
        Categoria other = (Categoria) object;
        if ((this.codi == null && other.codi != null) || (this.codi != null && !this.codi.equals(other.codi))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.infomila.modeldedades.Categoria[ codi=" + codi + " ]";
    }
    
}
