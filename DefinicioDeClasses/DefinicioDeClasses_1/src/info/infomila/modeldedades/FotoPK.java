/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.infomila.modeldedades;

import info.infomila.modeldedades.exception.SubhastesException;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Adria
 */
@Embeddable
public class FotoPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "productecataleg_cod")
    private Productecataleg productecatalegCod;
    @Basic(optional = false)
    @Column(name = "codi")
    private int codi;

    public FotoPK() {
    }

    public FotoPK(Productecataleg productecatalegCod,int codi) {
        setProductecatalegCod(productecatalegCod);
        setCodi(codi);
    }
    
    public FotoPK(Productecataleg productecatalegCod) {
        setProductecatalegCod(productecatalegCod);
    }

    public Productecataleg getProductecatalegCod() {
        return productecatalegCod;
    }

    public final void setProductecatalegCod(Productecataleg productecatalegCod) {
        if(productecatalegCod == null) throw new SubhastesException("Error la clau primaria no pot ser null");
        this.productecatalegCod = productecatalegCod;
    }

    public int getCodi() {
        return codi;
    }

    protected final void setCodi(Integer codi) {
        if(codi == null) throw new SubhastesException("Error la clau primaria no pot ser null");
        this.codi = codi;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) codi;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FotoPK)) {
            return false;
        }
        FotoPK other = (FotoPK) object;
        if (this.productecatalegCod != other.productecatalegCod) {
            return false;
        }
        if (this.codi != other.codi) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.infomila.modeldedades.FotoPK[ productecatalegCod=" + productecatalegCod + ", codi=" + codi + " ]";
    }
    
}
