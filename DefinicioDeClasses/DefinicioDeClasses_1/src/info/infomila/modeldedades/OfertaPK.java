/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.infomila.modeldedades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Adria
 */
@Embeddable
public class OfertaPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "numero")
    private int numero;
    @Basic(optional = false)
    @Column(name = "subhasta_cod")
    private int subhastaCod;

    public OfertaPK() {
    }

    public OfertaPK(int numero, int subhastaCod) {
        this.numero = numero;
        this.subhastaCod = subhastaCod;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getSubhastaCod() {
        return subhastaCod;
    }

    public void setSubhastaCod(int subhastaCod) {
        this.subhastaCod = subhastaCod;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) numero;
        hash += (int) subhastaCod;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OfertaPK)) {
            return false;
        }
        OfertaPK other = (OfertaPK) object;
        if (this.numero != other.numero) {
            return false;
        }
        if (this.subhastaCod != other.subhastaCod) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.infomila.modeldedades.OfertaPK[ numero=" + numero + ", subhastaCod=" + subhastaCod + " ]";
    }
    
}
