/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.infomila.modeldedades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Adria
 */
@Entity
@Table(name = "productecataleg")
@NamedQueries({
    @NamedQuery(name = "Productecataleg.findAll", query = "SELECT p FROM Productecataleg p"),
    @NamedQuery(name = "Productecataleg.findByNumerolot", query = "SELECT p FROM Productecataleg p WHERE p.numerolot = :numerolot"),
    @NamedQuery(name = "Productecataleg.findByNom", query = "SELECT p FROM Productecataleg p WHERE p.nom = :nom"),
    @NamedQuery(name = "Productecataleg.findByDeschtml", query = "SELECT p FROM Productecataleg p WHERE p.deschtml = :deschtml"),
    @NamedQuery(name = "Productecataleg.findByValorestimat", query = "SELECT p FROM Productecataleg p WHERE p.valorestimat = :valorestimat")})
public class Productecataleg implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "numerolot")
    private Integer numerolot;
    @Basic(optional = false)
    @Column(name = "nom")
    private String nom;
    @Basic(optional = false)
    @Column(name = "deschtml")
    private String deschtml;
    @Basic(optional = false)
    @Column(name = "valorestimat")
    private BigDecimal valorestimat;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productecataleg", fetch = FetchType.LAZY)
    private List<Foto> fotoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productecatalegCod", fetch = FetchType.LAZY)
    private List<Subhasta> subhastaList;
    @JoinColumn(name = "categoria", referencedColumnName = "codi")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Categoria categoria;

    protected Productecataleg() {
    }
/*
    public Productecataleg(Integer numerolot) {
        this.numerolot = numerolot;
    }
*/
    public Productecataleg(String nom, String deschtml, BigDecimal valorestimat, Categoria c) {
        this.nom = nom;
        this.deschtml = deschtml;
        this.valorestimat = valorestimat;
        this.categoria = c;
    }

    public Integer getNumerolot() {
        return numerolot;
    }

    public void setNumerolot(Integer numerolot) {
        this.numerolot = numerolot;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDeschtml() {
        return deschtml;
    }

    public void setDeschtml(String deschtml) {
        this.deschtml = deschtml;
    }

    public BigDecimal getValorestimat() {
        return valorestimat;
    }

    public void setValorestimat(BigDecimal valorestimat) {
        this.valorestimat = valorestimat;
    }

    public List<Foto> getFotoList() {
        return fotoList;
    }

    public void setFotoList(List<Foto> fotoList) {
        this.fotoList = fotoList;
    }

    public List<Subhasta> getSubhastaList() {
        return subhastaList;
    }

    public void setSubhastaList(List<Subhasta> subhastaList) {
        this.subhastaList = subhastaList;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numerolot != null ? numerolot.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Productecataleg)) {
            return false;
        }
        Productecataleg other = (Productecataleg) object;
        if ((this.numerolot == null && other.numerolot != null) || (this.numerolot != null && !this.numerolot.equals(other.numerolot))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.infomila.modeldedades.Productecataleg[ numerolot=" + numerolot + " ]";
    }
    
}
