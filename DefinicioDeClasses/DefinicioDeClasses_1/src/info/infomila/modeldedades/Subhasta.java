/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.infomila.modeldedades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Adria
 */
@Entity
@Table(name = "subhasta")
@NamedQueries({
    @NamedQuery(name = "Subhasta.findAll", query = "SELECT s FROM Subhasta s"),
    @NamedQuery(name = "Subhasta.findByCodi", query = "SELECT s FROM Subhasta s WHERE s.codi = :codi"),
    @NamedQuery(name = "Subhasta.findByDatafinalitzacio", query = "SELECT s FROM Subhasta s WHERE s.datafinalitzacio = :datafinalitzacio"),
    @NamedQuery(name = "Subhasta.findByPreupartida", query = "SELECT s FROM Subhasta s WHERE s.preupartida = :preupartida")})
public class Subhasta implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "codi")
    private Integer codi;
    @Basic(optional = false)
    @Column(name = "datafinalitzacio")
    @Temporal(TemporalType.DATE)
    private Date datafinalitzacio;
    @Basic(optional = false)
    @Column(name = "preupartida")
    private BigDecimal preupartida;
    @JoinColumn(name = "productecataleg_cod", referencedColumnName = "numerolot")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Productecataleg productecatalegCod;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "subhasta", fetch = FetchType.LAZY)
    private List<Oferta> ofertaList;

    protected Subhasta() {
    }
/*
    public Subhasta(Integer codi) {
        this.codi = codi;
    }
*/
    public Subhasta(Productecataleg pc, Date datafinalitzacio, BigDecimal preupartida) {
        this.productecatalegCod = pc;
        this.datafinalitzacio = datafinalitzacio;
        this.preupartida = preupartida;
    }

    public Integer getCodi() {
        return codi;
    }

    public void setCodi(Integer codi) {
        this.codi = codi;
    }

    public Date getDatafinalitzacio() {
        return datafinalitzacio;
    }

    public void setDatafinalitzacio(Date datafinalitzacio) {
        this.datafinalitzacio = datafinalitzacio;
    }

    public BigDecimal getPreupartida() {
        return preupartida;
    }

    public void setPreupartida(BigDecimal preupartida) {
        this.preupartida = preupartida;
    }

    public Productecataleg getProductecatalegCod() {
        return productecatalegCod;
    }

    public void setProductecatalegCod(Productecataleg productecatalegCod) {
        this.productecatalegCod = productecatalegCod;
    }

    public List<Oferta> getOfertaList() {
        return ofertaList;
    }

    public void setOfertaList(List<Oferta> ofertaList) {
        this.ofertaList = ofertaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codi != null ? codi.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Subhasta)) {
            return false;
        }
        Subhasta other = (Subhasta) object;
        if ((this.codi == null && other.codi != null) || (this.codi != null && !this.codi.equals(other.codi))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.infomila.modeldedades.Subhasta[ codi=" + codi + " ]";
    }
    
}
