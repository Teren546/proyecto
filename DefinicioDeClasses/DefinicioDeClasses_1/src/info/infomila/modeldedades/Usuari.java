/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.infomila.modeldedades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Adria
 */
@Entity
@Table(name = "usuari")
@NamedQueries({
    @NamedQuery(name = "Usuari.findAll", query = "SELECT u FROM Usuari u"),
    @NamedQuery(name = "Usuari.findByCodi", query = "SELECT u FROM Usuari u WHERE u.codi = :codi"),
    @NamedQuery(name = "Usuari.findByAlias", query = "SELECT u FROM Usuari u WHERE u.alias = :alias"),
    @NamedQuery(name = "Usuari.findByNumeroTarja", query = "SELECT u FROM Usuari u WHERE u.numeroTarja = :numeroTarja"),
    @NamedQuery(name = "Usuari.findByTitular", query = "SELECT u FROM Usuari u WHERE u.titular = :titular"),
    @NamedQuery(name = "Usuari.findByCvv", query = "SELECT u FROM Usuari u WHERE u.cvv = :cvv"),
    @NamedQuery(name = "Usuari.findByCaducitat", query = "SELECT u FROM Usuari u WHERE u.caducitat = :caducitat"),
    @NamedQuery(name = "Usuari.findByTipusTarja", query = "SELECT u FROM Usuari u WHERE u.tipusTarja = :tipusTarja")})
public class Usuari implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "codi")
    private Integer codi;
    @Basic(optional = false)
    @Column(name = "alias")
    private String alias;
    @Basic(optional = false)
    @Column(name = "numero_tarja")
    private String numeroTarja;
    @Basic(optional = false)
    @Column(name = "titular")
    private String titular;
    @Basic(optional = false)
    @Column(name = "cvv")
    private String cvv;
    @Basic(optional = false)
    @Column(name = "caducitat")
    @Temporal(TemporalType.DATE)
    private Date caducitat;
    @Basic(optional = false)
    @Column(name = "tipus_tarja")
    private TipusTarja tipusTarja;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "usuariCod", fetch = FetchType.LAZY)
    private List<Oferta> ofertaList;

    protected Usuari() {
    }
/*
    public Usuari(Integer codi) {
        this.codi = codi;
    }
*/
    public Usuari(String alias, String numeroTarja, String titular, String cvv, Date caducitat, TipusTarja tipusTarja) {
        this.alias = alias;
        this.numeroTarja = numeroTarja;
        this.titular = titular;
        this.cvv = cvv;
        this.caducitat = caducitat;
        this.tipusTarja = tipusTarja;
    }

    public Integer getCodi() {
        return codi;
    }

    public void setCodi(Integer codi) {
        this.codi = codi;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getNumeroTarja() {
        return numeroTarja;
    }

    public void setNumeroTarja(String numeroTarja) {
        this.numeroTarja = numeroTarja;
    }

    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public Date getCaducitat() {
        return caducitat;
    }

    public void setCaducitat(Date caducitat) {
        this.caducitat = caducitat;
    }

    public TipusTarja getTipusTarja() {
        return tipusTarja;
    }

    public void setTipusTarja(TipusTarja tipusTarja) {
        this.tipusTarja = tipusTarja;
    }

    public List<Oferta> getOfertaList() {
        return ofertaList;
    }

    public void setOfertaList(List<Oferta> ofertaList) {
        this.ofertaList = ofertaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codi != null ? codi.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuari)) {
            return false;
        }
        Usuari other = (Usuari) object;
        if ((this.codi == null && other.codi != null) || (this.codi != null && !this.codi.equals(other.codi))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.infomila.modeldedades.Usuari[ codi=" + codi + " ]";
    }
    
}
