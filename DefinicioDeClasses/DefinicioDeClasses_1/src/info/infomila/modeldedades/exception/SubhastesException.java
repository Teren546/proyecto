/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.infomila.modeldedades.exception;

/**
 *
 * @author Adria
 */
public class SubhastesException extends RuntimeException {

    public SubhastesException(String message) {
        super(message);
    }

    public SubhastesException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
