/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.infomila.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

/**
 *
 * @author Adria
 */
@Embeddable
public class Foto implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Basic(optional = false)
    @Column(name = "url", nullable = false, length = 2048)
    private String mURL;
    @Basic(optional = false)
    @Column(name = "caption", nullable = false)
    private String mCaption;
    @JoinColumn(name = "productecataleg_cod", referencedColumnName = "numero_lot", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private ProducteCataleg mProducteCataleg;

    protected Foto() {
    }

    public Foto(String url, String caption) {
        setUrl(url);
        setCaption(caption);
    }

    public String getUrl() {
        return mURL;
    }

    public final void setUrl(String url) {
        if(url == null) throw new SubhastaException("Error, la url no pot ser null");
        if(url.length() < 1 || url.length() > 2048) throw new SubhastaException("Error, la url no esta en el rang permes (1 - 2048) de caracters");
        this.mURL = url;
    }

    public String getCaption() {
        return mCaption;
    }

    public final void setCaption(String caption) {
        if(caption == null) throw new SubhastaException("Error, el caption no pot ser null");
        this.mCaption = caption;
    }

    public ProducteCataleg getProducteCataleg() {
        return mProducteCataleg;
    }

    public void setProducteCataleg(ProducteCataleg mProducteCataleg) {
        if(mProducteCataleg == null) throw new SubhastaException("Error, el producte cataleg no pot ser null");
        this.mProducteCataleg = mProducteCataleg;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.mURL);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Foto other = (Foto) obj;
        if (!Objects.equals(this.mURL, other.mURL)) {
            return false;
        }
        return true;
    }
}
