/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.infomila.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author Adria
 */
@Embeddable
public class OfertaPK implements Serializable{
    @ManyToOne
    @JoinColumn(name="subhasta_cod", referencedColumnName="producte_cataleg")
    private Subhasta mSubhasta;
    @Column(name="codi")
    private int mCodi;

    public OfertaPK(Subhasta subhasta, int i) {
        setmCodi(i);
        setmSubhasta(subhasta);
    }

    protected OfertaPK() {
    }

    public Subhasta getmSubhasta() {
        return mSubhasta;
    }

    public int getmCodi() {
        return mCodi;
    }

    public final void setmSubhasta(Subhasta mSubhasta) {
        if(mSubhasta == null) throw new SubhastaException("Error, la subhasta no pot ser null");
        this.mSubhasta = mSubhasta;
    }

    public final void setmCodi(int mCodi) {
        if(mCodi<=0) throw new SubhastaException("Error, el codi ha de ser estrictament positu");
        this.mCodi = mCodi;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + Objects.hashCode(this.mSubhasta);
        hash = 37 * hash + this.mCodi;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OfertaPK other = (OfertaPK) obj;
        if (!Objects.equals(this.mSubhasta, other.mSubhasta)) {
            return false;
        }
        if (this.mCodi != other.mCodi) {
            return false;
        }
        return true;
    }
    
    
}
