/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.infomila.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 *
 * @author Adria
 */
@Entity
@Table(name = "producte_cataleg")
@NamedQueries({
    @NamedQuery(name = "ProducteCataleg.findAll", query = "SELECT s FROM ProducteCataleg s")
})
public class ProducteCataleg implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "numero_lot")
    private Integer mNumeroLoteria;
    @Basic(optional = false)
    @Column(name = "nom", unique = true, length=64)
    private String mNom;
    @Basic(optional = false)
    @Column(name = "desc_html")
    private String mDescHTML;
    @Basic(optional = false)
    @Column(name = "valor_estimat")
    private BigDecimal mValorEstimat;
    @ElementCollection(fetch = FetchType.LAZY)
    private List<Foto> mLlistaFotos = new ArrayList<Foto>();
    @JoinColumn(name = "categoria", referencedColumnName = "codi")
    @ManyToOne(optional = false)
    private Categoria mCategoria;
    
    
    
    @OneToOne(cascade = CascadeType.PERSIST)
    @PrimaryKeyJoinColumn
    private Subhasta mSubhasta;

    protected ProducteCataleg() {
    }

    public ProducteCataleg(String nom, String deschtml, BigDecimal valorestimat, Categoria c) {
        setNom(nom);
        setDeschtml(deschtml);
        setValorestimat(valorestimat);
        setCategoria(c);
    }

    public Integer getNumerolot() {
        return mNumeroLoteria;
    }

    protected void setNumerolot(Integer numerolot) {
        this.mNumeroLoteria = numerolot;
    }

    public String getNom() {
        return mNom;
    }

    public final void setNom(String nom) {
        if(nom == null) throw new SubhastaException("Error, el nom no pot ser null");
        if(nom.length() < 1 || nom.length() > 63) throw new SubhastaException("Error, el nom ha de estar entre 1 i 64 caracters");
        this.mNom = nom;
    }

    public String getDeschtml() {
        return mDescHTML;
    }

    public final void setDeschtml(String deschtml) {
        this.mDescHTML = deschtml;
    }

    public BigDecimal getValorestimat() {
        return mValorEstimat;
    }

    public final void setValorestimat(BigDecimal valorestimat) {
        if(valorestimat == null) throw new SubhastaException("Error, el valor estimat no pot ser null");
        if(valorestimat.compareTo(new BigDecimal(0)) <= 0) throw new SubhastaException("Error, el valor estimat ha de ser estrictament positiu");
        this.mValorEstimat = valorestimat;
    }

    public Categoria getCategoria() {
        return mCategoria;
    }

    public final void setCategoria(Categoria categoria) {
        if(categoria == null) throw new SubhastaException("Error, categoria no pot ser null");
        this.mCategoria = categoria;
    }
    
    public void addFoto(Foto f){
        if(f == null) throw new SubhastaException("Error, intent d'afegir foto null");
        f.setProducteCataleg(this);
        this.mLlistaFotos.add(f);
    }

    public Subhasta getmSubhasta() {
        return mSubhasta;
    }

    public void setmSubhasta(Subhasta mSubhasta) {
        this.mSubhasta = mSubhasta;
    }
    
    public Foto getFotoAt(int index){
        if(index < 0) return null;
        if(index >= mLlistaFotos.size()) return null;
        return mLlistaFotos.get(index);
    }
    
    public int getNumFotos(){
        return mLlistaFotos.size();
    }
    

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.mNumeroLoteria);
        hash = 71 * hash + Objects.hashCode(this.mNom);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProducteCataleg other = (ProducteCataleg) obj;
        if (!Objects.equals(this.mNumeroLoteria, other.mNumeroLoteria)) {
            return false;
        }
        if (!Objects.equals(this.mNom, other.mNom)) {
            return false;
        }
        return true;
    }
    
    
}
