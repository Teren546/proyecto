/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.infomila.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Adria
 */
public class Subhastacopy implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer mCodi;
    

    private Date mDataFinalitzacio;
    @Basic(optional = false)
    @Column(name = "preu_partida")
    private BigDecimal mPreuPartida;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ofertaPK.mSubhasta")
    private List<Oferta> mLlistaOfertes = new ArrayList<Oferta>(); //Ho faig aixi per en cas de que vngui de hibernate
    
    @OneToOne
    @MapsId
    @JoinColumn(name="producte_cataleg")
    private ProducteCataleg mProducteCataleg;

    protected Subhastacopy() {
    }

    public Subhastacopy(Date datafinalitzacio, BigDecimal preupartida, ProducteCataleg productecatalegCod) {
        setDatafinalitzacio(datafinalitzacio);
        setPreupartida(preupartida);
        setProducteCataleg(productecatalegCod);
    }
    
    public Date getDatafinalitzacio() {
        return mDataFinalitzacio;
    }

    public final void setDatafinalitzacio(Date datafinalitzacio) {
        if(datafinalitzacio == null) throw new SubhastaException("Error, la data finalizacio no pot ser null");
        this.mDataFinalitzacio = datafinalitzacio;
    }

    public BigDecimal getPreupartida() {
        return mPreuPartida;
    }

    public final void setPreupartida(BigDecimal preupartida) {
        if(preupartida == null) throw new SubhastaException("Error, el preu de partida no pot ser null");
        if(preupartida.compareTo(new BigDecimal(0)) <= 0) throw new SubhastaException("Error, el preu de partida ha de ser estrictament positiu");
        
        this.mPreuPartida = preupartida;
    }
    
    protected final void setProducteCataleg(ProducteCataleg pc){
        if(pc == null) throw new SubhastaException("Error, el producte cataleg no pot ser null");
        this.mProducteCataleg = pc;
    }

    public ProducteCataleg getProductecatalegCod() {
        return mProducteCataleg;
    }
    
    public Oferta getOfertaAt(int index){
        return mLlistaOfertes.get(index);
    }
    
    public Oferta getMaxOferta(){
        if(mLlistaOfertes.size() == 0) return null;
        Oferta of = mLlistaOfertes.get(0);
        for(Oferta o:mLlistaOfertes){
            if(of.getImport1().compareTo(o.getImport1()) < 0){
                of = o;
            }
        }
        return of;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + Objects.hashCode(this);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Subhastacopy other = (Subhastacopy) obj;
        if (!Objects.equals(this.mProducteCataleg, other.mProducteCataleg)) {
            return false;
        }
        return true;
    }
    
    

    @Override
    public String toString() {
        return "Subhasta{, datafinalitzacio=" + mDataFinalitzacio + ", preupartida=" + mPreuPartida + ", productecatalegCod=" + mProducteCataleg + ", ofertaList=" + mLlistaOfertes + '}';
    }

    public void addOferta(Oferta of) {
        if(of == null) throw new RuntimeException("Error");
        mLlistaOfertes.add(of);
    }

    public int getNumOfertes() {
        return this.mLlistaOfertes.size();
    }

    public List<Oferta> getLlOfertes() {
        return this.mLlistaOfertes;
    }

    public void setLlOfertes(List<Oferta> llOfertes) {
        this.mLlistaOfertes = llOfertes;
    }

    
}
