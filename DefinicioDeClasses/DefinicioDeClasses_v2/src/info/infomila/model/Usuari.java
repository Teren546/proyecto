/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.infomila.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Adria
 */
@Entity
@Table(name = "USUARI")
@NamedQueries({
    @NamedQuery(name = "Usuari.findAll", query = "SELECT u FROM Usuari u")
})
public class Usuari implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "codi", nullable = false)
    private Integer mCodi;
    @Basic(optional = false)
    @Column(name = "alias", nullable = false, length=64, unique=true)
    private String mAlias;
    @Basic(optional = false)
    @Column(name = "nom", nullable = false, length=64)
    private String mNom;
    @Basic(optional = false)
    @Column(name = "cognoms", nullable = false, length=64)
    private String mCognoms;
    @Basic(optional = false)
    @Column(name = "numero_tarja", nullable = false, length=16)
    private String mNumeroTarja;
    @Basic(optional = false)
    @Column(name = "titular", nullable = false, length=64)
    private String mTitular;
    @Basic(optional = false)
    @Column(name = "cvv", nullable = false)
    private Integer mCVV;
    @Basic(optional = false)
    @Column(name = "caducitat", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date mCaducitat;
    @Basic(optional = false)
    @Column(name = "tipus_tarja", nullable = false)
    @Enumerated(EnumType.STRING)
    private TipusTarja mTipusTarja;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mUsuari")
    private List<Oferta> mLlistaOfertes = new ArrayList<Oferta>();
    
    protected Usuari() {
    }

    public Usuari(String alias, String numero_tarja, String titular, Integer CVV, Date caducitat, TipusTarja tipus, String nom, String cognoms) {
        setAlias(alias);
        setNumero_tarja(numero_tarja);
        setTitular(titular);
        setCVV(CVV);
        setCaducitat(caducitat);
        setTipus(tipus);
        setNom(nom);
        setCognoms(cognoms);
    }

    public Integer getCodi() {
        return mCodi;
    }
    
    public void setCodi(int codi) {
        mCodi = codi;
    }
    
    /**
     * 
     * @param codi 
     */
    protected final void setCodi(Integer codi) {
        this.mCodi = codi;
    }

    public String getAlias() {
        return mAlias;
    }

    public final void setAlias(String alias) {
        if(alias == null) throw new SubhastaException("Error el alias no pot ser null");
        if(alias.length() == 0 || alias.length() > 64) throw new SubhastaException("Error el alias ha de estar entre 1 y 64 caracters");
        this.mAlias = alias;
    }

    public String getNumero_tarja() {
        return mNumeroTarja;
    }

    public final void setNumero_tarja(String numero_tarja) {
        if(numero_tarja == null) throw new SubhastaException("Error numero de tarja no pot ser null");
        if(numero_tarja.length() != 16) throw new SubhastaException("El numero de tarja es de 16 digits");
        this.mNumeroTarja = numero_tarja;
    }

    public String getTitular() {
        return mTitular;
    }

    public final void setTitular(String titular) {
        if(titular == null) throw new SubhastaException("Error el titular no pot ser null");
        if(mAlias.length() == 0 || mAlias.length() > 64) throw new SubhastaException("Error el alias ha de estar entre 1 y 64 caracters");
        this.mTitular = titular;
    }

    public Integer getCVV() {
        return mCVV;
    }

    public final void setCVV(Integer CVV) {
        if(CVV == null) throw new SubhastaException("Error el CVV no pot ser null");
        if(CVV < 0 || CVV > 999) throw new SubhastaException("Error el CVV ha de estar entre 0 i 999");
        this.mCVV = CVV;
    }

    public Date getCaducitat() {
        return mCaducitat;
    }

    public final void setCaducitat(Date caducitat) {
        if(caducitat == null) throw new SubhastaException("Error la caducitat no pot ser null");
        this.mCaducitat = caducitat;
    }

    public TipusTarja getTipus() {
        return mTipusTarja;
    }

    public final void setTipus(TipusTarja tipus) {
        if(tipus == null) throw new SubhastaException("Error el tipus de tarja no pot ser null");
        this.mTipusTarja = tipus;
    }
    
    public String getNom() {
        return mNom;
    }

    public final void setNom(String nom) {
        if(nom == null) throw new SubhastaException("Error nom no pot ser null");
        if(nom.length() < 1 || nom.length() > 63) throw new SubhastaException("El nom no esta en els rang permesos 1 - 63");
        this.mNom = nom;
    }

    public String getCognoms() {
        return mCognoms;
    }

    public final void setCognoms(String cognoms) {
        if(cognoms == null) throw new SubhastaException("Error cognoms no pot ser null");
        if(cognoms.length() < 1 || cognoms.length() > 63) throw new SubhastaException("Els cognoms no esta en els rang permesos 1 - 63");
        this.mCognoms = cognoms;
    }
    
    public void addOferta(Oferta of){
        if(of == null) throw new RuntimeException("Error");
        this.mLlistaOfertes.add(of);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.mCodi);
        hash = 53 * hash + Objects.hashCode(this.mAlias);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Usuari other = (Usuari) obj;
        if (!Objects.equals(this.mCodi, other.mCodi)) {
            return false;
        }
        if (!Objects.equals(this.mAlias, other.mAlias)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Usuari{" + "mCodi=" + mCodi + ", mAlias=" + mAlias + ", mNom=" + mNom + ", mCognoms=" + mCognoms + ", mNumeroTarja=" + mNumeroTarja + ", mTitular=" + mTitular + ", mCVV=" + mCVV + ", mCaducitat=" + mCaducitat + ", mTipusTarja=" + mTipusTarja + ", mLlistaOfertes=" + mLlistaOfertes + '}';
    }

    public boolean containsOferta(Oferta of) {
        return this.mLlistaOfertes.contains(of);
    }
    
    
    
}
