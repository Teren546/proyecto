/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.infomila.probes;

/**
 *
 * @author Adria
 */
public class GeneracioDeInserts {
    
    public static void main(String[] args) {
        String[] categorias = new String[]{"JUEGOS","ARTE","FUTBOL","INFORMATICA"};
        
        String[][] productos = new String[][]
        {
            {"Rayman","World of Warcraft","League of leagends","Uncharted","MGSV"},
            {"El sueño","Los jugadores de cartas","Tres estudios","Number 5"},
            {"Pelota","Camiseta","Botas"},
            {"Portatil","Sobremesa","Grafica Nvidia","Procesador i7"}
        };
        
        String[][] usuaris = new String[][]{
            {"Teren546","Briklindark","Yangus","Dragonslayex"},
            {"Adria","Oriol","David","Victor"},
            {"Gomez","Bernaus","Fernandez","Ocaña"}
        };
        
        for(int i=0; i < 4;i++){
            System.out.println("Insert into usuari(alias,cvv,caducitat,cognoms,nom,numero_tarja,tipus_tarja,titular) "
                    + " values ('"+usuaris[0][i]+"',323,'2016-06-23','"+usuaris[2][i]+"','"+usuaris[1][i]+"','1234567890123456','VISA','Titular');");
        }
        
        int numProducte = 0;
        int numSubhasta = 0;
        for(int i=0; i < categorias.length; i++){
            System.out.println("Insert into categoria(nom) "
                    + "values ('"+categorias[i]+"');");
            for(int x=0; x < productos[i].length; x++){
                int aux = ++numProducte;
                System.out.println("Insert into producte_cataleg(desc_html,nom,valor_estimat,categoria) "
                        + "values ('<html></html>','"+productos[i][x]+"',"+(aux*(x+1))+",'"+(i+1)+"');");
                    System.out.println("Insert into subhasta(data_finalitzacio,preu_partida,producte_cataleg) "
                            + "values ('2016-06-22',5,"+aux+");");
                    System.out.println("Insert into productecataleg_mllistafotos(productecataleg_numero_lot,caption,productecataleg_cod,url) "
                            + "values("+aux+",'blablabla',"+aux+",'https://upload.wikimedia.org/wikipedia/commons/thumb/5/55/Sasso_lungo_da_passo_pordoi.jpg/270px-Sasso_lungo_da_passo_pordoi.jpg');");
                    for(int j=0;j<4;j++){
                        System.out.println("Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) "
                                + "values ("+(j+1)+",'ONLINE',"+(j+10)+","+aux+","+(j+1)+");");
                    }
            }
        }
        
    }
    
}
