/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.infomila.probes;

import info.infomila.model.CanalOferta;
import info.infomila.model.Categoria;
import info.infomila.model.Foto;
import info.infomila.model.Oferta;
import info.infomila.model.ProducteCataleg;
import info.infomila.model.Subhasta;
import info.infomila.model.TipusTarja;
import info.infomila.model.Usuari;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Adria
 */
public class ValidaEstructura {
    public static void main(String[] args) {
        String up = "DefinicioDeClassesPU";
        EntityManagerFactory emf = null;
        EntityManager em = null;
        try {
            em = null;
            emf = null;
            System.out.println("Intent amb " + up);
            emf = Persistence.createEntityManagerFactory(up);
            System.out.println("EntityManagerFactory creada");
            em = emf.createEntityManager();
            System.out.println("EntityManager creat");
            
//            ProducteCataleg pc = em.find(ProducteCataleg.class, 101);
//            System.out.println(pc.getDeschtml());
            
//            em.getTransaction().begin();
//            for(int i=0; i < 10; i++){
//                Categoria c = new Categoria("Categoria " + i, null);
//                Usuari u = new Usuari("Usuari " + i, "1234567890123456", "Titular", 323, new Date(), TipusTarja.VISA, "PEPE", "GOTERA");
//                em.persist(u);
//                em.persist(c);
//                for(int x = 0; x < 10; x++){
//                    ProducteCataleg pc = new ProducteCataleg("ProducteCataleg " + i + "" + x, "<html></html>", new BigDecimal((x*i + 1)),c);
//                    for(int z = 0; z < 5; z++){
//                        Foto f = new Foto("URL" + z, "Descripcio llarga");
//                        pc.addFoto(f);
//                    }
//                    em.persist(pc);
//                    if(i%2 == 0){
//                        for(int q=0; q < 3; q++){
//                            Subhasta sb = new Subhasta(new Date(), new BigDecimal(10), pc);
//                            Oferta of = new Oferta(new BigDecimal(30), CanalOferta.ONLINE, sb, u);
//                            sb.addOferta(of);
//                            em.persist(sb);
//                            
//                        }
//                    }
//                }
//            }
//           
//            em.getTransaction().commit();
        } catch (Exception ex) {
            System.out.println("Exception: " + ex.getMessage());
            System.out.print(ex.getCause() != null ? "Caused by:" + ex.getCause().getMessage() + "\n" : "");
            System.out.println("Traça:");
            ex.printStackTrace();
        } finally {
            if (em != null) {
                if(em.getTransaction().isActive()){
                    em.getTransaction().rollback();
                }
                em.close();
                System.out.println("EntityManager tancat");
            }
            if (emf != null) {
                emf.close();
                System.out.println("EntityManagerFactory tancada");
            }
        }
    }
}
