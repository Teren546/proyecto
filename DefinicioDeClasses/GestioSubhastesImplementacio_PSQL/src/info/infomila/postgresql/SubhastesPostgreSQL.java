/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.infomila.postgresql;

import info.infomila.interficieSubhastes.GestioSubhastesException;
import info.infomila.interficieSubhastes.IGestioSubhastes;
import info.infomila.model.Oferta;
import info.infomila.model.ProducteCataleg;
import info.infomila.model.Subhasta;
import info.infomila.model.Usuari;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.swing.JOptionPane;

/**
 *
 * @author Adria
 */
public class SubhastesPostgreSQL implements IGestioSubhastes {
    
    private EntityManager mConexio;
    private EntityManagerFactory mEntityManagerFactory;
    private File mFitxerPropietats;
    private String mUnitatDePersistencia;
    
    public SubhastesPostgreSQL(){
        
    }
    
    public SubhastesPostgreSQL(SubhastesPostgreSQL sq) throws GestioSubhastesException{
        mConexio = sq.cloneEntityManagerFactory();
    }
    
    /**
     * Fa la conexio contra la BD de PostgreSQL indicada en el fitxer f
     * @param f Fitxer de propietats que conte la informacio referent a la conexio amb la bd
     * @throws info.infomila.interficieSubhastes.GestioSubhastesException En cas de no trobar el fitxer de propietats
     */
    public SubhastesPostgreSQL(File f) throws GestioSubhastesException{
        try {
            mFitxerPropietats = f;
            Properties p = new Properties();
            p.loadFromXML(new FileInputStream(f));
            
            mUnitatDePersistencia = p.getProperty("UP");
//            Properties props = new Properties();
//            props.setProperty("org.hibernate.dialect.PostgreSQL92Dialect", "false"); //Performance for hibernate
            mEntityManagerFactory = Persistence.createEntityManagerFactory(mUnitatDePersistencia);
            mConexio = mEntityManagerFactory.createEntityManager();
        } catch (IOException ex) {
            throw new GestioSubhastesException("Error al buscar el fitxer de propietats", ex);
        } catch (Exception ex){
            throw new GestioSubhastesException("Error al intentar establir la conexio", ex);
        }
    }
    
    @Override
    public void close() throws GestioSubhastesException {
        try{
            if(mConexio != null){
                if(mConexio.isOpen()){
                    mConexio.close();
                }
            }
            if(mEntityManagerFactory != null){
                if(mEntityManagerFactory.isOpen()){
                    mEntityManagerFactory.close();
                }
            }
        }catch(Exception ex){
            throw new GestioSubhastesException("Error en tancar les factories", ex);
        }
    }

    @Override
    public List<Subhasta> getSubhastes() throws GestioSubhastesException {
        try{
            Query q = mConexio.createNamedQuery("Subhasta.findAll");
            List<Subhasta> ll = q.getResultList();
            return ll;
        }catch(Exception ex){
            throw new GestioSubhastesException("Error al recuperar la llista de subhastes", ex);
        }
    }

    @Override
    public void insertSubhasta(Subhasta s) throws GestioSubhastesException {
        try{
            if(s == null) throw new GestioSubhastesException("Error, intent d'inserir una subhasta null");
            mConexio.getTransaction().begin();
            mConexio.persist(s);
            mConexio.getTransaction().commit();
        }catch(Exception ex){
            if(mConexio.getTransaction().isActive()){
                mConexio.getTransaction().rollback();
            }
            throw new GestioSubhastesException("Error al inserir la subhastes", ex);
        }
    }

    @Override
    public void insertOferta(Oferta of) throws GestioSubhastesException {
        try{
            if(of == null) throw new GestioSubhastesException("Error, intent d'inserir una oferta null");
            mConexio.getTransaction().begin();
            mConexio.persist(of);
            mConexio.getTransaction().commit();
        }catch(Exception ex){
            if(mConexio.getTransaction().isActive()){
                mConexio.getTransaction().rollback();
            }
            ex.printStackTrace();
            throw new GestioSubhastesException("Error al inserir la oferta", ex);
        }
    }

    @Override
    public void insertUsuariBD(Usuari u) throws GestioSubhastesException {
        try{
            if(u == null) throw new GestioSubhastesException("Error, intent d'inserir un usuari null");
            mConexio.getTransaction().begin();
            mConexio.persist(u);
            mConexio.flush();
            mConexio.getTransaction().commit();
        }catch(Exception ex){
            if(mConexio.getTransaction().isActive()){
                mConexio.getTransaction().rollback();
            }
            throw new GestioSubhastesException("Error al inserir el usuari", ex);
        }
    }
    
    

    @Override
    public Subhasta getSubhastaByCodi(int codiSub) throws GestioSubhastesException {
        try{
            Query q = mConexio.createNamedQuery("Subhasta.findByCodi");
            q.setParameter("codi", codiSub);
            Subhasta s = (Subhasta) q.getSingleResult();
//            ProducteCataleg pc = s.getProductecatalegCod();
//            mConexio.detach(s);
//            s = mConexio.find(Subhasta.class, pc.getNumerolot());
            return s;
        }catch(Exception ex){
            throw new GestioSubhastesException("Error al recuperar la subhasta", ex);
        }
    }

    @Override
    public List<ProducteCataleg> getProductes() throws GestioSubhastesException {
        try{
           Query q = mConexio.createNamedQuery("ProducteCataleg.findAll"); 
           return q.getResultList();
        }catch(Exception ex){
            throw new GestioSubhastesException("Error al obtenir la llista de productes");
        }
    }

    @Override
    public List<Subhasta> getActiveSubhastes() throws GestioSubhastesException {
        try{
           Query q = mConexio.createNamedQuery("Subhasta.findOpen"); 
           q.setParameter("dataAvui", new Date());
           return q.getResultList();
        }catch(Exception ex){
            throw new GestioSubhastesException("Error al obtenir la llista de productes");
        }
    }

    @Override
    public List<Subhasta> getClosedSubhastes() throws GestioSubhastesException {
        try{
           Query q = mConexio.createNamedQuery("Subhasta.findClosed"); 
           q.setParameter("dataAvui", new Date());
           return q.getResultList();
        }catch(Exception ex){
            throw new GestioSubhastesException("Error al obtenir la llista de productes");
        }
    }

    @Override
    public List<Subhasta> getFilteredSubhastes(Boolean active, String nameORdesc, BigDecimal minValue, BigDecimal maxValue) throws GestioSubhastesException {
        try{
            Query q = null;
            ArrayList<Subhasta> ll = new ArrayList<Subhasta>();
            
            q = mConexio.createNativeQuery("SELECT s.producte_cataleg " +
                    "FROM " +
                    "Subhasta s LEFT OUTER JOIN Oferta o on(s.producte_cataleg = o.subhasta_cod) " +
                    "LEFT OUTER JOIN producte_cataleg pc on(pc.numero_lot = s.producte_cataleg) " +
                    " WHERE " //:name is null or :name='' or 
                        + " (:name is null or :name='' or pc.nom like :name) "
                    + " group by s.producte_cataleg "
                        + " having " 
                        + " (:maxValue is null or MAX(o.import) <= :maxValue or MAX(o.import) is null) "
                        + " AND "
                        + " (:minValue is null or MAX(o.import) >= :minValue or MAX(o.import) is null) "
                        + " order by s.producte_cataleg");
            q.setParameter("name", "%" + nameORdesc + "%");
            q.setParameter("maxValue", maxValue);
            q.setParameter("minValue", minValue);
            List<Object> r = q.getResultList();
            if(active == null){
                for(Object obj:r){
                    Subhasta s = mConexio.find(Subhasta.class, (Integer)obj);
                    ll.add(s);
                }
                return ll;
            }else if(active){
                for(Object obj:r){
                    Subhasta s = mConexio.find(Subhasta.class, (Integer)obj);
                    if(s.getDatafinalitzacio().compareTo(new Date()) > 0){
                        ll.add(s);
                    }
                }
                return ll;
            }else{
                for(Object obj:r){
                    Subhasta s = mConexio.find(Subhasta.class, (Integer)obj);
                    if(s.getDatafinalitzacio().compareTo(new Date()) <= 0){
                        ll.add(s);
                    }
                }
                return ll;
            }
        }catch(Exception ex){
            throw new GestioSubhastesException("Error al recuperar la subhasta", ex);
        }
    }

    @Override
    public List<Usuari> getUsuaris() throws GestioSubhastesException {
        try{
            Query q = mConexio.createNamedQuery("Usuari.findAll");
            return q.getResultList();
        }catch(Exception ex){
            throw new GestioSubhastesException("Error els usuaris", ex);
        }
    }

    @Override
    public void flush() throws GestioSubhastesException {
        try{
            mConexio.getTransaction().begin();
            mConexio.flush();
            mConexio.getTransaction().commit();
        }catch(Exception ex){
            throw new GestioSubhastesException("Error al refrescar", ex);
        }
    }

    @Override
    public Subhasta refreshOfertasFromSubhasta(Subhasta sb) throws GestioSubhastesException {
        try{
//            Query q = mConexio.createNamedQuery("Subhasta.findByCodi");
//            q.setParameter("codi", sb.getCodi());
//            Subhasta sbnova = (Subhasta) q.getSingleResult();
            mConexio.detach(sb);
            sb = mConexio.find(Subhasta.class, sb.getProductecatalegCod().getNumerolot());
            return sb;
        }catch(Exception ex){
            throw new GestioSubhastesException("Error al refrescar", ex);
        }
    }


    public EntityManager cloneEntityManagerFactory() throws GestioSubhastesException {
        return mEntityManagerFactory.createEntityManager();
    }

    @Override
    public void commit() throws GestioSubhastesException {
        try{
            mConexio.getTransaction().begin();
            
            mConexio.getTransaction().commit();
        }catch(Exception ex){
            
        }
    }

    @Override
    public void reconnect() throws GestioSubhastesException {
        if(mConexio != null){
            if(mConexio.isOpen()){
                throw new GestioSubhastesException("Error, la conexio ya estaba oberta");
            }
        }
        try {
            Properties p = new Properties();
            p.loadFromXML(new FileInputStream(mFitxerPropietats));
            
            mUnitatDePersistencia = p.getProperty("UP");
            
            mEntityManagerFactory = Persistence.createEntityManagerFactory(mUnitatDePersistencia);
            mConexio = mEntityManagerFactory.createEntityManager();
        } catch (IOException ex) {
            throw new GestioSubhastesException("Error al buscar el fitxer de propietats", ex);
        } catch (Exception ex){
            throw new GestioSubhastesException("Error al intentar establir la conexio", ex);
        }
    }

    @Override
    public void actualitzarUsuariBD(Usuari u) throws GestioSubhastesException {
        try{
            mConexio.getTransaction().begin();
            
            Usuari actualitzar = mConexio.find(Usuari.class, u.getCodi());
            
            actualitzar.setNom(u.getNom());
            actualitzar.setAlias(u.getAlias());
            actualitzar.setCVV(u.getCVV());
            actualitzar.setCaducitat(u.getCaducitat());
            actualitzar.setCognoms(u.getCognoms());
            actualitzar.setNumero_tarja(u.getNumero_tarja());
            actualitzar.setTipus(u.getTipus());
            actualitzar.setTitular(u.getTitular());
            
            mConexio.getTransaction().commit();
        }catch(Exception ex){
            if(mConexio.getTransaction().isActive()){
                mConexio.getTransaction().rollback();
            }
            throw new GestioSubhastesException("Error al actualitzar usuari", ex);
        }
    }

    @Override
    public Usuari getUsuariByCod(int codi) throws GestioSubhastesException {
        try{
            Usuari u = mConexio.find(Usuari.class, codi);
            if(u == null) throw new GestioSubhastesException("No s'ha trobat el usuari");
            return u;
        }catch(Exception ex){
            throw new GestioSubhastesException("Error al recuperar el usuari", ex);
        }
    }

    @Override
    public List<Subhasta> getSubhastesFromUsuari(int codi) throws GestioSubhastesException {
       try{
           
           Query q = mConexio.createNativeQuery(
                   "select distinct(producte_cataleg) as codiSub\n" +
                    "from \n" +
                    "subhasta sb\n" +
                    "left outer join\n" +
                    "oferta o on(sb.producte_cataleg = o.subhasta_cod)\n" +
                    "where :codi in(o.usuari_cod)\n" +
                    "order by 1"
           );
           q.setParameter("codi", codi);
           List<Integer> ll = q.getResultList();
           ArrayList<Subhasta> subhastesDelUsuari = new ArrayList<Subhasta>();
           for(Integer i: ll){
               subhastesDelUsuari.add(this.getSubhastaByCodi(i));
           }
           return subhastesDelUsuari;
       }catch(Exception ex){
           throw new GestioSubhastesException("Error al recuperar la llista de subhastes del usuari", ex);
       }
    }

    @Override
    public boolean hihaActus() throws GestioSubhastesException {
        try{
           
           return true;
       }catch(Exception ex){
           throw new GestioSubhastesException("Error al recuperar la llista de subhastes del usuari", ex);
       }
    }

    @Override
    public void refresh(Object obj) throws GestioSubhastesException {
        try{
            mConexio.getTransaction().begin();
            mConexio.refresh(obj);
            mConexio.getTransaction().commit();
        }catch(Throwable ex){
            if(mConexio.getTransaction().isActive()){
                mConexio.getTransaction().rollback();
            }
            ex.printStackTrace();
            throw new GestioSubhastesException("Error al refrescar", ex);
        }
    }

    
}

//Query amb jqpl
//                q = mConexio.createQuery("SELECT s FROM Subhasta s "
//                    + "WHERE "
//                        + " (:name is null or :name='' or s.mProducteCataleg.mNom like :name) "
//                        + " AND "
//                        + " (:name is null or :name='' or s.mProducteCataleg.mDescHTML like :name) "
//                        + " AND "
//                        + " (:maxValue is null or s.mPreuPartida <= :maxValue) "
//                        + " AND "
//                        + " (:minValue is null or s.mPreuPartida >= :minValue) ");
