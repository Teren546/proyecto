﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using System.Configuration;
using System.Data.Common;
using System.ComponentModel;
using GestorCatalegAPP.DefinicionsClases;

namespace GestorCatalegAPP.ConexionBD
{
    class GestorBDImplementacio : IGestorBD
    {
        NpgsqlConnection mConexio;
        public GestorBDImplementacio()
        {
            mConexio = new NpgsqlConnection(Properties.Settings.Default.DB_CONN_STRING);
            mConexio.Open();
        }

        public void actualitzarProducte(ProducteCataleg pc)
        {
            NpgsqlTransaction trans = mConexio.BeginTransaction();
            try
            {
                NpgsqlCommand cmd = new NpgsqlCommand();
                cmd.Connection = mConexio;

                cmd.CommandText = @"
                update producte_cataleg set 
                desc_html = @desc,
                nom = @nom,
                valor_estimat = @valor,
                categoria = @categoria
                where numero_lot = @codi
            ";
                cmd.Parameters.AddWithValue("codi", pc.numeroLot);
                cmd.Parameters.AddWithValue("desc", pc.descHTML);
                cmd.Parameters.AddWithValue("nom", pc.nom);
                cmd.Parameters.AddWithValue("valor", pc.valorEstimat);
                cmd.Parameters.AddWithValue("categoria", pc.categoria.codi);

                int i = cmd.ExecuteNonQuery();
                if (i != 1)
                {
                    throw new Exception("Error al actualitzar, numbre de registres afectats = " + i);
                }
                inserirImatgesDeProducte(pc.llistaFotos, pc);

                trans.Commit();
            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw new Exception(ex.Message);
            }
        }

        public void inserirProducte(ProducteCataleg pc)
        {
            NpgsqlTransaction trans = mConexio.BeginTransaction();
            try{
                NpgsqlCommand cmd = new NpgsqlCommand();
                cmd.Connection = mConexio;

                cmd.CommandText = "insert into producte_cataleg (desc_html,nom,valor_estimat,categoria) " +
                    "values (@desc,@nom,@valor,@categoria)";
                cmd.Parameters.AddWithValue("desc", pc.descHTML);
                cmd.Parameters.AddWithValue("nom", pc.nom);
                cmd.Parameters.AddWithValue("valor", pc.valorEstimat);
                cmd.Parameters.AddWithValue("categoria", pc.categoria.codi);

                cmd.ExecuteNonQuery();
                cmd.Dispose();

                cmd = new NpgsqlCommand();
                cmd.Connection = mConexio;
                cmd.CommandText = "select max(numero_lot) from producte_cataleg";
                pc.numeroLot =(int)cmd.ExecuteScalar();

                inserirImatgesDeProducte(pc.llistaFotos, pc);
                trans.Commit();
            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw new Exception(ex.Message);
            }
        }

        public void eliminarProducte(ProducteCataleg pc)
        {
            NpgsqlTransaction trans = mConexio.BeginTransaction();
            try
            {
                NpgsqlCommand cmd = new NpgsqlCommand();
                cmd.Connection = mConexio;

                cmd.CommandText = "delete from productecataleg_mllistafotos where productecataleg_numero_lot = @codi";
                cmd.Parameters.AddWithValue("codi", pc.numeroLot);

                cmd.ExecuteNonQuery();
                cmd.Dispose();

                cmd = new NpgsqlCommand();
                cmd.Connection = mConexio;
                cmd.CommandText = "delete from producte_cataleg where numero_lot = @codi";
                cmd.Parameters.AddWithValue("codi", pc.numeroLot);
                int i = cmd.ExecuteNonQuery();
                if (i != 1)
                {
                    throw new Exception("Error al actualitzar, numbre de registres afectats de producte cataleg = " + i);
                }
                cmd.Dispose();

                trans.Commit();
            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw new Exception(ex.Message);
            }
        }

        public void inserirCategoria(Categoria c)
        {
            NpgsqlTransaction trans = mConexio.BeginTransaction();
            try
            {
                NpgsqlCommand cmd = new NpgsqlCommand();
                cmd.Connection = mConexio;

                cmd.CommandText = "insert into categoria (nom,categoria_pare) " +
                    "values (@nom,@categoria_pare)";
                cmd.Parameters.AddWithValue("nom", c.nom);
                if (c.categoriaPare != null)
                {
                    cmd.Parameters.AddWithValue("categoria_pare", c.categoriaPare.codi);
                }
                else
                {
                    cmd.Parameters.AddWithValue("categoria_pare", null);
                }
                cmd.ExecuteNonQuery();

                cmd = new NpgsqlCommand();
                cmd.Connection = mConexio;
                cmd.CommandText = "select max(codi) from categoria";
                c.codi = (int)cmd.ExecuteScalar();
                trans.Commit();
            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw new Exception(ex.Message);
            }
        }

        public void actualitzarCategoria(Categoria c)
        {
            NpgsqlTransaction trans = mConexio.BeginTransaction();
            try
            {
                NpgsqlCommand cmd = new NpgsqlCommand();
                cmd.Connection = mConexio;

                cmd.CommandText = @"update categoria set nom=@nom,categoria_pare=@categoria_pare where codi = @codi";

                cmd.Parameters.AddWithValue("codi", c.codi);
                cmd.Parameters.AddWithValue("nom", c.nom);
                if (c.categoriaPare != null)
                {
                    cmd.Parameters.AddWithValue("categoria_pare", c.categoriaPare.codi);
                }
                else
                {
                    cmd.Parameters.AddWithValue("categoria_pare", DBNull.Value);
                }
                int i = cmd.ExecuteNonQuery();
                if (i != 1)
                {
                    throw new Exception("Error al actualitzar, numbre de registres afectats = " + i);
                }
                trans.Commit();
            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw new Exception(ex.Message);
            }
        }

        public void eliminarCategoria(Categoria c)
        {
            NpgsqlTransaction trans = mConexio.BeginTransaction();
            try
            {
                NpgsqlCommand cmd = new NpgsqlCommand();
                cmd.Connection = mConexio;

                cmd.CommandText = @"delete from categoria where codi = @codi";

                cmd.Parameters.AddWithValue("codi", c.codi);
                int i = cmd.ExecuteNonQuery();
                if (i != 1)
                {
                    throw new Exception("Error al esborrar, numbre de registres afectats = " + i);
                }

                trans.Commit();
            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw new Exception(ex.Message);
            }
        }

        public BindingList<ProducteCataleg> getLlistaProductes()
        {
            BindingList<ProducteCataleg> ll = new BindingList<ProducteCataleg>();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.Connection = mConexio;

            cmd.CommandText = 
                @"
            SELECT 
                pc.numero_lot as numero_lot, 
                pc.desc_html as desc_html,
                pc.nom as nomp, 
                pc.valor_estimat as valor_estimat,
                c.codi as codiCat,
                c.nom as nomCat,
                sb.producte_cataleg as codiSubhasta,
                sb.data_finalitzacio as datafi,
                MAX(o.import) as maxoferta
                FROM 
                producte_cataleg pc 
                LEFT OUTER JOIN 
                categoria c on(c.codi = pc.categoria)
                LEFT OUTER JOIN 
                subhasta sb on(sb.producte_cataleg = pc.numero_lot)
                left outer join 
                oferta o on(o.subhasta_cod = sb.producte_cataleg)
                group by 
                pc.numero_lot, 
                pc.desc_html,
                pc.nom, 
                pc.valor_estimat,
                c.codi,
                c.nom,
                sb.producte_cataleg,
                sb.data_finalitzacio
                order by
                pc.numero_lot
            ";
            DbDataReader db = cmd.ExecuteReader();
            while (db.Read())
            {
                ProducteCataleg pc = new ProducteCataleg();
                pc.numeroLot = db.GetInt32(db.GetOrdinal("numero_lot"));
                pc.descHTML = db.GetString(db.GetOrdinal("desc_html"));
                pc.nom = db.GetString(db.GetOrdinal("nomp"));
                pc.valorEstimat = db.GetDecimal(db.GetOrdinal("valor_estimat"));
                pc.categoria = new Categoria();
                pc.categoria.nom = db.GetString(db.GetOrdinal("nomCat"));
                pc.categoria.codi = db.GetInt32(db.GetOrdinal("codiCat"));

                Subhasta s = null;
                try
                {
                    int codiSub = db.GetInt32(db.GetOrdinal("codiSubhasta"));
                    if (codiSub != null)
                    {
                        s = new Subhasta();
                        s.codiSub = codiSub;
                        s.data_fi = db.GetDateTime(db.GetOrdinal("datafi"));
                        try
                        {
                            s.preu_Actual = db.GetDecimal(db.GetOrdinal("maxoferta"));
                        }
                        catch (Exception ex) { }
                    }
                }
                catch (Exception ex) { }
                pc.Subhasta = s;
                ll.Add(pc);
            }
            db.Close();
            foreach (ProducteCataleg pc in ll)
            {
                pc.llistaFotos = getLlistaFotosOfProducte(pc);
            }
            return ll;
        }

        public BindingList<Categoria> getLlistaCategories()
        {
            BindingList<Categoria> ll = new BindingList<Categoria>();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.Connection = mConexio;

            cmd.CommandText = @"
                   SELECT c.codi as codi,
                          c.nom as nom,
                          c.categoria_pare as categoria_pare_cod,
                          cc.nom as nom_categoria_pare
                    FROM categoria c left outer join categoria cc on(c.categoria_pare = cc.codi)";
            DbDataReader db = cmd.ExecuteReader();
            while (db.Read())
            {
                Categoria c = new Categoria();
                c.codi = db.GetInt32(db.GetOrdinal("codi"));
                c.nom = db.GetString(db.GetOrdinal("nom"));
                try
                {
                    c.categoriaPare_cod = db.GetInt32(db.GetOrdinal("categoria_pare_cod"));
                    c.categoriaPare = new Categoria();
                    c.categoriaPare.codi = c.categoriaPare_cod;
                    c.categoriaPare.nom = db.GetString(db.GetOrdinal("nom_categoria_pare"));
                }
                catch (Exception ex)
                {
                    //Per si la categoria es nula
                }
                ll.Add(c);
            }
            db.Close();
            return ll;
        }

        public Categoria getCategoriaByCod(int codi)
        {
            Categoria c = null;
            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.Connection = mConexio;

            cmd.CommandText = @"
                        SELECT c.codi as codi,
                          c.nom as nom,
                          c.categoria_pare as categoria_pare_cod,
                          cc.nom as nom_categoria_pare
                        FROM categoria c left outer join categoria cc on(c.categoria_pare = cc.codi) 
                        where c.codi = @codi";
            cmd.Parameters.AddWithValue("codi", codi);
            DbDataReader db = cmd.ExecuteReader();
            while (db.Read())
            {
                c = new Categoria();
                c.codi = db.GetInt32(db.GetOrdinal("codi"));
                c.nom = db.GetString(db.GetOrdinal("nom"));
                try
                {
                    c.categoriaPare_cod = db.GetInt32(db.GetOrdinal("categoria_pare_cod"));
                    c.categoriaPare = new Categoria();
                    c.categoriaPare.codi = c.categoriaPare_cod;
                    c.categoriaPare.nom = db.GetString(db.GetOrdinal("nom_categoria_pare"));
                }
                catch (Exception ex)
                {
                    //Per si la categoria es nula
                }
            }
            db.Close();
            return c;
        }

        public ProducteCataleg getProducteCatalegByCod(int codi)
        {
            ProducteCataleg pc = null;
            NpgsqlCommand cmd = new NpgsqlCommand();

            cmd.Connection = mConexio;

            cmd.CommandText = "SELECT pc.numero_lot as numero_lot, " +
                              "pc.desc_html as desc_html,pc.nom as nomp, " +
                              "pc.valor_estimat as valor_estimat,c.codi as codiCat,c.nom as nomCat " +
                              "FROM producte_cataleg pc LEFT OUTER JOIN categoria c on(c.codi = pc.categoria) " +
                              "WHERE pc.numero_lot = @codi";
            cmd.Parameters.AddWithValue("codi", codi);
            DbDataReader db = cmd.ExecuteReader();
            while (db.Read())
            {
                pc = new ProducteCataleg();
                pc.numeroLot = db.GetInt32(db.GetOrdinal("numero_lot"));
                pc.descHTML = db.GetString(db.GetOrdinal("desc_html"));
                pc.nom = db.GetString(db.GetOrdinal("nomp"));
                pc.valorEstimat = db.GetDecimal(db.GetOrdinal("valor_estimat"));
                pc.categoria = new Categoria();
                pc.categoria.nom = db.GetString(db.GetOrdinal("nomCat"));
                pc.categoria.codi = db.GetInt32(db.GetOrdinal("codiCat"));
                
            }
            db.Close();
            return pc;
        }

        public BindingList<Foto> getLlistaFotosOfProducte(ProducteCataleg pc)
        {
            BindingList<Foto> ll = new BindingList<Foto>();
            NpgsqlCommand cmd = new NpgsqlCommand();
            
            cmd.Connection = mConexio;

            cmd.CommandText = "SELECT url,caption FROM productecataleg_mllistafotos where productecataleg_cod = @codi";
            cmd.Parameters.AddWithValue("codi", pc.numeroLot);
            DbDataReader db = cmd.ExecuteReader();
            while (db.Read())
            {
                Foto f = new Foto();
                f.url = db.GetString(db.GetOrdinal("url"));
                f.caption = db.GetString(db.GetOrdinal("caption"));
                f.producteCataleg = pc;
                ll.Add(f);
            }
            db.Close();
            return ll;
        }

        public void close()
        {
            mConexio.Close();
        }

        public void inserirImatgesDeProducte(BindingList<Foto> ll, ProducteCataleg pc)
        {
                if(ll.Count != 0)
                {
                    NpgsqlCommand cmd = new NpgsqlCommand();

                    cmd.Connection = mConexio;


                    cmd.CommandText = "delete from productecataleg_mllistafotos where productecataleg_numero_lot = @codi";
                    cmd.Parameters.AddWithValue("codi", pc.numeroLot);

                    cmd.ExecuteNonQuery();
                }
            
                foreach (Foto f in ll)
                {
                        NpgsqlCommand cmd = new NpgsqlCommand();

                        cmd.Connection = mConexio;

                        cmd.CommandText = @"Insert into productecataleg_mllistafotos(productecataleg_numero_lot,productecataleg_cod,url,caption) 
                                        values (@codi,@codi2,@url,@caption)";
                        cmd.Parameters.AddWithValue("codi", f.producteCataleg.numeroLot);
                        cmd.Parameters.AddWithValue("codi2", f.producteCataleg.numeroLot);
                        cmd.Parameters.AddWithValue("caption", f.caption);
                        cmd.Parameters.AddWithValue("url", f.url);

                        cmd.ExecuteNonQuery();
                    
                }
            
        }


        public BindingList<ProducteCataleg> getFilteredProductes(estatSubhasta estat, string nom,string desc, Categoria c)
        {
            BindingList<ProducteCataleg> ll = new BindingList<ProducteCataleg>();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.Connection = mConexio;
            /*
            cmd.CommandText = @"SELECT 
                                pc.numero_lot as numero_lot, 
                                pc.desc_html as desc_html,
                                pc.nom as nomp, 
                                pc.valor_estimat as valor_estimat,
                                c.codi as codiCat,
                                c.nom as nomCat,
                                sb.producte_cataleg,
                                sb.data_finalitzacio
                                FROM 
                                producte_cataleg pc 
                                LEFT OUTER JOIN 
                                categoria c on(c.codi = pc.categoria)
                                LEFT OUTER JOIN 
                                subhasta sb on(sb.producte_cataleg = pc.numero_lot)
                                WHERE
                                (@desc='' or @desc is null or pc.desc_html like @desc)
                                and
                                (@nom='' or @nom is null or pc.nom like @nom)
                                and
                                (@codiCat is null or c.codi = @codiCat) ";
             * */
            cmd.CommandText = @"
                SELECT 
                    pc.numero_lot as numero_lot, 
                    pc.desc_html as desc_html,
                    pc.nom as nomp, 
                    pc.valor_estimat as valor_estimat,
                    c.codi as codiCat,
                    c.nom as nomCat,
                    sb.producte_cataleg as codiSubhasta,
                    sb.data_finalitzacio as datafi,
                    MAX(o.import) as maxoferta
                FROM 
                    producte_cataleg pc 
                    LEFT OUTER JOIN 
                    categoria c on(c.codi = pc.categoria)
                    LEFT OUTER JOIN 
                    subhasta sb on(sb.producte_cataleg = pc.numero_lot)
                    left outer join 
                    oferta o on(o.subhasta_cod = sb.producte_cataleg)
                WHERE
                    (@desc='' or @desc is null or pc.desc_html like @desc)
                    and
                    (@nom='' or @nom is null or pc.nom like @nom)
                    and
                    (@codiCat is null or c.codi = @codiCat)
                group by 
                    pc.numero_lot, 
                    pc.desc_html,
                    pc.nom, 
                    pc.valor_estimat,
                    c.codi,
                    c.nom,
                    sb.producte_cataleg,
                    sb.data_finalitzacio
                    order by
                    pc.numero_lot";
            cmd.Parameters.AddWithValue("desc", "%"+desc+"%");
            cmd.Parameters.AddWithValue("nom", "%"+nom+"%");
            if (c != null)
            {
                cmd.Parameters.AddWithValue("codiCat", c.codi);
            }
            else
            {
                cmd.Parameters.AddWithValue("codiCat", DBNull.Value);
            }

            DbDataReader db = cmd.ExecuteReader();
            while (db.Read())
            {
                ProducteCataleg pc = new ProducteCataleg();
                pc.numeroLot = db.GetInt32(db.GetOrdinal("numero_lot"));
                pc.descHTML = db.GetString(db.GetOrdinal("desc_html"));
                pc.nom = db.GetString(db.GetOrdinal("nomp"));
                pc.valorEstimat = db.GetDecimal(db.GetOrdinal("valor_estimat"));
                pc.categoria = new Categoria();
                pc.categoria.nom = db.GetString(db.GetOrdinal("nomCat"));
                pc.categoria.codi = db.GetInt32(db.GetOrdinal("codiCat"));
                Subhasta s = null;
                try
                {
                    int codiSub = db.GetInt32(db.GetOrdinal("codiSubhasta"));
                    if (codiSub != null)
                    {
                        s = new Subhasta();
                        s.codiSub = codiSub;
                        s.data_fi = db.GetDateTime(db.GetOrdinal("datafi"));
                        try
                        {
                            s.preu_Actual = db.GetDecimal(db.GetOrdinal("maxoferta"));
                        }
                        catch (Exception ex) { }
                    }
                }
                catch (Exception ex) { }
                pc.Subhasta = s;
                if (estat == estatSubhasta.TOTS)
                {
                    ll.Add(pc);
                }
                else
                {
                    if (estat == pc.estat)
                    {
                        ll.Add(pc);
                    }
                }
            }
            db.Close();
            foreach (ProducteCataleg pc in ll)
            {
                pc.llistaFotos = getLlistaFotosOfProducte(pc);
            }
            return ll;
        }

        private bool teSubhastesActives(ProducteCataleg pc)
        {
            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.Connection = mConexio;

            cmd.CommandText = @"SELECT 1
                                FROM subhasta
                                WHERE data_finalitzacio > CURRENT_TIMESTAMP and producte_cataleg = @codi";
            cmd.Parameters.AddWithValue("codi",pc.numeroLot);

            DbDataReader db = cmd.ExecuteReader();
            while (db.Read())
            {
                return true;
            }
            return false;
        }

        private bool teSubhastesTancades(ProducteCataleg pc)
        {
            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.Connection = mConexio;

            cmd.CommandText = @"SELECT 1
                                FROM subhasta
                                WHERE data_finalitzacio <= CURRENT_TIMESTAMP and producte_cataleg = @codi";
            cmd.Parameters.AddWithValue("codi", pc.numeroLot);

            DbDataReader db = cmd.ExecuteReader();
            while (db.Read())
            {
                return true;
            }
            return false;
        }
    }
}
