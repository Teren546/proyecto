﻿using GestorCatalegAPP.DefinicionsClases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorCatalegAPP
{
    
    public interface IGestorBD
    {
        BindingList<ProducteCataleg> getLlistaProductes();

        BindingList<Categoria> getLlistaCategories();

        BindingList<Foto> getLlistaFotosOfProducte(ProducteCataleg pc);

        BindingList<ProducteCataleg> getFilteredProductes(estatSubhasta estat, string nom,string desc, Categoria c);

        Categoria getCategoriaByCod(int codi);

        ProducteCataleg getProducteCatalegByCod(int codi);

        void actualitzarProducte(ProducteCataleg pc);

        void inserirProducte(ProducteCataleg pc);

        void inserirImatgesDeProducte(BindingList<Foto> ll, ProducteCataleg pc);

        void eliminarProducte(ProducteCataleg pc);

        void inserirCategoria(Categoria c);

        void actualitzarCategoria(Categoria c);

        void eliminarCategoria(Categoria c);

        void close();
    }
}
