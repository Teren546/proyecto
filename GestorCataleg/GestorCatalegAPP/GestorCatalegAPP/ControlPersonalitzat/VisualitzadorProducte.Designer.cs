﻿namespace GestorCatalegAPP.ControlPersonalitzat
{
    partial class VisualitzadorProducte
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.lblNomProducte = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblNomCategoria = new System.Windows.Forms.Label();
            this.pnlImatge1 = new System.Windows.Forms.Panel();
            this.pnlImatge2 = new System.Windows.Forms.Panel();
            this.pnlImatge3 = new System.Windows.Forms.Panel();
            this.pnlImatge4 = new System.Windows.Forms.Panel();
            this.pnlImatge5 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.lblPreuEstimat = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblPreuActual = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.pnlImatge1.SuspendLayout();
            this.pnlImatge2.SuspendLayout();
            this.pnlImatge3.SuspendLayout();
            this.pnlImatge4.SuspendLayout();
            this.pnlImatge5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(780, 218);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lblPreuActual);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.lblPreuEstimat);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.lblNomCategoria);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.lblNomProducte);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(780, 87);
            this.panel2.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.pnlImatge5);
            this.panel3.Controls.Add(this.pnlImatge4);
            this.panel3.Controls.Add(this.pnlImatge3);
            this.panel3.Controls.Add(this.pnlImatge2);
            this.panel3.Controls.Add(this.pnlImatge1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 87);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(780, 131);
            this.panel3.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nom del producte:";
            // 
            // lblNomProducte
            // 
            this.lblNomProducte.AutoSize = true;
            this.lblNomProducte.Location = new System.Drawing.Point(133, 4);
            this.lblNomProducte.Name = "lblNomProducte";
            this.lblNomProducte.Size = new System.Drawing.Size(46, 17);
            this.lblNomProducte.TabIndex = 1;
            this.lblNomProducte.Text = "label2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(54, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Categoria:";
            // 
            // lblNomCategoria
            // 
            this.lblNomCategoria.AutoSize = true;
            this.lblNomCategoria.Location = new System.Drawing.Point(133, 21);
            this.lblNomCategoria.Name = "lblNomCategoria";
            this.lblNomCategoria.Size = new System.Drawing.Size(46, 17);
            this.lblNomCategoria.TabIndex = 3;
            this.lblNomCategoria.Text = "label3";
            // 
            // pnlImatge1
            // 
            this.pnlImatge1.Controls.Add(this.pictureBox1);
            this.pnlImatge1.Location = new System.Drawing.Point(6, 3);
            this.pnlImatge1.Name = "pnlImatge1";
            this.pnlImatge1.Size = new System.Drawing.Size(148, 125);
            this.pnlImatge1.TabIndex = 0;
            // 
            // pnlImatge2
            // 
            this.pnlImatge2.Controls.Add(this.pictureBox2);
            this.pnlImatge2.Location = new System.Drawing.Point(160, 3);
            this.pnlImatge2.Name = "pnlImatge2";
            this.pnlImatge2.Size = new System.Drawing.Size(148, 125);
            this.pnlImatge2.TabIndex = 1;
            // 
            // pnlImatge3
            // 
            this.pnlImatge3.Controls.Add(this.pictureBox3);
            this.pnlImatge3.Location = new System.Drawing.Point(314, 3);
            this.pnlImatge3.Name = "pnlImatge3";
            this.pnlImatge3.Size = new System.Drawing.Size(148, 125);
            this.pnlImatge3.TabIndex = 2;
            // 
            // pnlImatge4
            // 
            this.pnlImatge4.Controls.Add(this.pictureBox4);
            this.pnlImatge4.Location = new System.Drawing.Point(468, 3);
            this.pnlImatge4.Name = "pnlImatge4";
            this.pnlImatge4.Size = new System.Drawing.Size(148, 125);
            this.pnlImatge4.TabIndex = 3;
            // 
            // pnlImatge5
            // 
            this.pnlImatge5.Controls.Add(this.pictureBox5);
            this.pnlImatge5.Location = new System.Drawing.Point(622, 3);
            this.pnlImatge5.Name = "pnlImatge5";
            this.pnlImatge5.Size = new System.Drawing.Size(148, 125);
            this.pnlImatge5.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(33, 38);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "Valor estimat:";
            // 
            // lblPreuEstimat
            // 
            this.lblPreuEstimat.AutoSize = true;
            this.lblPreuEstimat.Location = new System.Drawing.Point(133, 38);
            this.lblPreuEstimat.Name = "lblPreuEstimat";
            this.lblPreuEstimat.Size = new System.Drawing.Size(46, 17);
            this.lblPreuEstimat.TabIndex = 5;
            this.lblPreuEstimat.Text = "label3";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(148, 125);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(148, 125);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox3.Location = new System.Drawing.Point(0, 0);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(148, 125);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 0;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox4.Location = new System.Drawing.Point(0, 0);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(148, 125);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 0;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox5.Location = new System.Drawing.Point(0, 0);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(148, 125);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 0;
            this.pictureBox5.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(43, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Preu actual:";
            // 
            // lblPreuActual
            // 
            this.lblPreuActual.AutoSize = true;
            this.lblPreuActual.Location = new System.Drawing.Point(134, 55);
            this.lblPreuActual.Name = "lblPreuActual";
            this.lblPreuActual.Size = new System.Drawing.Size(46, 17);
            this.lblPreuActual.TabIndex = 7;
            this.lblPreuActual.Text = "label5";
            // 
            // VisualitzadorProducte
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Name = "VisualitzadorProducte";
            this.Size = new System.Drawing.Size(780, 218);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.pnlImatge1.ResumeLayout(false);
            this.pnlImatge2.ResumeLayout(false);
            this.pnlImatge3.ResumeLayout(false);
            this.pnlImatge4.ResumeLayout(false);
            this.pnlImatge5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel pnlImatge5;
        private System.Windows.Forms.Panel pnlImatge4;
        private System.Windows.Forms.Panel pnlImatge3;
        private System.Windows.Forms.Panel pnlImatge2;
        private System.Windows.Forms.Panel pnlImatge1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblPreuEstimat;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblNomCategoria;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblNomProducte;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblPreuActual;
        private System.Windows.Forms.Label label3;
    }
}
