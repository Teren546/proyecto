﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GestorCatalegAPP.DefinicionsClases;

namespace GestorCatalegAPP.ControlPersonalitzat
{
    public partial class VisualitzadorProducte : UserControl
    {
        
        private ProducteCataleg mProducte;
        public ProducteCataleg Producte 
        {
            get
            {
                return mProducte;
            }
            set
            {
                if (value == null)
                {
                    lblNomCategoria.Text = "";
                    lblNomProducte.Text = "";
                    lblPreuEstimat.Text = "";
                    lblPreuActual.Text = "";
                    return;
                }
                mProducte = value;
                for (int i = 0; i < 5; i++)
                {
                    switch (i)
                    {
                        case 0:
                            pictureBox1.ImageLocation = null;
                            break;
                        case 1:
                            pictureBox2.ImageLocation = null;
                            break;
                        case 2:
                            pictureBox3.ImageLocation = null;
                            break;
                        case 3:
                            pictureBox4.ImageLocation = null;
                            break;
                        case 4:
                            pictureBox5.ImageLocation = null;
                            break;
                    }
                }
                int cont = 0;
                foreach (Foto f in mProducte.llistaFotos)
                {
                    switch (cont)
                    {
                        case 0:
                            pictureBox1.ImageLocation = f.url;
                            break;
                        case 1:
                            pictureBox2.ImageLocation = f.url;
                            break;
                        case 2:
                            pictureBox3.ImageLocation = f.url;
                            break;
                        case 3:
                            pictureBox4.ImageLocation = f.url;
                            break;
                        case 4:
                            pictureBox5.ImageLocation = f.url;
                            break;
                        default:
                            break;
                    }
                    cont++;
                }
                lblNomCategoria.Text = mProducte.nomcategoria;
                lblNomProducte.Text = mProducte.nom;
                lblPreuEstimat.Text = mProducte.valorEstimat.ToString();
                try
                {
                    lblPreuActual.Text = mProducte.PreuFinal.ToString();
                }
                catch (Exception ex)
                {
                    lblPreuActual.Text = "";
                }
                mProducte = value;
            }
        }


        public VisualitzadorProducte()
        {
            InitializeComponent();
        }
    }
}
