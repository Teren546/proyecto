﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GestorCatalegAPP.DefinicionsClases
{
    public class Foto
    {
        public ProducteCataleg producteCataleg { get; set; }

        public string caption { get; set; }

        public string url { get; set; }

        // override object.Equals
        public override bool Equals(object obj)
        {
            //       
            // See the full list of guidelines at
            //   http://go.microsoft.com/fwlink/?LinkID=85237  
            // and also the guidance for operator== at
            //   http://go.microsoft.com/fwlink/?LinkId=85238
            //

            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            // TODO: write your implementation of Equals() here
            Foto f = (Foto)obj;
            if (!url.Equals(f.url)) return false;
            return true;
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            // TODO: write your implementation of GetHashCode() here
            throw new NotImplementedException();
            return base.GetHashCode();
        }
    }
}
