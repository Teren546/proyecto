﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GestorCatalegAPP.DefinicionsClases;
using System.ComponentModel;

namespace GestorCatalegAPP
{
    public class ProducteCataleg
    {

        public int numeroLot { get; set; }

        public string descHTML { get; set; }

        public string nom { get; set; }

        public Decimal valorEstimat { get; set; }

        public Categoria categoria { get; set; }

        public string nomcategoria
        { 
            get 
            { 
                if(categoria != null){
                    return categoria.nom;
                }else{
                    return null;
                }
            } 
        }

        private BindingList<Foto> mLlistaFotos = new BindingList<Foto>();
        public BindingList<Foto> llistaFotos { 
            get
            {
                if (mLlistaFotos == null) mLlistaFotos = new BindingList<Foto>();
                return mLlistaFotos;
            }
            set
            {
                mLlistaFotos = value;
            }
        }
        public Subhasta Subhasta
        {
            get;
            set;
        }

        public Decimal? PreuFinal
        {
            get
            {
                if (Subhasta == null)
                {
                    return null;
                }
                else
                {
                    if (estat == estatSubhasta.VENUT)
                    {
                        return Subhasta.preu_Actual;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }


        
        public estatSubhasta estat {
            get
            {
                if (Subhasta == null)
                {
                    return estatSubhasta.PENDENT;
                }
                else if (Subhasta.data_fi.CompareTo(DateTime.Now) <= 0 && Subhasta.preu_Actual == 0)
                {
                    return estatSubhasta.NO_VENUT;
                }
                else if (Subhasta.data_fi.CompareTo(DateTime.Now) <= 0)
                {
                    return estatSubhasta.VENUT;
                }
                else if(Subhasta.data_fi.CompareTo(DateTime.Now) > 0)
                {
                    return estatSubhasta.EN_SUBHASTA;
                }
                else
                {
                    return estatSubhasta.TOTS;
                }
            }
            
       }


        // override object.Equals
        public override bool Equals(object obj)
        {
            //       
            // See the full list of guidelines at
            //   http://go.microsoft.com/fwlink/?LinkID=85237  
            // and also the guidance for operator== at
            //   http://go.microsoft.com/fwlink/?LinkId=85238
            //

            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            // TODO: write your implementation of Equals() here
            ProducteCataleg pc = (ProducteCataleg)obj;
            if (pc.numeroLot != this.numeroLot)
            {
                return false;
            }
            return true;
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            // TODO: write your implementation of GetHashCode() here
            throw new NotImplementedException();
            return base.GetHashCode();
        }

    }
}
