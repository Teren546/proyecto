﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorCatalegAPP.DefinicionsClases
{
    public class Subhasta
    {
        public int codiSub { get; set; }

        public DateTime data_fi { get; set; }

        public Decimal preu_Actual { get; set; }

    }
}
