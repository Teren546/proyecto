﻿using GestorCatalegAPP.DefinicionsClases;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestorCatalegAPP
{
    public partial class EdicioDeProducteCataleg : Form
    {
        private IGestorBD mConexio;
        private BindingList<Categoria> mLlCategoria;
        private ProducteCataleg mProducteActual;
        private Boolean esUpdate;
        private Boolean esGuardar = false;

        public EdicioDeProducteCataleg()
        {
            InitializeComponent();
        }

        public EdicioDeProducteCataleg(IGestorBD mConexio, BindingList<Categoria> mLlCategoria, ProducteCataleg pc)
        {
            InitializeComponent();
            if (pc == null)
            {
                mProducteActual = new ProducteCataleg();
                mProducteActual.llistaFotos = new BindingList<Foto>();
                esUpdate = false;
            }
            else 
            {
                mProducteActual = pc;
                if (mProducteActual.llistaFotos == null)
                {
                    mProducteActual.llistaFotos = new BindingList<Foto>();
                }
                esUpdate = true; 
            }
            // TODO: Complete member initialization
            this.mConexio = mConexio;
            this.mLlCategoria = mLlCategoria;
        }

        private void EdicioDeProducteCataleg_Load(object sender, EventArgs e)
        {
            cboCategoria.DataSource = mLlCategoria;
            cboCategoria.DisplayMember = "nom";
            lsbURLS.DataSource = mProducteActual.llistaFotos;
            lsbURLS.DisplayMember = "url";
            if (esUpdate)
            {
                txtNom.Text = mProducteActual.nom;
                txtDescripcio.Text = mProducteActual.descHTML;
                txtValorEstimat.Text = mProducteActual.valorEstimat.ToString();
                cboCategoria.SelectedIndex = mLlCategoria.IndexOf(mProducteActual.categoria);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (txtURL.Text.Length != 0 && txtDescImatge.Text.Length != 0)
            {
                Foto f = new Foto();
                f.caption = txtDescImatge.Text;
                f.url = txtURL.Text;
                f.producteCataleg = mProducteActual;
                if (mProducteActual.llistaFotos.Contains(f))
                {
                    MessageBox.Show("Aquesta imatge ja ha estat inserida");
                }
                else
                {
                    mProducteActual.llistaFotos.Add(f);
                }
            }
            else
            {
                MessageBox.Show("La descripcio i la url han de contenir alguna cosa");
            }
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (lsbURLS.SelectedIndex >= 0)
            {
                mProducteActual.llistaFotos.RemoveAt(lsbURLS.SelectedIndex);
            }
        }

        private void EdicioDeProducteCataleg_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!esGuardar)
            {
                DialogResult d = MessageBox.Show("Es perdran els cambis esteu segurs?", "Atencio", MessageBoxButtons.OKCancel);
                if (d != System.Windows.Forms.DialogResult.OK)
                {
                    e.Cancel = true;
                }
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (comprobarValors())
            {
                try
                {
                    mProducteActual.nom = txtNom.Text;
                    mProducteActual.categoria = (Categoria)cboCategoria.SelectedItem;
                    mProducteActual.descHTML = txtDescripcio.Text;
                    mProducteActual.valorEstimat = Decimal.Parse(txtValorEstimat.Text);
                    if (!esUpdate)
                    {
                        mConexio.inserirProducte(mProducteActual);
                    }
                    else
                    {
                        mConexio.actualitzarProducte(mProducteActual);
                    }
                    if (esUpdate)
                    {
                        MessageBox.Show("Actualitzat amb exit");
                    }
                    else 
                    { 
                        MessageBox.Show("Inserit amb exit");
                    }
                    esGuardar = true;
                    this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Els camps no son valids");
            }
        }

        private bool comprobarValors()
        {
            if (txtNom.Text.Length == 0)
            {
                return false;
            }
            if (txtDescripcio.Text.Length == 0)
            {
                return false;
            }
            if (cboCategoria.SelectedIndex < 0)
            {
                return false;
            }
            try
            {
                Decimal.Parse(txtValorEstimat.Text);
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                const int port = 22;
                const string host = "92.222.27.83";
                const string username = "m2.agomez";
                const string password = "48235541X";
                const string workingdirectory = "public_html";
                string uploadfile = openFileDialog1.FileName;
                string nomArxiu = openFileDialog1.SafeFileName;

                Console.WriteLine("Creating client and connecting");
                using (var client = new SftpClient(host, port, username, password))
                {
                    client.Connect();
                    Console.WriteLine("Connected to {0}", host);

                    client.ChangeDirectory(workingdirectory);
                    Console.WriteLine("Changed directory to {0}", workingdirectory);

                    using (var fileStream = openFileDialog1.OpenFile())
                    {
                        Console.WriteLine("Uploading {0} ({1:N0} bytes)",
                                            uploadfile, fileStream.Length);
                        client.BufferSize = 4 * 1024; // bypass Payload error large files
                        client.UploadFile(fileStream, Path.GetFileName(uploadfile));
                    }
                }
                txtDescImatge.Text = "";
                txtURL.Text = "http://92.222.27.83/~m2.agomez/" + nomArxiu;
            }
        }

        private int subirArchivoFTP(string arxiu)
        {
            try
            {
                FtpWebRequest ftp = (FtpWebRequest)FtpWebRequest.Create("sftp://92.222.27.83/public_html/" + arxiu);
                
                //Entrem dades d'usuari
                NetworkCredential cr = new NetworkCredential("m2-agomez", "48235541X");
                ftp.Credentials = cr;
                
                //Pujem arxius
                ftp.Method = WebRequestMethods.Ftp.UploadFile;

                ftp.UsePassive = true;
                ftp.UseBinary = true;
                ftp.KeepAlive = true;

                FileStream stream = File.OpenRead(arxiu);

                byte[] buffer = new byte[stream.Length];

                stream.Read(buffer, 0, buffer.Length);

                stream.Close();

                Stream reqStream = ftp.GetRequestStream();

                reqStream.Write(buffer, 0, buffer.Length);

                reqStream.Flush();

                reqStream.Close();

                return 0;
            }
            catch (Exception ex)
            {

                return -1;
            }
        }
    }
}
