﻿namespace GestorCatalegAPP
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabs = new System.Windows.Forms.TabControl();
            this.tabGestioProductes = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgvProductesCataleg = new System.Windows.Forms.DataGridView();
            this.CodigoProducto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NombreProducto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ValorEstimadoProducto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CategoriaProducto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PreuFinal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btnNouProducte = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDescProd = new System.Windows.Forms.TextBox();
            this.rdbTots = new System.Windows.Forms.RadioButton();
            this.txtNomDesc = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cboCategoria = new System.Windows.Forms.ComboBox();
            this.rdbPendent = new System.Windows.Forms.RadioButton();
            this.rdbVenut = new System.Windows.Forms.RadioButton();
            this.rdbEnSubhasta = new System.Windows.Forms.RadioButton();
            this.btnNetejarFiltre = new System.Windows.Forms.Button();
            this.btnFiltrar = new System.Windows.Forms.Button();
            this.lblFiltres = new System.Windows.Forms.Label();
            this.tabGestioCategorias = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.dgvCategories = new System.Windows.Forms.DataGridView();
            this.CodiCategoria = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NomCategoria = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NomCategoriaPare = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnNetejarCategories = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.btnAfegirCategoria = new System.Windows.Forms.Button();
            this.btnActualizarCategoria = new System.Windows.Forms.Button();
            this.txtNomCategoria = new System.Windows.Forms.TextBox();
            this.lblNomCategoria = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cboCateogoriesPare = new System.Windows.Forms.ComboBox();
            this.visProducte = new GestorCatalegAPP.ControlPersonalitzat.VisualitzadorProducte();
            this.tabs.SuspendLayout();
            this.tabGestioProductes.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProductesCataleg)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabGestioCategorias.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCategories)).BeginInit();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabs
            // 
            this.tabs.Controls.Add(this.tabGestioProductes);
            this.tabs.Controls.Add(this.tabGestioCategorias);
            this.tabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabs.Location = new System.Drawing.Point(0, 0);
            this.tabs.Name = "tabs";
            this.tabs.SelectedIndex = 0;
            this.tabs.Size = new System.Drawing.Size(1311, 622);
            this.tabs.TabIndex = 0;
            // 
            // tabGestioProductes
            // 
            this.tabGestioProductes.Controls.Add(this.panel1);
            this.tabGestioProductes.Controls.Add(this.panel2);
            this.tabGestioProductes.Location = new System.Drawing.Point(4, 25);
            this.tabGestioProductes.Name = "tabGestioProductes";
            this.tabGestioProductes.Padding = new System.Windows.Forms.Padding(3);
            this.tabGestioProductes.Size = new System.Drawing.Size(1303, 593);
            this.tabGestioProductes.TabIndex = 0;
            this.tabGestioProductes.Text = "Gestio Productes";
            this.tabGestioProductes.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgvProductesCataleg);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(213, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1087, 587);
            this.panel1.TabIndex = 0;
            // 
            // dgvProductesCataleg
            // 
            this.dgvProductesCataleg.AllowUserToAddRows = false;
            this.dgvProductesCataleg.AllowUserToDeleteRows = false;
            this.dgvProductesCataleg.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvProductesCataleg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProductesCataleg.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CodigoProducto,
            this.NombreProducto,
            this.ValorEstimadoProducto,
            this.CategoriaProducto,
            this.PreuFinal});
            this.dgvProductesCataleg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvProductesCataleg.Location = new System.Drawing.Point(0, 0);
            this.dgvProductesCataleg.MultiSelect = false;
            this.dgvProductesCataleg.Name = "dgvProductesCataleg";
            this.dgvProductesCataleg.ReadOnly = true;
            this.dgvProductesCataleg.RowTemplate.Height = 24;
            this.dgvProductesCataleg.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvProductesCataleg.Size = new System.Drawing.Size(1087, 340);
            this.dgvProductesCataleg.TabIndex = 1;
            this.dgvProductesCataleg.SelectionChanged += new System.EventHandler(this.dgvProductesCataleg_SelectionChanged);
            // 
            // CodigoProducto
            // 
            this.CodigoProducto.DataPropertyName = "numeroLot";
            this.CodigoProducto.HeaderText = "Codi";
            this.CodigoProducto.Name = "CodigoProducto";
            this.CodigoProducto.ReadOnly = true;
            // 
            // NombreProducto
            // 
            this.NombreProducto.DataPropertyName = "nom";
            this.NombreProducto.HeaderText = "Nom";
            this.NombreProducto.Name = "NombreProducto";
            this.NombreProducto.ReadOnly = true;
            // 
            // ValorEstimadoProducto
            // 
            this.ValorEstimadoProducto.DataPropertyName = "valorEstimat";
            this.ValorEstimadoProducto.HeaderText = "ValorEstimat";
            this.ValorEstimadoProducto.Name = "ValorEstimadoProducto";
            this.ValorEstimadoProducto.ReadOnly = true;
            // 
            // CategoriaProducto
            // 
            this.CategoriaProducto.DataPropertyName = "nomcategoria";
            this.CategoriaProducto.HeaderText = "Categoria";
            this.CategoriaProducto.Name = "CategoriaProducto";
            this.CategoriaProducto.ReadOnly = true;
            // 
            // PreuFinal
            // 
            this.PreuFinal.DataPropertyName = "PreuFinal";
            this.PreuFinal.HeaderText = "Preu final";
            this.PreuFinal.Name = "PreuFinal";
            this.PreuFinal.ReadOnly = true;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.panel7);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 340);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1087, 247);
            this.panel3.TabIndex = 0;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.label1);
            this.panel7.Controls.Add(this.btnNouProducte);
            this.panel7.Controls.Add(this.button1);
            this.panel7.Controls.Add(this.button2);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(285, 247);
            this.panel7.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(225, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Crear/Modificar/Eliminar productes";
            // 
            // btnNouProducte
            // 
            this.btnNouProducte.Location = new System.Drawing.Point(6, 39);
            this.btnNouProducte.Name = "btnNouProducte";
            this.btnNouProducte.Size = new System.Drawing.Size(252, 63);
            this.btnNouProducte.TabIndex = 2;
            this.btnNouProducte.Text = "Nou Producte";
            this.btnNouProducte.UseVisualStyleBackColor = true;
            this.btnNouProducte.Click += new System.EventHandler(this.btnNouProducte_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 108);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(252, 63);
            this.button1.TabIndex = 3;
            this.button1.Text = "Eliminar seleccio";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(6, 177);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(252, 63);
            this.button2.TabIndex = 4;
            this.button2.Text = "Editar seleccio";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Silver;
            this.panel4.Controls.Add(this.visProducte);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(285, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(802, 247);
            this.panel4.TabIndex = 1;
            this.panel4.Paint += new System.Windows.Forms.PaintEventHandler(this.panel4_Paint);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.txtDescProd);
            this.panel2.Controls.Add(this.rdbTots);
            this.panel2.Controls.Add(this.txtNomDesc);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.cboCategoria);
            this.panel2.Controls.Add(this.rdbPendent);
            this.panel2.Controls.Add(this.rdbVenut);
            this.panel2.Controls.Add(this.rdbEnSubhasta);
            this.panel2.Controls.Add(this.btnNetejarFiltre);
            this.panel2.Controls.Add(this.btnFiltrar);
            this.panel2.Controls.Add(this.lblFiltres);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(210, 587);
            this.panel2.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 202);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 17);
            this.label5.TabIndex = 12;
            this.label5.Text = "Desc";
            // 
            // txtDescProd
            // 
            this.txtDescProd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDescProd.Location = new System.Drawing.Point(9, 222);
            this.txtDescProd.Name = "txtDescProd";
            this.txtDescProd.Size = new System.Drawing.Size(186, 22);
            this.txtDescProd.TabIndex = 11;
            // 
            // rdbTots
            // 
            this.rdbTots.AutoSize = true;
            this.rdbTots.Location = new System.Drawing.Point(9, 120);
            this.rdbTots.Name = "rdbTots";
            this.rdbTots.Size = new System.Drawing.Size(57, 21);
            this.rdbTots.TabIndex = 10;
            this.rdbTots.TabStop = true;
            this.rdbTots.Text = "Tots";
            this.rdbTots.UseVisualStyleBackColor = true;
            // 
            // txtNomDesc
            // 
            this.txtNomDesc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNomDesc.Location = new System.Drawing.Point(9, 175);
            this.txtNomDesc.Name = "txtNomDesc";
            this.txtNomDesc.Size = new System.Drawing.Size(186, 22);
            this.txtNomDesc.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 155);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "Nom";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 247);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "Categoria";
            // 
            // cboCategoria
            // 
            this.cboCategoria.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboCategoria.FormattingEnabled = true;
            this.cboCategoria.Location = new System.Drawing.Point(9, 267);
            this.cboCategoria.Name = "cboCategoria";
            this.cboCategoria.Size = new System.Drawing.Size(186, 24);
            this.cboCategoria.TabIndex = 6;
            // 
            // rdbPendent
            // 
            this.rdbPendent.AutoSize = true;
            this.rdbPendent.Location = new System.Drawing.Point(9, 68);
            this.rdbPendent.Name = "rdbPendent";
            this.rdbPendent.Size = new System.Drawing.Size(82, 21);
            this.rdbPendent.TabIndex = 5;
            this.rdbPendent.TabStop = true;
            this.rdbPendent.Text = "Pendent";
            this.rdbPendent.UseVisualStyleBackColor = true;
            // 
            // rdbVenut
            // 
            this.rdbVenut.AutoSize = true;
            this.rdbVenut.Location = new System.Drawing.Point(9, 95);
            this.rdbVenut.Name = "rdbVenut";
            this.rdbVenut.Size = new System.Drawing.Size(66, 21);
            this.rdbVenut.TabIndex = 4;
            this.rdbVenut.TabStop = true;
            this.rdbVenut.Text = "Venut";
            this.rdbVenut.UseVisualStyleBackColor = true;
            // 
            // rdbEnSubhasta
            // 
            this.rdbEnSubhasta.AutoSize = true;
            this.rdbEnSubhasta.Location = new System.Drawing.Point(9, 41);
            this.rdbEnSubhasta.Name = "rdbEnSubhasta";
            this.rdbEnSubhasta.Size = new System.Drawing.Size(108, 21);
            this.rdbEnSubhasta.TabIndex = 3;
            this.rdbEnSubhasta.TabStop = true;
            this.rdbEnSubhasta.Text = "En subhasta";
            this.rdbEnSubhasta.UseVisualStyleBackColor = true;
            // 
            // btnNetejarFiltre
            // 
            this.btnNetejarFiltre.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNetejarFiltre.Location = new System.Drawing.Point(105, 519);
            this.btnNetejarFiltre.Name = "btnNetejarFiltre";
            this.btnNetejarFiltre.Size = new System.Drawing.Size(90, 63);
            this.btnNetejarFiltre.TabIndex = 2;
            this.btnNetejarFiltre.Text = "Netejar";
            this.btnNetejarFiltre.UseVisualStyleBackColor = true;
            this.btnNetejarFiltre.Click += new System.EventHandler(this.btnNetejarFiltre_Click);
            // 
            // btnFiltrar
            // 
            this.btnFiltrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnFiltrar.Location = new System.Drawing.Point(9, 519);
            this.btnFiltrar.Name = "btnFiltrar";
            this.btnFiltrar.Size = new System.Drawing.Size(90, 63);
            this.btnFiltrar.TabIndex = 1;
            this.btnFiltrar.Text = "Filtrar";
            this.btnFiltrar.UseVisualStyleBackColor = true;
            this.btnFiltrar.Click += new System.EventHandler(this.btnFiltrar_Click);
            // 
            // lblFiltres
            // 
            this.lblFiltres.AutoSize = true;
            this.lblFiltres.Location = new System.Drawing.Point(6, 7);
            this.lblFiltres.Name = "lblFiltres";
            this.lblFiltres.Size = new System.Drawing.Size(46, 17);
            this.lblFiltres.TabIndex = 0;
            this.lblFiltres.Text = "Filtres";
            // 
            // tabGestioCategorias
            // 
            this.tabGestioCategorias.Controls.Add(this.panel5);
            this.tabGestioCategorias.Location = new System.Drawing.Point(4, 25);
            this.tabGestioCategorias.Name = "tabGestioCategorias";
            this.tabGestioCategorias.Padding = new System.Windows.Forms.Padding(3);
            this.tabGestioCategorias.Size = new System.Drawing.Size(1303, 593);
            this.tabGestioCategorias.TabIndex = 1;
            this.tabGestioCategorias.Text = "Gestio Categories";
            this.tabGestioCategorias.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.dgvCategories);
            this.panel5.Controls.Add(this.panel6);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(3, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1297, 587);
            this.panel5.TabIndex = 0;
            // 
            // dgvCategories
            // 
            this.dgvCategories.AllowUserToAddRows = false;
            this.dgvCategories.AllowUserToDeleteRows = false;
            this.dgvCategories.AllowUserToResizeRows = false;
            this.dgvCategories.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCategories.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCategories.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CodiCategoria,
            this.NomCategoria,
            this.NomCategoriaPare});
            this.dgvCategories.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCategories.Location = new System.Drawing.Point(0, 0);
            this.dgvCategories.Name = "dgvCategories";
            this.dgvCategories.ReadOnly = true;
            this.dgvCategories.RowTemplate.Height = 24;
            this.dgvCategories.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCategories.Size = new System.Drawing.Size(1297, 449);
            this.dgvCategories.TabIndex = 1;
            this.dgvCategories.SelectionChanged += new System.EventHandler(this.dgvCategories_SelectionChanged);
            // 
            // CodiCategoria
            // 
            this.CodiCategoria.DataPropertyName = "codi";
            this.CodiCategoria.HeaderText = "Codi";
            this.CodiCategoria.Name = "CodiCategoria";
            this.CodiCategoria.ReadOnly = true;
            // 
            // NomCategoria
            // 
            this.NomCategoria.DataPropertyName = "nom";
            this.NomCategoria.HeaderText = "Categoria";
            this.NomCategoria.Name = "NomCategoria";
            this.NomCategoria.ReadOnly = true;
            // 
            // NomCategoriaPare
            // 
            this.NomCategoriaPare.DataPropertyName = "CategoriaPareNom";
            this.NomCategoriaPare.HeaderText = "Categoria Pare";
            this.NomCategoriaPare.Name = "NomCategoriaPare";
            this.NomCategoriaPare.ReadOnly = true;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.btnNetejarCategories);
            this.panel6.Controls.Add(this.button3);
            this.panel6.Controls.Add(this.btnAfegirCategoria);
            this.panel6.Controls.Add(this.btnActualizarCategoria);
            this.panel6.Controls.Add(this.txtNomCategoria);
            this.panel6.Controls.Add(this.lblNomCategoria);
            this.panel6.Controls.Add(this.label4);
            this.panel6.Controls.Add(this.cboCateogoriesPare);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(0, 449);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1297, 138);
            this.panel6.TabIndex = 0;
            // 
            // btnNetejarCategories
            // 
            this.btnNetejarCategories.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnNetejarCategories.Location = new System.Drawing.Point(366, 92);
            this.btnNetejarCategories.Name = "btnNetejarCategories";
            this.btnNetejarCategories.Size = new System.Drawing.Size(112, 25);
            this.btnNetejarCategories.TabIndex = 7;
            this.btnNetejarCategories.Text = "Netejar";
            this.btnNetejarCategories.UseVisualStyleBackColor = true;
            this.btnNetejarCategories.Click += new System.EventHandler(this.btnNetejarCategories_Click);
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button3.Location = new System.Drawing.Point(248, 92);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(112, 25);
            this.button3.TabIndex = 6;
            this.button3.Text = "Eliminar";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnAfegirCategoria
            // 
            this.btnAfegirCategoria.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAfegirCategoria.Location = new System.Drawing.Point(130, 92);
            this.btnAfegirCategoria.Name = "btnAfegirCategoria";
            this.btnAfegirCategoria.Size = new System.Drawing.Size(112, 25);
            this.btnAfegirCategoria.TabIndex = 5;
            this.btnAfegirCategoria.Text = "Afegir";
            this.btnAfegirCategoria.UseVisualStyleBackColor = true;
            this.btnAfegirCategoria.Click += new System.EventHandler(this.btnAfegirCategoria_Click);
            // 
            // btnActualizarCategoria
            // 
            this.btnActualizarCategoria.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnActualizarCategoria.Location = new System.Drawing.Point(12, 93);
            this.btnActualizarCategoria.Name = "btnActualizarCategoria";
            this.btnActualizarCategoria.Size = new System.Drawing.Size(112, 24);
            this.btnActualizarCategoria.TabIndex = 4;
            this.btnActualizarCategoria.Text = "Actualitzar";
            this.btnActualizarCategoria.UseVisualStyleBackColor = true;
            this.btnActualizarCategoria.Click += new System.EventHandler(this.btnActualizarCategoria_Click);
            // 
            // txtNomCategoria
            // 
            this.txtNomCategoria.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtNomCategoria.Location = new System.Drawing.Point(116, 51);
            this.txtNomCategoria.Name = "txtNomCategoria";
            this.txtNomCategoria.Size = new System.Drawing.Size(244, 22);
            this.txtNomCategoria.TabIndex = 3;
            // 
            // lblNomCategoria
            // 
            this.lblNomCategoria.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblNomCategoria.AutoSize = true;
            this.lblNomCategoria.Location = new System.Drawing.Point(9, 51);
            this.lblNomCategoria.Name = "lblNomCategoria";
            this.lblNomCategoria.Size = new System.Drawing.Size(100, 17);
            this.lblNomCategoria.TabIndex = 2;
            this.lblNomCategoria.Text = "Nom categoria";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(103, 17);
            this.label4.TabIndex = 1;
            this.label4.Text = "Categoria Pare";
            // 
            // cboCateogoriesPare
            // 
            this.cboCateogoriesPare.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cboCateogoriesPare.FormattingEnabled = true;
            this.cboCateogoriesPare.Location = new System.Drawing.Point(115, 12);
            this.cboCateogoriesPare.Name = "cboCateogoriesPare";
            this.cboCateogoriesPare.Size = new System.Drawing.Size(245, 24);
            this.cboCateogoriesPare.TabIndex = 0;
            // 
            // visProducte
            // 
            this.visProducte.Dock = System.Windows.Forms.DockStyle.Fill;
            this.visProducte.Location = new System.Drawing.Point(0, 0);
            this.visProducte.Name = "visProducte";
            this.visProducte.Size = new System.Drawing.Size(802, 247);
            this.visProducte.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1311, 622);
            this.Controls.Add(this.tabs);
            this.MinimumSize = new System.Drawing.Size(1080, 597);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabs.ResumeLayout(false);
            this.tabGestioProductes.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProductesCataleg)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tabGestioCategorias.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCategories)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabs;
        private System.Windows.Forms.TabPage tabGestioProductes;
        private System.Windows.Forms.TabPage tabGestioCategorias;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtNomDesc;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboCategoria;
        private System.Windows.Forms.RadioButton rdbPendent;
        private System.Windows.Forms.RadioButton rdbVenut;
        private System.Windows.Forms.RadioButton rdbEnSubhasta;
        private System.Windows.Forms.Button btnNetejarFiltre;
        private System.Windows.Forms.Button btnFiltrar;
        private System.Windows.Forms.Label lblFiltres;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgvProductesCataleg;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnNouProducte;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnAfegirCategoria;
        private System.Windows.Forms.Button btnActualizarCategoria;
        private System.Windows.Forms.TextBox txtNomCategoria;
        private System.Windows.Forms.Label lblNomCategoria;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cboCateogoriesPare;
        private System.Windows.Forms.RadioButton rdbTots;
        private System.Windows.Forms.Button btnNetejarCategories;
        private System.Windows.Forms.DataGridView dgvCategories;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtDescProd;
        private System.Windows.Forms.DataGridViewTextBoxColumn CodigoProducto;
        private System.Windows.Forms.DataGridViewTextBoxColumn NombreProducto;
        private System.Windows.Forms.DataGridViewTextBoxColumn ValorEstimadoProducto;
        private System.Windows.Forms.DataGridViewTextBoxColumn CategoriaProducto;
        private System.Windows.Forms.DataGridViewTextBoxColumn PreuFinal;
        private System.Windows.Forms.DataGridViewTextBoxColumn CodiCategoria;
        private System.Windows.Forms.DataGridViewTextBoxColumn NomCategoria;
        private System.Windows.Forms.DataGridViewTextBoxColumn NomCategoriaPare;
        private System.Windows.Forms.Panel panel7;
        private ControlPersonalitzat.VisualitzadorProducte visProducte;
    }
}

