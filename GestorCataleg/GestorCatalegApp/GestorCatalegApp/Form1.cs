﻿using GestorCatalegAPP.DefinicionsClases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestorCatalegAPP
{
    public partial class Form1 : Form
    {

        private IGestorBD mConexio;
        private BindingList<ProducteCataleg> mLlProductes;
        private BindingList<Categoria> mLlCategoria;
        private List<Categoria> mLlistaCboCategories = new List<Categoria>();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            init();
        }

        private void init()
        {
            rdbTots.Select();
            getValuesFromDB();
        }

        private void getValuesFromDB()
        {
            dgvProductesCataleg.AutoGenerateColumns = false;
            dgvCategories.AutoGenerateColumns = false;
            try
            {
                mConexio = (IGestorBD)Activator.CreateInstance(Type.GetType(Properties.Settings.Default.DB_PROVIDER));
                carregarValorsAlDataGridViewProductes();
                carregarValorsAlDataGridViewCategories();
                carregarValorsAlCbosCategories();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al conectar amb la BD: " + ex.Message);
                if (mConexio != null)
                {
                    mConexio.close();
                }
                this.Close();
            }
        }

        private void carregarValorsAlCbosCategories()
        {
            foreach (Categoria c in mLlCategoria)
            {
                mLlistaCboCategories.Add(c);
            }
            cboCategoria.DataSource = mLlCategoria;
            cboCategoria.DisplayMember = "nom";
            cboCateogoriesPare.DataSource = mLlistaCboCategories;
            cboCateogoriesPare.DisplayMember = "nom";
        }

        private void carregarValorsAlDataGridViewCategories()
        {
            mLlCategoria = mConexio.getLlistaCategories();
            dgvCategories.DataSource = mLlCategoria;
        }

        private void carregarValorsAlDataGridViewProductes()
        {
            mLlProductes = mConexio.getLlistaProductes();
            dgvProductesCataleg.DataSource = mLlProductes;
            posarColorsDGV();
        }

        private void posarColorsDGV()
        {
            int pos = -1;
            foreach (DataGridViewRow d in dgvProductesCataleg.Rows)
            {
                try
                {
                    pos++;
                    estatSubhasta estat = mLlProductes.ElementAt(pos).estat;
                    if (estat == estatSubhasta.VENUT)
                    {
                        d.DefaultCellStyle.BackColor = Color.Lime;
                    }
                    else if (estat == estatSubhasta.EN_SUBHASTA)
                    {
                        d.DefaultCellStyle.BackColor = Color.Yellow;
                    }
                    else if (estat == estatSubhasta.NO_VENUT)
                    {
                        d.DefaultCellStyle.BackColor = Color.Tomato;
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }

        

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                mConexio.close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al tancar la conexio: " + ex.Message);
            }
        }

        private void btnNetejarFiltre_Click(object sender, EventArgs e)
        {
            rdbTots.Select();
            txtNomDesc.Text = "";
            cboCategoria.SelectedIndex = -1;
            carregarValorsAlDataGridViewProductes();
        }

        private void btnNetejarCategories_Click(object sender, EventArgs e)
        {
            cboCateogoriesPare.SelectedIndex = -1;
            txtNomCategoria.Text = "";
        }

        private void btnNouProducte_Click(object sender, EventArgs e)
        {
            try
            {
                EdicioDeProducteCataleg f = new EdicioDeProducteCataleg(mConexio, mLlCategoria, null);
                f.ShowDialog();
                carregarValorsAlDataGridViewProductes();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al actualitzar la llista de productes: " + ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (dgvProductesCataleg.CurrentCell.RowIndex >=0)
            {
                try
                {
                    ProducteCataleg pc = mLlProductes.ElementAt(dgvProductesCataleg.CurrentCell.RowIndex);
                    mConexio.eliminarProducte(pc);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show("No hi ha res seleccionat");
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dgvProductesCataleg.CurrentCell.RowIndex >= 0)
            {
                try
                {
                    ProducteCataleg pc = mLlProductes.ElementAt(dgvProductesCataleg.CurrentCell.RowIndex);
                    pc.llistaFotos = mConexio.getLlistaFotosOfProducte(pc);
                    EdicioDeProducteCataleg f = new EdicioDeProducteCataleg(mConexio, mLlCategoria, pc);
                    f.ShowDialog();
                    carregarValorsAlDataGridViewProductes();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show("No hi ha res seleccionat");
            }
        }

        private void btnFiltrar_Click(object sender, EventArgs e)
        {
            try
            {
                estatSubhasta estat = tipusDeProducteAFiltrar();
                Categoria c = null;
                if(cboCategoria.SelectedIndex >= 0){
                    c = (Categoria)cboCategoria.SelectedItem;
                }
                mLlProductes = mConexio.getFilteredProductes(estat,txtNomDesc.Text,txtDescProd.Text,c);
                dgvProductesCataleg.DataSource = mLlProductes;
                posarColorsDGV();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al filtrar: " + ex.Message);
            }
        }

        private estatSubhasta tipusDeProducteAFiltrar()
        {
            if (rdbEnSubhasta.Checked)
            {
                return estatSubhasta.EN_SUBHASTA;
            }
            else if (rdbPendent.Checked)
            {
                return estatSubhasta.PENDENT;
            }
            else if (rdbVenut.Checked)
            {
                return estatSubhasta.VENUT;
            }
            else
            {
                return estatSubhasta.TOTS;
            }
        }

        private void dgvCategories_SelectionChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgvCategories.SelectedRows)
            {
                Categoria c = mLlCategoria.ElementAt(row.Index);
                txtNomCategoria.Text = c.nom;
                int i = mLlCategoria.IndexOf(c.categoriaPare);
                if (i >= 0)
                {
                    cboCateogoriesPare.SelectedIndex = i;
                }
                else
                {
                    cboCateogoriesPare.SelectedIndex = -1;
                }
            }
        }

        private void btnActualizarCategoria_Click(object sender, EventArgs e)
        {
            if (dgvCategories.SelectedRows.Count == 0)
            {
                MessageBox.Show("No hi ha cap categoria seleccionada");
                return;
            }
            int pos = dgvCategories.SelectedRows[0].Index;
            Categoria c = mLlCategoria.ElementAt(pos);
            if (txtNomCategoria.Text.Length == 0)
            {
                MessageBox.Show("No es pot inserir nom null");
                return;
            }
            if (cboCateogoriesPare.SelectedIndex >= 0)
            {
                c.categoriaPare = mLlCategoria.ElementAt(cboCateogoriesPare.SelectedIndex);
            }
            c.nom = txtNomCategoria.Text;
            try
            {
                mConexio.actualitzarCategoria(c);
                carregarValorsAlDataGridViewCategories();
                carregarValorsAlCbosCategories();
                MessageBox.Show("Actualitzat amb exit");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al actualitzar la categoria: " + ex.Message);
            }
        }

        private void btnAfegirCategoria_Click(object sender, EventArgs e)
        {
            if (dgvCategories.SelectedRows.Count == 0)
            {
                MessageBox.Show("No hi ha cap categoria seleccionada");
                return;
            }
            int pos = dgvCategories.SelectedRows[0].Index;
            Categoria c = mLlCategoria.ElementAt(pos);
            if (txtNomCategoria.Text.Length == 0)
            {
                MessageBox.Show("No es pot inserir nom null");
                return;
            }
            if (cboCateogoriesPare.SelectedIndex >= 0)
            {
                c.categoriaPare = mLlCategoria.ElementAt(cboCateogoriesPare.SelectedIndex);
            }
            c.nom = txtNomCategoria.Text;
            try
            {
                mConexio.inserirCategoria(c);
                carregarValorsAlDataGridViewCategories();
                carregarValorsAlCbosCategories();
                MessageBox.Show("Actualitzat amb exit");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al inserir la categoria: " + ex.Message);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dgvCategories.SelectedRows.Count == 0)
            {
                MessageBox.Show("No hi ha cap categoria seleccionada");
                return;
            }
            int pos = dgvCategories.SelectedRows[0].Index;
            Categoria c = mLlCategoria.ElementAt(pos);
            try
            {
                mConexio.eliminarCategoria(c);
                carregarValorsAlDataGridViewCategories();
                carregarValorsAlCbosCategories();
                MessageBox.Show("Eliminat amb exit");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al actualitzar la categoria: " + ex.Message);
            }
        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dgvProductesCataleg_SelectionChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgvProductesCataleg.SelectedRows)
            {
                ProducteCataleg pc = mLlProductes.ElementAt(row.Index);
                visProducte.Producte = pc;
            }
        }
    }
}
