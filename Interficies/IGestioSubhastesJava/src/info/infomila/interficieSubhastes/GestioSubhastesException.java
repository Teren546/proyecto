/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.infomila.interficieSubhastes;

/**
 *
 * @author Adria
 */
public class GestioSubhastesException extends Exception {

    public GestioSubhastesException(String message) {
        super(message);
    }

    public GestioSubhastesException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
