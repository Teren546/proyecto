/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.infomila.interficieSubhastes;

import info.infomila.model.Oferta;
import info.infomila.model.ProducteCataleg;
import info.infomila.model.Subhasta;
import info.infomila.model.Usuari;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author Adria
 */
public interface IGestioSubhastes {
    
    /**
     * Tanca la conexio.
     * @throws GestioSubhastesException 
     */
    void close() throws GestioSubhastesException;
    
    /**
     * Es reconecta amb la BD, sempre que no hi hagi ya la conexio oberta.
     * @throws GestioSubhastesException 
     */
    void reconnect() throws GestioSubhastesException;
    
    /**
     * Comproba si hi ha actulizacions de les subhastes
     * @return
     * @throws GestioSubhastesException 
     */
    boolean hihaActus() throws GestioSubhastesException;
    
    /**
     * Retorna una llista amb totes les subhastes disponibles a la BD
     * @return llista amb totes les subhastes de la bd
     * @throws GestioSubhastesException 
     */
    List<Subhasta> getSubhastes() throws GestioSubhastesException;

    /**
     * Actualitza la subhasta pasada per parametra amb la informacio de la BD
     * @param sb
     * @return
     * @throws GestioSubhastesException 
     */
    Subhasta refreshOfertasFromSubhasta(Subhasta sb) throws GestioSubhastesException;
    
    /**
     * Retorna una llista amb les subhastes actives de la BD
     * @return
     * @throws GestioSubhastesException 
     */
    List<Subhasta> getActiveSubhastes() throws GestioSubhastesException;
    
    /**
     * Retorna una llista amb les subhastes tancades de la BD
     * @return
     * @throws GestioSubhastesException 
     */
    List<Subhasta> getClosedSubhastes() throws GestioSubhastesException;
    
    /**
     * Retorna una llista de subhastes filtrada per els camps de la subhasta
     * @param active pot ser null, en aquest cas, es retornen totes les subhastes
     * @param nameORdesc pot ser null, en aquest cas, no es comprobara el nom
     * @param maxValue pot ser null, el valor maxim de valor de la comanda
     * @param minValue pot ser null, el valor minim de valor de la comanda
     * @return 
     */
    List<Subhasta> getFilteredSubhastes(Boolean active, String nameORdesc, BigDecimal maxValue, BigDecimal minValue) throws GestioSubhastesException;
    
    /**
     * Recupera les subhastes del usuari amb el codi passat per parametre
     * @param codi
     * @return
     * @throws GestioSubhastesException 
     */
    List<Subhasta> getSubhastesFromUsuari(int codi) throws GestioSubhastesException;
    
    /**
     * Retorna la llista amb tots els productes disponibles a la BD
     * @return llista amb tots els productes de la bd
     * @throws GestioSubhastesException 
     */
    List<ProducteCataleg> getProductes() throws GestioSubhastesException;
    
    /**
     * Recupera la llista de usuaris que estan registrats a la BD
     * @return
     * @throws GestioSubhastesException 
     */
    List<Usuari> getUsuaris() throws GestioSubhastesException;
    
    /**
     * Recupera el usuari amb el codi passat per parametre
     * @return
     * @throws GestioSubhastesException 
     */
    Usuari getUsuariByCod(int codi) throws GestioSubhastesException;
    
    /**
     * Fa un fluhs de la informacio
     * @throws GestioSubhastesException 
     */
    void flush() throws GestioSubhastesException;
    
    
    /**
     * Inserta la subhasta pasada per parametre a la BD
     * @param s
     * @throws GestioSubhastesException 
     */
    void insertSubhasta(Subhasta s) throws GestioSubhastesException;
    
    /**
     * Inserta la oferta pasada per parametre a la BD
     * @param of
     * @throws GestioSubhastesException 
     */
    void insertOferta(Oferta of) throws GestioSubhastesException;
    
    /**
     * Inserta el usuari pasat per parametre a la BD
     * @param u
     * @throws GestioSubhastesException 
     */
    void insertUsuariBD(Usuari u) throws GestioSubhastesException;
    
    /**
     * Actualitza el usuari pasat per parametre a la BD
     * @param u
     * @throws GestioSubhastesException 
     */
    void actualitzarUsuariBD(Usuari u) throws GestioSubhastesException;
    
    /**
     * Recupera la subhasta que te el codi pasat per parametre
     * @param codi
     * @return
     * @throws GestioSubhastesException 
     */
    Subhasta getSubhastaByCodi(int codiSub) throws GestioSubhastesException;
    
    /**
     * Fa un commit del que hi hagi en memoria
     * @throws GestioSubhastesException 
     */
    void commit() throws GestioSubhastesException;
    
    void refresh(Object obj) throws GestioSubhastesException;
}
