/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.infomila.manager;

import info.infomila.interficieSubhastes.GestioSubhastesException;
import info.infomila.interficieSubhastes.IGestioSubhastes;
import info.infomila.manager.ui.UIConnectant;
import info.infomila.manager.ui.UIManager;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.JOptionPane;

/**
 *
 * @author Adria
 */
public class ManagerSubhastes {

    public static void main(String[] args) {
        UIConnectant connecting = new UIConnectant();
        try {
            String rutaFitxer = "FitxerConfig.xml";
            if (args.length == 1) {
                rutaFitxer = args[0];
            }

            File pFitxerDePropietats = new File(rutaFitxer);

            if (!pFitxerDePropietats.exists()) {
                JOptionPane.showMessageDialog(null, "Error al iniciar la aplicacio, el fitxer no s'ha trobat");
                System.exit(-1);
            }

            Properties p = new Properties();
            String className = null;
            FileInputStream fs = null;
            try {
                fs = new FileInputStream(pFitxerDePropietats);
                p.loadFromXML(fs);
                className = p.getProperty("className");
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, "Error al iniciar la aplicacio, no s'han trobat les propietats: " + ex.getMessage());
                System.exit(-1);
            } finally {
                if (fs != null) {
                    try {
                        fs.close();
                    } catch (IOException ex) {
                    }
                }
            }

            IGestioSubhastes vGestioSubhastes = null;
            try {
                vGestioSubhastes = (IGestioSubhastes) Class.forName(className).getConstructor(File.class).newInstance(pFitxerDePropietats);
            } catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                JOptionPane.showMessageDialog(null, "Error al iniciar la aplicacio:\n " + ex.getMessage() + "\n" + ex.getCause());
                System.exit(-1);
            }
            
            //if(1==1) System.exit(-1);
            UIManager ui = new UIManager(vGestioSubhastes,pFitxerDePropietats);
            connecting.tancar();
            ui.mostrarInterficie();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error al iniciar la aplicacio:\n " + ex.getMessage() + "\n" + ex.getCause());
            System.exit(-1);
        }
    }
}
