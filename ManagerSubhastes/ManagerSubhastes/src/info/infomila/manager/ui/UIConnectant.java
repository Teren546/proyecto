/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.infomila.manager.ui;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author Adria
 */
public class UIConnectant {
    
    private JFrame mUI;
    
    public UIConnectant(){
        mUI = new JFrame();
        JLabel l = new JLabel("Conectant amb la BD...");
        mUI.add(l);
        mUI.setTitle("Obrint conexio amb el servidor");
        mUI.setSize(new Dimension(150, 100));
        mUI.setResizable(false);
        mUI.setVisible(true);
        mUI.setLocationRelativeTo(null); 
        mUI.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        
        mUI.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    }
    
    public void tancar(){
        mUI.setVisible(false);
        mUI.dispose();
    }
}
