/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.infomila.manager.ui;

import com.github.lgooddatepicker.timepicker.TimePicker;
import com.toedter.calendar.JDateChooser;
import info.infomila.interficieSubhastes.IGestioSubhastes;
import info.infomila.model.Oferta;
import info.infomila.model.ProducteCataleg;
import info.infomila.model.Subhasta;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.time.LocalTime;
import java.util.Date;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Adria
 */
public class UICreacioSubhasta {

    // <editor-fold defaultstate="collapsed" desc="Final Strings">  
    private final String mStringDiaFi = "Selecciona el dia finalitzacio de la subasta ";
    private final String mStringHora = "Hora seleccionada: ";
    private final String mStringPreuSortida = "Entra el preu de sortida: ";
    private final String mStringDESA = "DESA";
    private final String mStringCANCELA = "CANCELA";
    private final String mStringMissatgeInsercio = "Voleu inserir la comanda?";
    private final String mStringAtencio = "ATENCIO";
    private final String mStringCancela = "Atencio, es perdran els canvis. Voleu continuar?";
    private final String mDadesIncorrectes = "Error, les dades son incorrectes";
    private final String mErrorTitol = "Error";
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Variables Interficie Grafica">  
    private JFrame mFramePare;
    private ProducteCataleg mProducteCataleg;
    private String mTitol;
    private JDialog mDialeg;
    private JDateChooser mDateChooser;
    private JPanel mContenidor, mPnlTop,mPnlBot;
    private TimePicker mSeleccioHora;
    private JFormattedTextField mSeleccioPreuSortida;
    private JButton mBtnDesa,mBtnCancela;
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Listeners botons">
    class GestioButtons implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            String aux = e.getActionCommand();
            if(mStringDESA.equalsIgnoreCase(aux)){
                int opcio = JOptionPane.showConfirmDialog(mDialeg, mStringMissatgeInsercio, mStringAtencio, JOptionPane.OK_CANCEL_OPTION);
                if(opcio==JOptionPane.OK_OPTION){
                    if(!campsValids()){
                        mostrarError();
                    }else{
                        Boolean error = false;
                        try{
                            Date d = mDateChooser.getDate();
                            LocalTime lt = mSeleccioHora.getTime();
                            if(d == null || lt == null){
                                throw new RuntimeException("Error, data o hora incorrectes");
                            }
                            d.setHours(lt.getHour());
                            d.setMinutes(lt.getMinute());
                            
                            Date ara = new Date();
                            if(ara.compareTo(d) >= 0){
                                throw new RuntimeException("Error, no es pot inserir una subhasta tancada");
                            }
                            BigDecimal preuSortida = null;
                            try{
                                preuSortida = new BigDecimal(mSeleccioPreuSortida.getText().replace(".", "").replace(",", "."));
                                if(preuSortida.compareTo(new BigDecimal(0)) <= 0){
                                    throw new RuntimeException("Error");
                                }
                            }catch(Exception ex){
                                throw new RuntimeException("Error, el numero inserit no es valid");
                            }
                            
                            Subhasta s = new Subhasta(d,preuSortida , mProducteCataleg);
                            mConexio.insertSubhasta(s);
                        }catch(Exception ex){
                            JOptionPane.showMessageDialog(mDialeg, "Error, dades incorrectes: " + ex.getMessage());
                            error = true;
                        }
                        if(!error){
                            JOptionPane.showMessageDialog(mDialeg, "Subhasta inserida correctament");
                            //Tanquem el dialeg
                            mDialeg.dispose(); 
                        }
                    } 
                }
            }else if(mStringCANCELA.equalsIgnoreCase(aux)){
                int opcio = JOptionPane.showConfirmDialog(mDialeg, mStringCancela, mStringAtencio, JOptionPane.OK_CANCEL_OPTION);
                if(opcio==JOptionPane.OK_OPTION){
                    //Tanquem el dialeg
                    mDialeg.dispose();
                }
            }
        }

        private void mostrarError() {
            JOptionPane.showMessageDialog(mDialeg, mDadesIncorrectes, mErrorTitol, JOptionPane.INFORMATION_MESSAGE);
        }

        private boolean campsValids() {
            
            System.out.println(mDateChooser.getDate());
            System.out.println(mSeleccioHora.getTime());
            System.out.println(mSeleccioPreuSortida.getText());
            if(mDateChooser.getDate() == null){
                return false;
            }
            if(mSeleccioHora.getTime() == null){
                return false;
            }
            try{
                Double d = Double.parseDouble(mSeleccioPreuSortida.getText());
                if(d == null){
                    return false;
                }
                if(d <= 0){
                    return false;
                }
            }catch(Exception ex){
                return false;
            }
            return true;
        }
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Listeners JFRAME">
    
    class GestioTancament extends WindowAdapter {

        @Override
        public void windowClosing(WindowEvent e) {
            int opcio = JOptionPane.showConfirmDialog(mDialeg, mStringCancela, mStringAtencio, JOptionPane.OK_CANCEL_OPTION);
            if(opcio==JOptionPane.OK_OPTION){
                mDialeg.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            }else{
                mDialeg.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            }
        }
    }
    // </editor-fold>
    
    private IGestioSubhastes mConexio;
    
    public UICreacioSubhasta(Frame owner, ProducteCataleg pc, String title, IGestioSubhastes conexio) {
        this.mProducteCataleg = pc;
        mFramePare = (JFrame) owner;
        mTitol = title;
        mConexio = conexio;
        initUI();
    }
    
    // <editor-fold defaultstate="collapsed" desc="Creaciod de interficie Grafica">

    private void initUI() {
        mDialeg = new JDialog(mFramePare);
        mDialeg.setTitle(mTitol);
        mDialeg.setSize(new Dimension(400, 200));
        mDialeg.setResizable(false);
        mDialeg.setLocationRelativeTo(mFramePare);
        mDialeg.setModal(true);
        
        mContenidor = new JPanel();
        mContenidor.setLayout(new BorderLayout());
        
        mDialeg.add(mContenidor);
        
        mPnlTop = new JPanel(); //FlowLayout
        mPnlBot = new JPanel();
        
        mContenidor.add(mPnlTop, BorderLayout.NORTH);
        mContenidor.add(mPnlBot, BorderLayout.SOUTH);
        
        crearInputs();
        addActionListeners();
    }
    
    public void mostrarDialeg(){
        mDialeg.setVisible(true);
    }

    private void crearInputs() {
        mDateChooser = new JDateChooser();
        mSeleccioHora = new TimePicker();
        mDateChooser.setMinSelectableDate(new Date());
        
        JLabel lblDataFiSelec = new JLabel();
        lblDataFiSelec.setText(mStringDiaFi);
        
        mPnlTop.add(lblDataFiSelec, BorderLayout.WEST);
        mPnlTop.add(mDateChooser,BorderLayout.EAST);
        
        
        mPnlBot.setLayout(new BorderLayout());
        JPanel jp1 = new JPanel();
        JPanel jp2 = new JPanel();
        
        JLabel lblDataFiHora = new JLabel();
        lblDataFiHora.setText(mStringHora);
        JLabel lblPreuSortida = new JLabel();
        lblPreuSortida.setText(mStringPreuSortida);
        
        
        jp1.add(lblDataFiHora);
        jp1.add(mSeleccioHora);
        
        NumberFormat formatDiners = NumberFormat.getNumberInstance();
        mSeleccioPreuSortida = new JFormattedTextField(formatDiners);
        mSeleccioPreuSortida.setValue(new Double(0));
        mSeleccioPreuSortida.setColumns(10);
        jp2.add(lblPreuSortida);
        jp2.add(mSeleccioPreuSortida);
        JLabel lblEurosLogo = new JLabel();
        lblEurosLogo.setText("€");
        jp2.add(lblEurosLogo);
        
        mBtnDesa = new JButton();
        mBtnDesa.setText(mStringDESA);
        
        mBtnCancela = new JButton();
        mBtnCancela.setText(mStringCANCELA);
        
        JPanel pnlButtons = new JPanel();
        pnlButtons.add(mBtnDesa);
        pnlButtons.add(mBtnCancela);
        
        
        mPnlBot.add(jp1,BorderLayout.NORTH);
        mPnlBot.add(jp2,BorderLayout.CENTER);
        mPnlBot.add(pnlButtons, BorderLayout.SOUTH);
    }

    private void addActionListeners() {
        GestioButtons gb = new GestioButtons();
        mBtnDesa.addActionListener(gb);
        mBtnCancela.addActionListener(gb);
        mDialeg.addWindowListener(new GestioTancament());
    }
    
    // </editor-fold>
}
