/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.infomila.manager.ui;

import info.infomila.interficieSubhastes.GestioSubhastesException;
import info.infomila.interficieSubhastes.IGestioSubhastes;
import info.infomila.model.Oferta;
import info.infomila.model.ProducteCataleg;
import info.infomila.model.Subhasta;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Adria
 */
public class UIManager {
    
    // <editor-fold defaultstate="collapsed" desc="Final Strings">  
    private final String mStringNom = "Nom: ";
    private final String mStringTots = "Tots";
    private final String mStringActives = "Actives";
    private final String mStringTancades = "Tancades";
    private final String mStringMinimEuros = "Min €";
    private final String mStringMaximEuros = "Max €";
    private final String mStringFiltrar = "Filtrar";
    private final String mStringNetejar = "Netejar";
    private final String mStringNovaSubhasta = "Nova subhasta";
    private final String mStringVeureInfoSubhastaSeleccionada = "Veure info subhasta seleccionada";
    private final String mStringProductesCataleg = "Productes cataleg";
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Variables Interficie Grafica">    
    private JFrame mJfrUI;
    //PANELLS PRINCIPALS
    private JPanel mPnlContenidor, mPnlFiltres, mPnlSeccioPartBaixa;
    private JScrollPane mPnlTaula;
    
    private JTable mTblTaulaDeSubhastes;
    private DefaultTableModel mTaulaSub;
    private final String[] mTitolsTaula = {"PRODUCTE","PREU PARTIDA","ULTIMA OFERTA","DATA FI"};
    
    //ZONA FILTRES
    // <editor-fold defaultstate="collapsed" desc="Variables dels filtres">
        private JPanel mPnlFiltreRadioButtons, mPnlFiltreNom, mPnlFiltrePreus;

        //RadioButtons de filtres
        private JRadioButton mRdbAll,mRdbActives,mRdbTancades;
        private ButtonGroup mRdbGroupFiltres;

        //JTextField filtre
        private JTextField mTxtNomDesc;
        private JLabel mLblDescNomFiltre;

        //Filtre de preus
        private JFormattedTextField mTxtMinValue;
        private JFormattedTextField mTxtMaxValue;

        //Boto filtres
        private JButton mBtnFiltrar;
        private JButton mBtnNetejarFiltre;
    // </editor-fold>
    private JComboBox mCboCatalegProd;
    private JButton mBtnNovaSubhasta;
    private JButton mBtnVeureDetallsSubhasta;
    // </editor-fold>   
    
    // <editor-fold defaultstate="collapsed" desc="Creacio de la interficie Grafica">
    private void initUI() {
        //Inicialitzacio dels panells principals
        mJfrUI = new JFrame();
        mPnlContenidor = new JPanel();
        mJfrUI.setLocationRelativeTo(null); 
        mPnlContenidor.setLayout(new BorderLayout());
        omplirPnlContenidor();
        
        mJfrUI.add(mPnlContenidor);
        mJfrUI.setSize(800, 600);
        mJfrUI.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        mJfrUI.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                try {
                    mConexio.close();
                } catch (GestioSubhastesException ex) {
                    JOptionPane.showMessageDialog(mJfrUI, "Error al tancar la conexio");
                }
            }
        });
    }
    
    public void mostrarInterficie(){
        
        mJfrUI.setMinimumSize(new Dimension(800,600));
        mJfrUI.setVisible(true);
        
    }

    private void omplirPnlContenidor() {
        crearZonaTaula();
        omplirZonaTaula();
        crearZonaFiltres();
        omplirZonaFiltres();
        crearZonaButtons();
        omplirZonaButtons();
    }

    private void crearZonaTaula() {
        
        mTaulaSub = new DefaultTableModel();
        mTblTaulaDeSubhastes = new JTable(mTaulaSub) {
            @Override
            public boolean isCellEditable(int row, int column) // Per aconseguir que la columna 2 sigui editable
            {
                return false;
            }

            @Override
            public Class<?> getColumnClass(int column) {
                Class clazz = String.class;
                return clazz;
            }

        };
        for(String s:mTitolsTaula){
            mTaulaSub.addColumn(s);
        }
        
        mPnlTaula = new JScrollPane(mTblTaulaDeSubhastes);
    }

    private void omplirZonaButtons() {
        mPnlContenidor.add(mPnlSeccioPartBaixa, BorderLayout.SOUTH);
    }

    private void crearZonaButtons() {
        mPnlSeccioPartBaixa = new JPanel();
        
        JLabel lblCataleg = new JLabel();
        lblCataleg.setText(mStringProductesCataleg);
        mCboCatalegProd = new JComboBox();
        
        mBtnNovaSubhasta = new JButton();
        mBtnNovaSubhasta.setText(mStringNovaSubhasta);
        
        mBtnVeureDetallsSubhasta = new JButton();
        mBtnVeureDetallsSubhasta.setText(mStringVeureInfoSubhastaSeleccionada);
        
        mPnlSeccioPartBaixa.add(lblCataleg);
        mPnlSeccioPartBaixa.add(mCboCatalegProd);
        mPnlSeccioPartBaixa.add(mBtnNovaSubhasta);
        mPnlSeccioPartBaixa.add(mBtnVeureDetallsSubhasta);
    }

    private void omplirZonaFiltres() {
        mPnlContenidor.add(mPnlFiltres, BorderLayout.EAST);
    }

    private void crearZonaFiltres() {
        mPnlFiltres = new JPanel();
        mPnlFiltres.setLayout(new BorderLayout());
        
        JPanel panelTop = new JPanel();
        panelTop.setLayout(new BorderLayout());
        JPanel panelBot = new JPanel();
        
        //Creacio dels contenidors
        mPnlFiltreRadioButtons = new JPanel();
        mPnlFiltreNom = new JPanel();
        mPnlFiltrePreus = new JPanel();
        mPnlFiltrePreus.setLayout(new BorderLayout());
        
        //Creacio del grup de botons de filtres
        mRdbGroupFiltres = new ButtonGroup();
        
        //Creacio dels radiobuttons
        mRdbAll = new JRadioButton();
        mRdbAll.setText(mStringTots);
        mRdbActives = new JRadioButton();
        mRdbActives.setText(mStringActives);
        mRdbTancades = new JRadioButton();
        mRdbTancades.setText(mStringTancades);
        
        //Marquem 1 per defecte
        mRdbAll.setSelected(true);
        
        //Add botons filtres
        mRdbGroupFiltres.add(mRdbAll);
        mRdbGroupFiltres.add(mRdbActives);
        mRdbGroupFiltres.add(mRdbTancades);
        
        //Afegim filtre radioButttons
        mPnlFiltreRadioButtons.add(mRdbAll);
        mPnlFiltreRadioButtons.add(mRdbActives);
        mPnlFiltreRadioButtons.add(mRdbTancades);
        
        //Afegim radiobuttons al panell de filtres        
        panelTop.add(mPnlFiltreRadioButtons,BorderLayout.NORTH);
        
        
        //Zona de filtre nom
        mTxtNomDesc = new JTextField(10);
        
        mLblDescNomFiltre = new JLabel();
        mLblDescNomFiltre.setText(mStringNom);
        
        
        mPnlFiltreNom.add(mLblDescNomFiltre);
        mPnlFiltreNom.add(mTxtNomDesc);
        
        panelTop.add(mPnlFiltreNom, BorderLayout.CENTER);
        
        //Zona de filtre per Preu
        NumberFormat formatDiners = NumberFormat.getNumberInstance();
        mTxtMinValue = new JFormattedTextField(formatDiners);
        mTxtMinValue.setValue(new Double(0));
        mTxtMinValue.setColumns(10);
        
        JPanel minValue = new JPanel();
        
        minValue.add(new JLabel(mStringMinimEuros));
        minValue.add(mTxtMinValue);
        
        mTxtMaxValue = new JFormattedTextField(formatDiners);
        mTxtMaxValue.setValue(new Double(0));
        mTxtMaxValue.setColumns(10);
        
        JPanel maxValue = new JPanel();
        
        maxValue.add(new JLabel(mStringMaximEuros));
        maxValue.add(mTxtMaxValue);
        
        mPnlFiltrePreus.add(minValue,BorderLayout.NORTH);
        mPnlFiltrePreus.add(maxValue,BorderLayout.SOUTH);
        
        panelTop.add(mPnlFiltrePreus, BorderLayout.SOUTH);
        
        mPnlFiltres.add(panelTop, BorderLayout.CENTER);
        
        mBtnFiltrar = new JButton();
        mBtnFiltrar.setText(mStringFiltrar);
        
        mBtnNetejarFiltre = new JButton();
        mBtnNetejarFiltre.setText(mStringNetejar);
        
        panelBot.add(mBtnFiltrar);
        panelBot.add(mBtnNetejarFiltre);
        
        mPnlFiltres.add(panelBot, BorderLayout.SOUTH);
    }

    private void omplirZonaTaula() {
        mPnlContenidor.add(mPnlTaula, BorderLayout.CENTER);
    }
    
    // </editor-fold>  
    
    // <editor-fold defaultstate="collapsed" desc="Listeners de la interficie Grafica">
    
    private void initListeners() {
        addRadioButtonsListener();
        addButtonsListener();
    }

    private void addRadioButtonsListener() {
        GestioRadioButtons grb = new GestioRadioButtons();
        
        mRdbAll.addActionListener(grb);
        mRdbActives.addActionListener(grb);
        mRdbTancades.addActionListener(grb);
    }

    private void addButtonsListener() {
        GestioButtons gb = new GestioButtons();
        mBtnFiltrar.addActionListener(gb);
        mBtnNetejarFiltre.addActionListener(gb);
        mBtnNovaSubhasta.addActionListener(gb);
        mBtnVeureDetallsSubhasta.addActionListener(gb);
    }
    
    class GestioRadioButtons implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            if(mStringActives.equalsIgnoreCase(e.getActionCommand())){
                System.out.println("Es mostraran les subhastes actives");
                OpcioSeleccionada = ERadioButtonsSelection.ACTIVES;
            }else if(mStringTancades.equalsIgnoreCase(e.getActionCommand())){
                System.out.println("Es mostraran les subhastes tancades");
                OpcioSeleccionada = ERadioButtonsSelection.TANCADES;
            }else{
                System.out.println("Es mostraran totes les subhastes");
                OpcioSeleccionada = ERadioButtonsSelection.TOTES;
            }
        }
    }
    
    class GestioButtons implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            String com = e.getActionCommand();
            if(mStringFiltrar.equalsIgnoreCase(com)){
                //TODO fer filtre
                Boolean filtreActiusTancats = null;
                switch(OpcioSeleccionada){
                    case ACTIVES:
                        filtreActiusTancats = true;
                        break;
                    case TANCADES:
                        filtreActiusTancats = false;
                        break;
                    case TOTES:
                        filtreActiusTancats = null;
                        break;
                }
                String nomOdesc = mTxtNomDesc.getText();
                String minVal = mTxtMinValue.getText().replace(".", "").replace(",", ".");
                String maxVal = mTxtMaxValue.getText().replace(".", "").replace(",", ".");
                BigDecimal minim = null;
                BigDecimal maxim = null;
                try{
                    minim = new BigDecimal(minVal);
                }catch(Exception ex){}
                try{
                    maxim = new BigDecimal(maxVal);
                }catch(Exception ex){}
                
                try {
                    mLlSubhastes = mConexio.getFilteredSubhastes(filtreActiusTancats, nomOdesc, minim, maxim);
                    if(mLlSubhastes == null) throw new GestioSubhastesException("La llista recuperada es nula");
                } catch (GestioSubhastesException ex) {
                    JOptionPane.showMessageDialog(null, "Error al recuperar les dades de subhastes:\n " + ex.getMessage() + "\n" + ex.getCause());
                }
                fillValuesInTable(mLlSubhastes);
            }else if(mStringNetejar.equalsIgnoreCase(com)){
                netejarCamps();
            }else if(mStringNovaSubhasta.equalsIgnoreCase(com)){
                mostrarPantallaNovaSubhasta();
            }else if(mStringVeureInfoSubhastaSeleccionada.equalsIgnoreCase(com)){
                mostrarPantallaInfoSubhasta();
            }
        }

        private void netejarCamps() {
            mRdbAll.setSelected(true);
            mTxtNomDesc.setText("");
            mTxtMinValue.setText("0");
            mTxtMaxValue.setText("0");
            omplirTaulaSubhastes();
        }
        
        private void mostrarPantallaNovaSubhasta() {
            
            int i = mCboCatalegProd.getSelectedIndex();
            if(i < 0){
                JOptionPane.showMessageDialog(mJfrUI, "S'ha de seleccionar un producte");
                return;
            }
            ProducteCataleg pc = mLlProductes.get(i);
            
            UICreacioSubhasta ui = new UICreacioSubhasta(mJfrUI,pc,mStringNovaSubhasta, mConexio);
            ui.mostrarDialeg(); //En el moment que fem aixo ens congelem en el dialeg
            //Aqui recuperarem la subhasta i la farem persistent.
            //Actualitcem aqui la llista
            omplirTaulaSubhastes();
            omplirProductes();
        }

        private void mostrarPantallaInfoSubhasta() {
            Integer i = mTblTaulaDeSubhastes.getSelectedRow();
            if(i < 0){
                JOptionPane.showMessageDialog(mJfrUI, "S'ha de seleccionar una subhasta per veure la seva informacio");
                return;
            }
            
            Subhasta s = mLlSubhastes.get(i);
            
            UIPanellDetall ui;
            try {
                ui = new UIPanellDetall(mJfrUI,mStringVeureInfoSubhastaSeleccionada, s, mConexio, mFitxerProps);
                ui.mostrarDialeg();
                omplirTaulaSubhastes();
            } catch (GestioSubhastesException ex) {
                JOptionPane.showMessageDialog(mJfrUI, "Error al carregar la informacio de la subhasta: " + ex.getMessage());
                return;
            }
        }

        private Boolean sonTotsActiusOTancats() {
            ButtonModel m = mRdbGroupFiltres.getSelection();
            Boolean actiu = null;
            if(m != null){
                if(m.isSelected()){
                    String btnSelected = m.getActionCommand();
                    if(mStringTots.equals(btnSelected)){
                        return null;
                    }else if(mStringActives.equals(btnSelected)){
                        return true;
                    }else{
                        return false;
                    }
                }
            }
            return null;
        }
    }
    
    // </editor-fold>
    
    /****************  LOGICA DE LA CLASE  ********************/
    
    // <editor-fold defaultstate="collapsed" desc="Variables">
    private ERadioButtonsSelection OpcioSeleccionada = ERadioButtonsSelection.TOTES;
    private IGestioSubhastes mConexio;
    private List<Subhasta> mLlSubhastes;
    private List<ProducteCataleg> mLlProductes;
    private File mFitxerProps;
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Constructors">
    public UIManager(IGestioSubhastes em, File mFitxerProps){
        initUI();
        initListeners();
        mConexio = em;
        this.mFitxerProps = mFitxerProps;
        omplirTaulaSubhastes();
        omplirProductes();
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Metodes privats">
    private void omplirProductes() {
        try{
            mCboCatalegProd.removeAllItems();
            mLlProductes = new ArrayList<ProducteCataleg>();
            List<ProducteCataleg> ll = mConexio.getProductes();
            for(ProducteCataleg pc : ll){
                if(pc.getmSubhasta() == null){
                    mCboCatalegProd.addItem(pc.getNom());
                    mLlProductes.add(pc);
                }
            }
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "Error al recuperar les dades de producte:\n " + ex.getMessage() + "\n" + ex.getCause());
            System.exit(-1);
        }
    }
    
    private void omplirTaulaSubhastes() {
        try {
            mLlSubhastes = mConexio.getSubhastes();
        } catch (GestioSubhastesException ex) {
            JOptionPane.showMessageDialog(null, "Error al recuperar les dades de subhastes:\n " + ex.getMessage() + "\n" + ex.getCause());
            System.exit(-1);
        }
        
        fillValuesInTable(mLlSubhastes);
    }

    private void fillValuesInTable(List<Subhasta> llSubhastes) {
        int nRows = mTaulaSub.getRowCount();
        for(int i=0; i < nRows;i++){
            mTaulaSub.removeRow(i);
            nRows --;
            i--;
        }
        
        for(Subhasta s:llSubhastes){
            Oferta bestOferta = s.getMaxOferta();
            String best = "";
            if(bestOferta != null){
                best = bestOferta.getImport1().toString();
            }
            SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
            mTaulaSub.addRow(new Object[]{
                    s.getProductecatalegCod().getNom(),
                    s.getPreupartida().toString(),
                    best,
                    dt1.format(s.getDatafinalitzacio())
            });
        }
    }
    // </editor-fold>
}
