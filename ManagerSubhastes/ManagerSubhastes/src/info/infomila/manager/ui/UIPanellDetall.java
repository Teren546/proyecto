/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.infomila.manager.ui;

import info.infomila.interficieSubhastes.GestioSubhastesException;
import info.infomila.interficieSubhastes.IGestioSubhastes;
import info.infomila.model.CanalOferta;
import info.infomila.model.Oferta;
import info.infomila.model.ProducteCataleg;
import info.infomila.model.Subhasta;
import info.infomila.model.Usuari;
import info.infomila.postgresql.SubhastesPostgreSQL;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Adria
 */
public class UIPanellDetall {
    
    // <editor-fold defaultstate="collapsed" desc="Final strings">
    private final String mStringTitol = "Llista d'ofertes de la subhasta: ";
    private final String mStringTipusOferta = "Tipus de oferta";
    private final String mStringValorNovaOferta = "Valor nova oferta: ";
    private final String mStringTelefon = "TELEFONICA";
    private final String mStringSala = "SALA";
    private final String mStringEuros = "€";
    private final String mStringAfegirOferta = "Afegir oferta";
    private final String mStringUsuari = "Usuari";
    private final String mStringTancarSubhasta = "Tancar subhasta";
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Variables interficie grafica">
    private final JFrame mFramePare;
    private final String mTitol;
    private JDialog mDialeg;
    private JPanel mContenidor;
    private JScrollPane mPnlTaulaOfertes;
    private JTable mTblTaulaDeOfertas;
    private final String[] mTitolsTaula = {"Alias","Import","Data"};
    private JComboBox mCboCanalOferta, mCboUsuaris;
    private JFormattedTextField mTxtNovaOferta;
    private JButton mBtnAfegirOferta;
    private JLabel mLblTitolFi;
    private JButton mBtnTancarSubhasta;
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Variables">
    private Subhasta mSubhastaActual;
    private List<Usuari> mUsuaris = new ArrayList<Usuari>();
    private List<Oferta> mOfertes = new ArrayList<Oferta>();
    private final IGestioSubhastes mConexio;
    private DefaultTableModel mTaulaSub;
    private Thread mConsultadorOfertes;
    private Boolean mFiThread = false;
    private File mFitxerProps;
    // </editor-fold>
    
    public UIPanellDetall(Frame owner, String title, Subhasta sh, IGestioSubhastes em, File mFitxerProps) throws GestioSubhastesException {
        this.mSubhastaActual = sh;
        mFramePare = (JFrame) owner;
        mTitol = title;
        mConexio = em;
        this.mFitxerProps = mFitxerProps;
        initUI();
        
        
        mUsuaris = mConexio.getUsuaris();
        int numOf = sh.getNumOfertes();
        for(int i=0; i < numOf; i++){
            mOfertes.add(sh.getOfertaAt(i));
        }
        omplirCBOUsuaris();
        omplirTaulaOfertes();
        
        initThread();
    }

    // <editor-fold defaultstate="collapsed" desc="Creacio de interficie Grafica">
    private void initUI() {
        mDialeg = new JDialog(mFramePare);
        mDialeg.setTitle(mTitol);
        mDialeg.setSize(new Dimension(1000, 600));
        mDialeg.setMinimumSize(new Dimension(1000, 600));
        mDialeg.setResizable(true);
        mDialeg.setLocationRelativeTo(mFramePare);
        mDialeg.setModal(true);
        mDialeg.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                mFiThread = true;
            }
        });
        
        mContenidor = new JPanel();
        mContenidor.setLayout(new BorderLayout());
        
        mDialeg.add(mContenidor);
        
        omplirContenidor();
    }
    
    public void mostrarDialeg(){
        mDialeg.setVisible(true);
    }

    private void omplirContenidor() {
        omplirPartSuperior();
        
        omplirPartCentral();
        
        omplirPartInferior();
    }

    private void omplirPartSuperior() {
        //PART SUPERIOR
        JPanel jp = new JPanel();
        Font f = new Font("arial", Font.BOLD, 16);
        JLabel lblTitol = new JLabel(mStringTitol + " " + mSubhastaActual.getProductecatalegCod().getNom());
        lblTitol.setFont(f);
        jp.add(lblTitol);
        
        if(mSubhastaActual.getDatafinalitzacio().compareTo(new Date()) <= 0){
            if(mSubhastaActual.getMaxOferta() == null){
                mLblTitolFi = new JLabel("No s'ha venut 0 ofertes");
                mLblTitolFi.setFont(f);
                jp.add(mLblTitolFi);
            }else{
                mLblTitolFi = new JLabel("Venut per: " + mSubhastaActual.getMaxOferta().getImport1());
                mLblTitolFi.setFont(f);
                jp.add(mLblTitolFi);
            }
            
        }else{
            if(mSubhastaActual.getMaxOferta() == null){
                mLblTitolFi = new JLabel("Preu actual: " + mSubhastaActual.getPreupartida());
                mLblTitolFi.setFont(f);
                jp.add(mLblTitolFi);
            }else{
                mLblTitolFi = new JLabel("Preu actual: " + mSubhastaActual.getMaxOferta().getImport1());
                mLblTitolFi.setFont(f);
                jp.add(mLblTitolFi);
            }
        }
        
        mContenidor.add(jp,BorderLayout.NORTH);
    }

    private void omplirPartCentral() {
        //PART CENTRAL
        mTaulaSub = new DefaultTableModel();
        mTblTaulaDeOfertas = new JTable(mTaulaSub) {
            @Override
            public boolean isCellEditable(int row, int column) // Per aconseguir que la columna 2 sigui editable
            {
                return false;
            }

            @Override
            public Class<?> getColumnClass(int column) {
                Class clazz = String.class;
                return clazz;
            }

        };
        for(String s:mTitolsTaula){
            mTaulaSub.addColumn(s);
        }
        
        mPnlTaulaOfertes = new JScrollPane(mTblTaulaDeOfertas);
        mContenidor.add(mPnlTaulaOfertes,BorderLayout.CENTER);
    }

    private void omplirPartInferior() {
        //PART INFERIOR
        JPanel jpBot = new JPanel();
        JLabel lblTipusOferta = new JLabel(mStringTipusOferta);
        mCboCanalOferta = new JComboBox();
        mCboCanalOferta.addItem(mStringSala);
        mCboCanalOferta.addItem(mStringTelefon);
        
        NumberFormat formatDiners = NumberFormat.getNumberInstance();
        mTxtNovaOferta = new JFormattedTextField(formatDiners);
        mTxtNovaOferta.setValue(new Double(0));
        mTxtNovaOferta.setColumns(10);
        
        JLabel lblNoveOferta = new JLabel(mStringValorNovaOferta);
        JLabel lblEuros = new JLabel(mStringEuros);
        
        mCboCanalOferta.setSelectedIndex(0);
        
        mBtnAfegirOferta = new JButton();
        mBtnAfegirOferta.setText(mStringAfegirOferta);
        
        mBtnAfegirOferta.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
                synchronized(mSubhastaActual){
                    if(mSubhastaActual.getDatafinalitzacio().compareTo(new Date()) <= 0){
                        JOptionPane.showMessageDialog(null, "S'ha acabat la subhasta");
                        mBtnAfegirOferta.setEnabled(false);
                    }

                    BigDecimal import1 = null;
                    try{

                        import1 = new BigDecimal(mTxtNovaOferta.getText().replace(".", "").replace(",", "."));
                        Oferta of = mSubhastaActual.getMaxOferta();
                        if(of != null){
                            if(of.getImport1().compareTo(import1) >= 0){
                                JOptionPane.showMessageDialog(null,"Error, el import no supera la ultima oferta");
                                return;
                            }
                        }
                        if(mSubhastaActual.getPreupartida().compareTo(import1) >= 0){
                            JOptionPane.showMessageDialog(null,"Error, el import no supera el preu de sortida");
                            return;
                        }
                    }catch(Exception ex){
                        JOptionPane.showMessageDialog(null, "El import introduit es incorrecte");
                        return;
                    }

                    int i = mCboCanalOferta.getSelectedIndex();
                    if(i < 0){
                        JOptionPane.showMessageDialog(null, "No s'ha seleccionat cap tipus de oferta");
                        return;
                    }
                    CanalOferta co = null;
                    if(i == 0){
                        co = CanalOferta.SALA;
                    }else{
                        co = CanalOferta.TELEFONICA;
                    }

                    i = mCboUsuaris.getSelectedIndex();
                    if(i < 0){
                        JOptionPane.showMessageDialog(null, "No s'ha seleccionat cap usuari");
                        return;
                    }
                    Usuari u = mUsuaris.get(i);
                    //Fem la oferta
                    Oferta of = new Oferta(import1, co, mSubhastaActual, u);

                        try {
                            mConexio.insertOferta(of);
                        } catch (GestioSubhastesException ex) {
                            JOptionPane.showMessageDialog(null, "Error al inserir la oferta");
                            return;
                        }
                        mOfertes.add(of);
                        mTaulaSub.addRow(new Object[]{
                                of.getUsuariCod().getAlias(),
                                of.getImport1().toString(),
                                of.getSubhasta().getDatafinalitzacio().toString()
                        });
                        mLblTitolFi.setText("Preu actual: " + mSubhastaActual.getMaxOferta().getImport1());
                }
            }
        });
        
        

        JLabel lblUsuari = new JLabel(mStringUsuari);
        mCboUsuaris = new JComboBox();
        
        mBtnTancarSubhasta = new JButton();
        mBtnTancarSubhasta.setText(mStringTancarSubhasta);
        mBtnTancarSubhasta.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                mSubhastaActual.setDatafinalitzacio(new Date());
                System.out.println(mSubhastaActual.getDatafinalitzacio());
                mBtnAfegirOferta.setEnabled(false);
                mBtnTancarSubhasta.setEnabled(false);
                if(mSubhastaActual.getMaxOferta() == null){
                    mLblTitolFi.setText("No s'ha venut 0 ofertes");
                }else{
                    mLblTitolFi.setText("Venut per: " + mSubhastaActual.getMaxOferta().getImport1());
                }
                try {
                    mConexio.commit();
                } catch (GestioSubhastesException ex) {
                    JOptionPane.showMessageDialog(null, "Error al tancar al subhasta");
                }
            }
        });
        
        if(mSubhastaActual.getDatafinalitzacio().compareTo(new Date()) <= 0){
            mBtnAfegirOferta.setEnabled(false);
            mBtnTancarSubhasta.setEnabled(false);
        }
        
        jpBot.add(lblUsuari);
        jpBot.add(mCboUsuaris);
        jpBot.add(lblTipusOferta);
        jpBot.add(mCboCanalOferta);
        jpBot.add(lblNoveOferta);
        jpBot.add(mTxtNovaOferta);
        jpBot.add(lblEuros);
        jpBot.add(mBtnAfegirOferta);
        jpBot.add(mBtnTancarSubhasta);

        mContenidor.add(jpBot,BorderLayout.SOUTH);
    }
    
    

    private void omplirCBOUsuaris() {
        for(Usuari u:mUsuaris){
            mCboUsuaris.addItem(u.getAlias());
        }
    }

    private void omplirTaulaOfertes() {
        for(Oferta of:mOfertes){
            mTaulaSub.addRow(new Object[]{
                    of.getUsuariCod().getAlias(),
                    of.getImport1().toString(),
                    of.getSubhasta().getDatafinalitzacio().toString()
            });
        }
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Threading">
    private void actualizarUI(){
        if(mSubhastaActual.getMaxOferta() == null){
            mLblTitolFi.setText("Preu actual: " + mSubhastaActual.getPreupartida());
        }else{
            mLblTitolFi.setText("Preu actual: " + mSubhastaActual.getMaxOferta().getImport1());
        }
        
        if(mSubhastaActual.getDatafinalitzacio().compareTo(new Date()) <= 0){
            mLblTitolFi = new JLabel("Venut per: " + mSubhastaActual.getMaxOferta().getImport1());
            mBtnTancarSubhasta.setEnabled(false);
            mBtnAfegirOferta.setEnabled(false);
        }
        
        //Netejem la taula i la llista d'ofertes
        mOfertes.clear();
        int nRows = mTaulaSub.getRowCount();
        for(int i=0; i < nRows;i++){
            mTaulaSub.removeRow(i);
            nRows --;
            i--;
        }
        
        int numOf = mSubhastaActual.getNumOfertes();
        for(int i=0; i < numOf; i++){
            Oferta of = mSubhastaActual.getOfertaAt(i);
            mOfertes.add(of);
            mTaulaSub.addRow(new Object[]{
                                of.getUsuariCod().getAlias(),
                                of.getImport1().toString(),
                                of.getSubhasta().getDatafinalitzacio().toString()
                        });
        }
    }

    private void initThread() {
        //Si la subhasta no esta activa no call fer el fil perque no hi hauran cambis en las ofertes
        if(!(mSubhastaActual.getDatafinalitzacio().compareTo(new Date()) <= 0)){
            mConsultadorOfertes = new Thread(new ComprobadorOfertes(this));
            mConsultadorOfertes.start();
        }
        
    }
    
    class ComprobadorOfertes implements Runnable{
        private IGestioSubhastes mConexioThread;
        private UIPanellDetall mUI;
        public ComprobadorOfertes(UIPanellDetall ui){
            mUI = ui;
        }
        
        @Override
        public void run() {
            try {
                Properties p = new Properties();
                String className = null;
                FileInputStream fs = null;
                try {
                    fs = new FileInputStream(mFitxerProps);
                    p.loadFromXML(fs);
                    className = p.getProperty("className");
                } catch (IOException ex) {
                    throw new Exception(ex);
                } finally {
                    if (fs != null) {
                        try {
                            fs.close();
                        } catch (IOException ex) {
                        }
                    }
                }

                mConexioThread = null;
                
                try {
                    mConexioThread = (IGestioSubhastes) Class.forName(className).getConstructor(File.class).newInstance(mFitxerProps);
                } catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                   throw new Exception(ex);
                }
                //mConexioThread = mConexio;
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, "Error al iniciar al crear el fil de recoleccio d'ofertes, les ofertes no s'actualitzaran");
            }
            while(!mFiThread){
                try {
                    Thread.sleep(20000);
                    synchronized(mSubhastaActual){
                        mSubhastaActual = mConexioThread.refreshOfertasFromSubhasta(mSubhastaActual);
                        mUI.actualizarUI();
                    }
                } catch (InterruptedException ex) {
                    Logger.getLogger(UIPanellDetall.class.getName()).log(Level.SEVERE, null, ex);
                } catch (GestioSubhastesException ex) {
                    Logger.getLogger(UIPanellDetall.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
    }
    // </editor-fold>
}
