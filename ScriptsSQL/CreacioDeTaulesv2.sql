
-- DROP TABLE categoria;

CREATE TABLE categoria
(
  codi serial NOT NULL,
  nom character varying(64) NOT NULL,
  categoria_pare integer,
  CONSTRAINT categoria_pkey PRIMARY KEY (codi),
  CONSTRAINT fk_categoria_categoria_c_pare FOREIGN KEY (categoria_pare)
      REFERENCES categoria (codi) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT unique_categoria_nom UNIQUE (nom)
);

-- Table: producte_cataleg

-- DROP TABLE producte_cataleg;

CREATE TABLE producte_cataleg
(
  numero_lot serial NOT NULL,
  desc_html text NOT NULL,
  nom character varying(64) NOT NULL,
  valor_estimat numeric(19,2) NOT NULL,
  categoria integer NOT NULL,
  CONSTRAINT producte_cataleg_pkey PRIMARY KEY (numero_lot),
  CONSTRAINT fk_prod_cataleg_categoria_categoria FOREIGN KEY (categoria)
      REFERENCES categoria (codi) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT unique_prod_cataleg_nom UNIQUE (nom)
);
  
-- Table: productecataleg_mllistafotos

-- DROP TABLE productecataleg_mllistafotos;

CREATE TABLE productecataleg_mllistafotos
(
  productecataleg_numero_lot integer NOT NULL,
  caption text NOT NULL,
  productecataleg_cod integer NOT NULL,
  url character varying(2048) NOT NULL,
  CONSTRAINT fk_prodcatfotos_prodcat_prod_cod FOREIGN KEY (productecataleg_cod)
      REFERENCES producte_cataleg (numero_lot) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_prod_catfotos_prod_cat_numlot FOREIGN KEY (productecataleg_numero_lot)
      REFERENCES producte_cataleg (numero_lot) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);


-- Table: subhasta

-- DROP TABLE subhasta;

CREATE TABLE subhasta
(
  data_finalitzacio date NOT NULL,
  preu_partida numeric(19,2) NOT NULL,
  producte_cataleg integer NOT NULL,
  CONSTRAINT subhasta_pkey PRIMARY KEY (producte_cataleg),
  CONSTRAINT fk_subhasta_prod_cataleg_pc FOREIGN KEY (producte_cataleg)
      REFERENCES producte_cataleg (numero_lot) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);


-- Table: usuari

-- DROP TABLE usuari;

CREATE TABLE usuari
(
  codi serial NOT NULL,
  alias character varying(64) NOT NULL,
  cvv integer NOT NULL,
  caducitat date NOT NULL,
  cognoms character varying(64) NOT NULL,
  nom character varying(64) NOT NULL,
  numero_tarja character varying(16) NOT NULL,
  tipus_tarja character varying(255) NOT NULL,
  titular character varying(64) NOT NULL,
  CONSTRAINT usuari_pkey PRIMARY KEY (codi),
  CONSTRAINT unique_usuari_alias UNIQUE (alias)
);

-- Table: oferta

-- DROP TABLE oferta;

CREATE TABLE oferta
(
  codi integer NOT NULL,
  canal_oferta character varying(255) NOT NULL,
  import numeric(19,2) NOT NULL,
  subhasta_cod integer NOT NULL,
  usuari_cod integer NOT NULL,
  CONSTRAINT pk_oferta PRIMARY KEY (codi, subhasta_cod),
  CONSTRAINT fk_oferta_usuari_usuari_cod FOREIGN KEY (usuari_cod)
      REFERENCES usuari (codi) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_oferta_subhasta_subhasta_cod FOREIGN KEY (subhasta_cod)
      REFERENCES subhasta (producte_cataleg) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);