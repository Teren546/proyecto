
Insert into usuari(alias,cvv,caducitat,cognoms,nom,numero_tarja,tipus_tarja,titular)  values ('Teren546',323,'2016-06-23','Gomez','Adria','1234567890123456','VISA','Titular');
Insert into usuari(alias,cvv,caducitat,cognoms,nom,numero_tarja,tipus_tarja,titular)  values ('Briklindark',323,'2016-06-23','Bernaus','Oriol','1234567890123456','VISA','Titular');
Insert into usuari(alias,cvv,caducitat,cognoms,nom,numero_tarja,tipus_tarja,titular)  values ('Yangus',323,'2016-06-23','Fernandez','David','1234567890123456','VISA','Titular');
Insert into usuari(alias,cvv,caducitat,cognoms,nom,numero_tarja,tipus_tarja,titular)  values ('Dragonslayex',323,'2016-06-23','Ocaña','Victor','1234567890123456','VISA','Titular');

--Categoria fotos
Insert into categoria(nom) values ('JUEGOS');
--Rayman
Insert into producte_cataleg(desc_html,nom,valor_estimat,categoria) values ('<html></html>','Rayman',1,'1');
Insert into subhasta(data_finalitzacio,preu_partida,producte_cataleg) values ('2016-06-22',5,1);
Insert into productecataleg_mllistafotos(productecataleg_numero_lot,caption,productecataleg_cod,url) values(1,'blablabla',1,'http://92.222.27.83/~m2.agomez/Rayman_en_rayman_3.jpg');
Insert into productecataleg_mllistafotos(productecataleg_numero_lot,caption,productecataleg_cod,url) values(1,'blablabla',1,'http://raymanpc.com/wiki/images/thumb/c/c8/R1_site_Rayman.jpg/200px-R1_site_Rayman.jpg');
Insert into productecataleg_mllistafotos(productecataleg_numero_lot,caption,productecataleg_cod,url) values(1,'blablabla',1,'http://www.rayman-fanpage.de/allgames/g2/box_spain/rayman1_dos_windows_box550_front_eu.jpg');
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (1,'ONLINE',10,1,1);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (2,'ONLINE',11,1,2);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (3,'ONLINE',12,1,3);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (4,'ONLINE',13,1,4);
--WOW
Insert into producte_cataleg(desc_html,nom,valor_estimat,categoria) values ('<html></html>','World of Warcraft',4,'1');
Insert into subhasta(data_finalitzacio,preu_partida,producte_cataleg) values ('2016-06-22',5,2);
Insert into productecataleg_mllistafotos(productecataleg_numero_lot,caption,productecataleg_cod,url) values(2,'blablabla',2,'http://bnetcmsus-a.akamaihd.net/cms/blog_header/3a/3AT84RAJGA3E1442463434456.jpg');
Insert into productecataleg_mllistafotos(productecataleg_numero_lot,caption,productecataleg_cod,url) values(2,'blablabla',2,'http://92.222.27.83/~m2.agomez/WoWlogo.png');
Insert into productecataleg_mllistafotos(productecataleg_numero_lot,caption,productecataleg_cod,url) values(2,'blablabla',2,'http://i13b.3djuegos.com/juegos/607/world_of_warcraft/fotos/maestras/world_of_warcraft-3238593.jpg');
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (1,'ONLINE',10,2,1);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (2,'ONLINE',11,2,2);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (3,'ONLINE',12,2,3);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (4,'ONLINE',13,2,4);
--LOL
Insert into producte_cataleg(desc_html,nom,valor_estimat,categoria) values ('<html></html>','League of leagends',9,'1');
Insert into subhasta(data_finalitzacio,preu_partida,producte_cataleg) values ('2016-06-22',5,3);
Insert into productecataleg_mllistafotos(productecataleg_numero_lot,caption,productecataleg_cod,url) values(3,'blablabla',3,'https://upload.wikimedia.org/wikipedia/en/7/77/League_of_Legends_logo.png');
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (1,'ONLINE',10,3,1);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (2,'ONLINE',11,3,2);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (3,'ONLINE',12,3,3);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (4,'ONLINE',13,3,4);
--Uncharted
Insert into producte_cataleg(desc_html,nom,valor_estimat,categoria) values ('<html></html>','Uncharted',16,'1');
Insert into subhasta(data_finalitzacio,preu_partida,producte_cataleg) values ('2016-06-22',5,4);
Insert into productecataleg_mllistafotos(productecataleg_numero_lot,caption,productecataleg_cod,url) values(4,'blablabla',4,'https://farm6.staticflickr.com/5624/23815901722_4d1edf4ed1_b.jpg');
Insert into productecataleg_mllistafotos(productecataleg_numero_lot,caption,productecataleg_cod,url) values(4,'blablabla',4,'https://i.ytimg.com/vi/hh5HV4iic1Y/maxresdefault.jpg');
Insert into productecataleg_mllistafotos(productecataleg_numero_lot,caption,productecataleg_cod,url) values(4,'blablabla',4,'https://psmedia.playstation.com/is/image/psmedia/uncharted-4-multiplayer-screen-17-ps4-eu-28oct15?$MediaCarousel_LargeImage$');
Insert into productecataleg_mllistafotos(productecataleg_numero_lot,caption,productecataleg_cod,url) values(4,'blablabla',4,'http://gematsu.com/wp-content/uploads/2016/02/Uncharted-4-Story-Trailer.jpg');
Insert into productecataleg_mllistafotos(productecataleg_numero_lot,caption,productecataleg_cod,url) values(4,'blablabla',4,'http://i.blogs.es/64fa58/uncharted_4-3121125/original.jpg');
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (1,'ONLINE',10,4,1);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (2,'ONLINE',11,4,2);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (3,'ONLINE',12,4,3);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (4,'ONLINE',13,4,4);
--MGSV
Insert into producte_cataleg(desc_html,nom,valor_estimat,categoria) values ('<html></html>','MGSV',25,'1');
Insert into subhasta(data_finalitzacio,preu_partida,producte_cataleg) values ('2016-06-22',5,5);
Insert into productecataleg_mllistafotos(productecataleg_numero_lot,caption,productecataleg_cod,url) values(5,'blablabla',5,'http://static.gosunoob.com/img/1/2015/08/mgsv_the_phantom_pain_wiki_guides-1024x559.jpg');
Insert into productecataleg_mllistafotos(productecataleg_numero_lot,caption,productecataleg_cod,url) values(5,'blablabla',5,'http://92.222.27.83/~m2.agomez/phantom-pain.jpg');
Insert into productecataleg_mllistafotos(productecataleg_numero_lot,caption,productecataleg_cod,url) values(5,'blablabla',5,'https://metrouk2.files.wordpress.com/2015/08/mgsv-the-phantom-pain-ash-face.jpg?quality=80&strip=all&strip=all');
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (1,'ONLINE',10,5,1);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (2,'ONLINE',11,5,2);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (3,'ONLINE',12,5,3);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (4,'ONLINE',13,5,4);
--ARTE
Insert into categoria(nom) values ('ARTE');
--El sueño
Insert into producte_cataleg(desc_html,nom,valor_estimat,categoria) values ('<html></html>','El sueño',6,'2');
Insert into subhasta(data_finalitzacio,preu_partida,producte_cataleg) values ('2016-06-22',5,6);
Insert into productecataleg_mllistafotos(productecataleg_numero_lot,caption,productecataleg_cod,url) values(6,'blablabla',6,'http://hyperbole.es/wp-content/uploads/2013/03/D4CF0B72A.jpg');
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (1,'ONLINE',10,6,1);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (2,'ONLINE',11,6,2);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (3,'ONLINE',12,6,3);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (4,'ONLINE',13,6,4);
--Los jugadores de cartas
Insert into producte_cataleg(desc_html,nom,valor_estimat,categoria) values ('<html></html>','Los jugadores de cartas',14,'2');
Insert into subhasta(data_finalitzacio,preu_partida,producte_cataleg) values ('2016-06-22',5,7);
Insert into productecataleg_mllistafotos(productecataleg_numero_lot,caption,productecataleg_cod,url) values(7,'blablabla',7,'https://upload.wikimedia.org/wikipedia/commons/thumb/9/9c/Paul_C%C3%A9zanne_-_Les_Joueurs_de_cartes.jpg/270px-Paul_C%C3%A9zanne_-_Les_Joueurs_de_cartes.jpg');
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (1,'ONLINE',10,7,1);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (2,'ONLINE',11,7,2);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (3,'ONLINE',12,7,3);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (4,'ONLINE',13,7,4);
--Tres estudios
Insert into producte_cataleg(desc_html,nom,valor_estimat,categoria) values ('<html></html>','Tres estudios',24,'2');
Insert into subhasta(data_finalitzacio,preu_partida,producte_cataleg) values ('2016-06-22',5,8);
Insert into productecataleg_mllistafotos(productecataleg_numero_lot,caption,productecataleg_cod,url) values(8,'blablabla',8,'https://udual.files.wordpress.com/2013/11/sin-tc3adtulo138.jpg');
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (1,'ONLINE',10,8,1);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (2,'ONLINE',11,8,2);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (3,'ONLINE',12,8,3);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (4,'ONLINE',13,8,4);
--Number 5
Insert into producte_cataleg(desc_html,nom,valor_estimat,categoria) values ('<html></html>','Number 5',36,'2');
Insert into productecataleg_mllistafotos(productecataleg_numero_lot,caption,productecataleg_cod,url) values(9,'blablabla',9,'http://1.bp.blogspot.com/-PhlfXcRnK9A/T8PGUIIvjGI/AAAAAAAAAC4/j5zlUMNhLws/s400/jacksonpollockno5.jpg');
--FUTBOL
Insert into categoria(nom) values ('FUTBOL');
--Pelota
Insert into producte_cataleg(desc_html,nom,valor_estimat,categoria) values ('<html></html>','Pelota',10,'3');
Insert into subhasta(data_finalitzacio,preu_partida,producte_cataleg) values ('2016-06-22',5,10);
Insert into productecataleg_mllistafotos(productecataleg_numero_lot,caption,productecataleg_cod,url) values(10,'blablabla',10,'http://culturacientifica.com/app/uploads/2014/11/3.jpg');
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (1,'ONLINE',10,10,1);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (2,'ONLINE',11,10,2);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (3,'ONLINE',12,10,3);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (4,'ONLINE',13,10,4);
--Camiseta
Insert into producte_cataleg(desc_html,nom,valor_estimat,categoria) values ('<html></html>','Camiseta',22,'3');
Insert into subhasta(data_finalitzacio,preu_partida,producte_cataleg) values ('2016-06-22',5,11);
Insert into productecataleg_mllistafotos(productecataleg_numero_lot,caption,productecataleg_cod,url) values(11,'blablabla',11,'http://www.equipadeporte.com/10764-thickbox/camisetas-futbol-baratas-mercury-equipacion-futbol-manga-corta-roma.jpg');
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (1,'ONLINE',10,11,1);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (2,'ONLINE',11,11,2);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (3,'ONLINE',12,11,3);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (4,'ONLINE',13,11,4);
--Botas
Insert into producte_cataleg(desc_html,nom,valor_estimat,categoria) values ('<html></html>','Botas',36,'3');
Insert into subhasta(data_finalitzacio,preu_partida,producte_cataleg) values ('2016-05-22',5,12);

--INFORMATICA
Insert into categoria(nom) values ('INFORMATICA');
--portatil
Insert into producte_cataleg(desc_html,nom,valor_estimat,categoria) values ('<html></html>','Portatil',13,'4');
Insert into subhasta(data_finalitzacio,preu_partida,producte_cataleg) values ('2016-06-22',5,13);
Insert into productecataleg_mllistafotos(productecataleg_numero_lot,caption,productecataleg_cod,url) values(13,'blablabla',13,'http://www.pcactual.com/medio/2011/03/08/portatil_asus_13_pulgadas_618x465.jpg');
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (1,'ONLINE',10,13,1);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (2,'ONLINE',11,13,2);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (3,'ONLINE',12,13,3);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (4,'ONLINE',13,13,4);
--sobremesa
Insert into producte_cataleg(desc_html,nom,valor_estimat,categoria) values ('<html></html>','Sobremesa',28,'4');
Insert into subhasta(data_finalitzacio,preu_partida,producte_cataleg) values ('2016-06-22',5,14);
Insert into productecataleg_mllistafotos(productecataleg_numero_lot,caption,productecataleg_cod,url) values(14,'blablabla',14,'http://www.idhardware.com/wp-content/uploads/2012/07/imagen-equipo.jpg');
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (1,'ONLINE',10,14,1);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (2,'ONLINE',11,14,2);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (3,'ONLINE',12,14,3);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (4,'ONLINE',13,14,4);
--nvidia
Insert into producte_cataleg(desc_html,nom,valor_estimat,categoria) values ('<html></html>','Grafica Nvidia',45,'4');
Insert into subhasta(data_finalitzacio,preu_partida,producte_cataleg) values ('2016-06-22',5,15);
Insert into productecataleg_mllistafotos(productecataleg_numero_lot,caption,productecataleg_cod,url) values(15,'blablabla',15,'http://www.fsgamer.com/wp-content/uploads/sites/2/2016/05/87774-nvidia-geforce-gtx-1080-.jpg');
Insert into productecataleg_mllistafotos(productecataleg_numero_lot,caption,productecataleg_cod,url) values(15,'blablabla',15,'http://static1.gamespot.com/uploads/original/1568/15683559/3063641-gtx_1080_kv_1462594232.jpg');
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (1,'ONLINE',10,15,1);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (2,'ONLINE',11,15,2);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (3,'ONLINE',12,15,3);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (4,'ONLINE',13,15,4);
--I7
Insert into producte_cataleg(desc_html,nom,valor_estimat,categoria) values ('<html></html>','Procesador i7',64,'4');
Insert into subhasta(data_finalitzacio,preu_partida,producte_cataleg) values ('2016-06-22',5,16);
Insert into productecataleg_mllistafotos(productecataleg_numero_lot,caption,productecataleg_cod,url) values(16,'blablabla',16,'http://ecx.images-amazon.com/images/I/91vUJpgWLEL._SL1500_.jpg');
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (1,'ONLINE',10,16,1);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (2,'ONLINE',11,16,2);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (3,'ONLINE',12,16,3);
Insert into oferta(codi,canal_oferta,import,subhasta_cod,usuari_cod) values (4,'ONLINE',13,16,4);
