/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.infomila.servidor.clientdeProves;

import info.infomila.model.TipusTarja;
import info.infomila.model.Usuari;
import info.infomila.servidor.ui.UIServidor;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.KeyStore;
import java.util.Date;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

/**
 *
 * @author Adria
 */
public class Client {
    
    
    
    public static void main(String[] args) {
        SSLSocket client;
        try {
            KeyStore keyStore = KeyStore.getInstance("JKS");
            keyStore.load(new FileInputStream("src/main/certs/client/clientKey.jks"),
                  "clientpass".toCharArray());

            KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            kmf.init(keyStore, "clientpass".toCharArray());

            KeyStore trustedStore = KeyStore.getInstance("JKS");
            trustedStore.load(new FileInputStream(
                  "src/main/certs/client/clientTrustedCerts.jks"), "clientpass"
                  .toCharArray());

            TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(trustedStore);

            SSLContext sc = SSLContext.getInstance("TLS");
            TrustManager[] trustManagers = tmf.getTrustManagers();
            KeyManager[] keyManagers = kmf.getKeyManagers();
            sc.init(keyManagers, trustManagers, null);

            SSLSocketFactory ssf = sc.getSocketFactory();
            client = (SSLSocket) ssf.createSocket("localhost", 1234);
            client.startHandshake();
            
            ObjectOutputStream writer = new ObjectOutputStream(client.getOutputStream());
            ObjectInputStream reader=new ObjectInputStream(client.getInputStream());
            int opcio = 0;
            Scanner scanner = new Scanner(System.in);
            while(opcio!=2){
                try{
                    System.out.println("Entra numero: ");
                    int i = scanner.nextInt();
                    writer.writeObject(i);
                    
                    Usuari u = new Usuari("nom", "1234567891234567", "dsjnfa", 555, new Date(), TipusTarja.VISA, "nom", "cognom");
                    writer.writeObject(u);
                }catch(Exception ex){
                    System.out.println(ex.getMessage());
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(UIServidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
