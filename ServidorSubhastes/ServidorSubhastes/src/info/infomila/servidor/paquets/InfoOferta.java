package info.infomila.servidor.paquets;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by Adria on 27/05/2016.
 */
public class InfoOferta implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private BigDecimal importe;
    private String aliesOfertant;

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    public String getAliesOfertant() {
        return aliesOfertant;
    }

    public void setAliesOfertant(String aliesOfertant) {
        this.aliesOfertant = aliesOfertant;
    }

    public InfoOferta(BigDecimal importe, String aliesOfertant) {
        this.importe = importe;
        this.aliesOfertant = aliesOfertant;
    }
}
