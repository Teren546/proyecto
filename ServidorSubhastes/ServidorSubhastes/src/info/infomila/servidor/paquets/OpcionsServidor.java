/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.infomila.servidor.paquets;

import java.io.Serializable;

/**
 *
 * @author Adria
 */
public enum OpcionsServidor implements Serializable {
    REGISTRAR_USUARI,
    Llista_Subhastes_Actives,
    Llista_Subhastes_From_Usuari,
    INSCRIPCIO_SUBHASTA,
    INFO_PRODUCTE,
    AFEGIR_OFERTA,
    ACTUALITZAR_USUARI,
    GET_INFO_OFERTES,
    SORTIR
}
