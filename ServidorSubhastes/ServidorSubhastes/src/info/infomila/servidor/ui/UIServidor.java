/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.infomila.servidor.ui;

import info.infomila.interficieSubhastes.GestioSubhastesException;
import info.infomila.interficieSubhastes.IGestioSubhastes;
import info.infomila.model.CanalOferta;
import info.infomila.model.Foto;
import info.infomila.model.Oferta;
import info.infomila.model.ProducteCataleg;
import info.infomila.model.Subhasta;
import info.infomila.model.Usuari;
import info.infomila.servidor.paquets.DadesSubhasta;
import info.infomila.servidor.paquets.InfoOferta;
import info.infomila.servidor.paquets.InformacioProducte;
import info.infomila.servidor.paquets.OpcionsServidor;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

/**
 *
 * @author Adria
 */
public class UIServidor {
    
    // <editor-fold defaultstate="collapsed" desc="Final Strings">
    
    private final String mStringAturar = "Aturar";
    private final String mStringEngegar = "Engegar";
    private final Integer mPORTServidor = 1234;
    private final String mStringNetejar = "Netejar";
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Interficie grafica">
    private JFrame mUIPrincipal;
    private JPanel mPnlContenidor,mPnlBot;
    private IGestioSubhastes mConexio;
    private JButton mBtnEngegar,mBtnAturar;
    private JTextArea mTxtLogZone;
    private JScrollPane mScrPnlForLog;
    private JButton mBtnNetejar;
    private JButton mBtnEnviarActu;
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Variables del servidor">
    private Thread mThreadAccepts;
    private SSLSocket mSocketAccepts;
    private SSLServerSocketFactory mSocketSSLFactory;
    private SSLServerSocket mServerSocket;
    private List<SSLSocket> mClientsConectats;
    private Boolean mServerStop = false;
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Actions Listeners Botons">
    private void addActionListeners() {
        
        mBtnEngegar.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    mTxtLogZone.setText("INIT LOG");
                    mConexio.reconnect();
                    mBtnAturar.setEnabled(true);
                    mBtnEngegar.setEnabled(false);
                    mServerStop = false;
                    obrirSocket();
                    startThread();
                } catch (GestioSubhastesException | IOException ex) {
                    JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage());
                }
            }
        });
        
        mBtnAturar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    mConexio.close();
                    mBtnEngegar.setEnabled(true);
                    mBtnAturar.setEnabled(false);
                    mServerSocket.close();
                    mServerStop = true;
                    mThreadAccepts = null;
                } catch (GestioSubhastesException | IOException ex) {
                    JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage());
                }
            }
        });
        
        mBtnNetejar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                UIServidor.this.netejarLog();
            }
        });
        
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Creacio de la UI">
    
    public UIServidor(IGestioSubhastes conexio) {
        mConexio = conexio;
        init();
        this.afegirAlLog("--- INIT LOG ----");
        this.afegirAlLog("Añadiendo propiedades SSL");
        afegirPropietatsSSL();
        startThread();
    }

    private void init() {
        mUIPrincipal = new JFrame();
        mUIPrincipal.setLayout(new BorderLayout());
        
        mPnlContenidor = new JPanel();
        mPnlContenidor.setLayout(new BorderLayout());
        omplirPanellContenidor();
        addActionListeners();
        
        mUIPrincipal.add(mPnlContenidor);
        
        mUIPrincipal.setSize(800, 600);
        mUIPrincipal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mUIPrincipal.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                try {
                    mConexio.close();
                } catch (GestioSubhastesException ex) {
                    JOptionPane.showMessageDialog(mUIPrincipal, "Error al tancar la conexio");
                }
            }
        });
    }

    private void omplirPanellContenidor() {
        mBtnEngegar = new JButton();
        mBtnAturar = new JButton();
        mBtnNetejar = new JButton();
        
        mBtnEngegar.setText(mStringEngegar);
        mBtnAturar.setText(mStringAturar);
        mBtnNetejar.setText(mStringNetejar);
        
        mTxtLogZone = new JTextArea();
        mTxtLogZone.setEditable(false);
        mScrPnlForLog = new JScrollPane(mTxtLogZone);
        mScrPnlForLog.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        
        mPnlBot = new JPanel();
        mPnlBot.add(mBtnEngegar);
        mPnlBot.add(mBtnAturar);
        mPnlBot.add(mBtnNetejar);
        
        mBtnEngegar.setEnabled(false);
        
        mPnlContenidor.add(mScrPnlForLog, BorderLayout.CENTER);
        mPnlContenidor.add(mPnlBot, BorderLayout.SOUTH);
    }
    
    public void mostrarUI(){
        mUIPrincipal.setVisible(true);
    }
    
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Gestio del servidor">
    private void startThread() {
        if(mThreadAccepts == null){
            mThreadAccepts = new Thread(new ServidorThread(this));
            mThreadAccepts.start();
            synchronized(mTxtLogZone){
                this.afegirAlLog("Thread per acceptar clients creat");
            }
        }
    }
    
    public void afegirAlLog(String s){
        String log = mTxtLogZone.getText();
        log += "\n" + s;
        mTxtLogZone.setText(log);
    }

    private void afegirPropietatsSSL() {
        try {
            Properties prop = new Properties();
            InputStream is = new FileInputStream("FitxerConfig.xml");
            prop.loadFromXML(is);
            String path = prop.getProperty("pathCertificats");
            
            KeyStore keyStore = KeyStore.getInstance("JKS");
            keyStore.load(new FileInputStream(path + "/serverKey.jks"),
                  "servpass".toCharArray());

            KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            kmf.init(keyStore, "servpass".toCharArray());

            KeyStore trustedStore = KeyStore.getInstance("JKS");
            trustedStore.load(new FileInputStream(
                  path + "/serverTrustedCerts.jks"), "servpass"
                  .toCharArray());

            TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(trustedStore);

            SSLContext sc = SSLContext.getInstance("TLS");
            TrustManager[] trustManagers = tmf.getTrustManagers();
            KeyManager[] keyManagers = kmf.getKeyManagers();
            sc.init(keyManagers, trustManagers, null);

            this.afegirAlLog("Creant factoria de Sockets SSL...");
            mSocketSSLFactory = sc.getServerSocketFactory();
            this.afegirAlLog("Factoria de sockets creada");
            obrirSocket();

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null,"Error al obrir el socket: " + ex.getMessage());
            System.exit(-1);
        }
    }

    private void obrirSocket() throws IOException {
        this.afegirAlLog("Creant SSL Socket amb al port: " + mPORTServidor);
        mServerSocket = (SSLServerSocket) mSocketSSLFactory.createServerSocket(mPORTServidor);
        this.afegirAlLog("Socket SSL Creat: " + mServerSocket.getInetAddress().toString());
    }

    private void netejarLog() {
        synchronized(mTxtLogZone){
            mTxtLogZone.setText("");
        }
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Servidor que reb accepts">
    class ServidorThread implements Runnable{
        
        private UIServidor tUI;
                
        public ServidorThread(UIServidor ui){
            tUI = ui;
        }
        
        @Override
        public void run() {
            try{
                synchronized(mTxtLogZone){
                    tUI.afegirAlLog("Servidor preparat. Esperant conexions...");
                }
                //Posem que nomes es necesita autenticacio del servidor
                mServerSocket.setNeedClientAuth(false);
                while(!mServerStop){
                    SSLSocket sslsocket =(SSLSocket) mServerSocket.accept();
                    synchronized(mTxtLogZone){
                        tUI.afegirAlLog("Usuari conectat: " + sslsocket.getInetAddress().toString());
                    }
                    try{
                        Thread t = new Thread(new AtencioClientThread(tUI,sslsocket));
                        t.start();
                        
                    }catch(Exception ex){
                        
                    }
                }
            }catch(Exception ex){
                if(ex.getMessage() != null){
                    if("socket closed".equalsIgnoreCase(ex.getMessage())){
                        tUI.afegirAlLog("Socket tancat");
                    }else{
                        tUI.afegirAlLog("Error en el servidor: \n"
                                + ex.getMessage() 
                                + "\n" 
                                + ex.getCause()
                                + "\n"
                        );
                        ex.printStackTrace();
                        JOptionPane.showMessageDialog(null, "Error al fer els accepts: " + ex.getMessage());
                    }
                    mThreadAccepts = null;
                }
            }
        }
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Gestio amb el client">
    class AtencioClientThread implements Runnable{

        private UIServidor tUI;
        private SSLSocket tSocket;
        private IGestioSubhastes tConexioThread;
//        private DataOutputStream writer;
//        private DataInputStream reader;
        
        private ObjectOutputStream writer;
        private ObjectInputStream reader;
        
        public AtencioClientThread(UIServidor ui, SSLSocket socket){
            tUI = ui;
            tSocket = socket;
        }
        
        @Override
        public void run() {
            tConexioThread = mConexio;
            try{
                writer=new ObjectOutputStream(tSocket.getOutputStream());
                reader=new ObjectInputStream(tSocket.getInputStream());
                Boolean sortir = false;
                OpcionsServidor opcio = (OpcionsServidor) reader.readObject();
                switch(opcio){
                    case REGISTRAR_USUARI:
                        registrarUsuari();
                        break;
                    case Llista_Subhastes_Actives:
                        getLlistaSubhastesActives();
                        break;
                    case Llista_Subhastes_From_Usuari:
                        getLlistaSubhastesFromUsuari();
                        break;
                    case INSCRIPCIO_SUBHASTA:
                        inscripcioSubhasta();
                        break;
                    case INFO_PRODUCTE:
                        getInfoProducte();
                        break;
                    case AFEGIR_OFERTA:
                        addOferta();
                        break;
                    case ACTUALITZAR_USUARI:
                        actualitzarUsuari();
                        break;
                    case GET_INFO_OFERTES:
                        getInformacioOfertes();
                        break;
                    case SORTIR:
                        sortir = true;
                        break;
                    default:
                        break;
                }
                synchronized(mTxtLogZone){
                    tUI.afegirAlLog("Tancant amb el client: " + tSocket.getInetAddress().toString());
                    tSocket.close();
                    tUI.afegirAlLog("Tancada conexio. ");
                }
            }catch(Exception ex){
                synchronized(mTxtLogZone){
                    tUI.afegirAlLog("Tancant amb el client: " + tSocket.getInetAddress().toString() + " per error: " + ex.getMessage());
                    try {
                        tSocket.close();
                    } catch (IOException ex1) {}
                    tUI.afegirAlLog("Tancada conexio. ");
                }
                ex.printStackTrace();
            }
        }

        private void registrarUsuari() throws IOException, ClassNotFoundException {
            //Llegim usuari
            Usuari u = (Usuari) reader.readObject();
            try {
                tUI.afegirAlLog("Intent de inserir el usuari: " + u);
                //Inserim
                tConexioThread.insertUsuariBD(u);
                //Recuperem
                writer.writeObject(u.getCodi());
                tUI.afegirAlLog("Usuari inserit amb codi: " + u.getCodi());
            } catch (GestioSubhastesException ex) {
                synchronized(mTxtLogZone){
                    tUI.afegirAlLog("Error al inserir el usuari: " + u);
                    tUI.afegirAlLog("Error: " + ex.getMessage() + " Cause:" + ex.getCause());
                }
                if(u.getCodi() == null){
                    //En cas de error enviem error
                    writer.writeObject(-1);
                }
            }
        }

        private void getLlistaSubhastesActives() throws IOException {
            List<Subhasta> llSubhastes = null;
            ArrayList<DadesSubhasta> llDadesSubhasta = new ArrayList<>();
            try{
                llSubhastes = tConexioThread.getActiveSubhastes();
                for(Subhasta s:llSubhastes){
                    Oferta o = s.getMaxOferta();
                    BigDecimal maxOferta = null;
                    String alies = null;
                    if(o != null){
                        alies = o.getUsuariCod().getAlias();
                        maxOferta = o.getImport1();
                    }else{
                        maxOferta = s.getPreupartida();
                    }
                    Foto f = s.getProductecatalegCod().getFotoAt(0);
                    String url = null;
                    if(f != null){
                        url = f.getUrl();
                    }
                    DadesSubhasta ds = new DadesSubhasta(s.getProductecatalegCod().getNumerolot(),s.getPreupartida(), maxOferta,url , s.getProductecatalegCod().getNom(), alies, s.getDatafinalitzacio());
                    llDadesSubhasta.add(ds);
                }
                for(DadesSubhasta ds: llDadesSubhasta){
                    writer.writeObject(ds);
                }
                //Enviamos fin succesful
                writer.writeObject(0);
            }catch(Exception ex){
                synchronized(mTxtLogZone){
                    tUI.afegirAlLog("Error: " + ex.getMessage() + " Cause:" + ex.getCause());
                }
                writer.writeObject(-1);
                throw new RuntimeException("Error", ex);
            }
        }

        private void inscripcioSubhasta() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        private void getInfoProducte() {
            try{
                tUI.afegirAlLog("Recuperant codi de subhasta.. ");
                Integer codiSub = (Integer) reader.readObject();
                tUI.afegirAlLog("Codi de subhasta " + codiSub);
                Subhasta sb = tConexioThread.getSubhastaByCodi(codiSub);
                ProducteCataleg pc = sb.getProductecatalegCod();
                ArrayList<String> ll = new ArrayList<String>();
                for(int i=0; i < pc.getNumFotos(); i++){
                    ll.add(pc.getFotoAt(i).getUrl());
                }
                InformacioProducte info = new InformacioProducte(pc.getNom(),ll,pc.getDeschtml());
                tUI.afegirAlLog("Enviant la informacio relacionada amb el producte");
                writer.writeObject(info);
            }catch(Exception ex){
                tUI.afegirAlLog("Error: " + ex.getMessage());
                ex.printStackTrace();
            }
        }

        private void addOferta() throws IOException, ClassNotFoundException {
            Oferta of = null;
            try{
                Integer codiSub = (Integer) reader.readObject();
                Subhasta sb = tConexioThread.getSubhastaByCodi(codiSub);
                //Enviem que hem trobat la subhasta
                if(sb.getDatafinalitzacio().compareTo(new Date()) < 0){
                    writer.writeObject(new Integer(-3));
                    return;
                }
                writer.writeObject(new Integer(0));
                
                BigDecimal importe = (BigDecimal) reader.readObject();
                Usuari u = (Usuari) reader.readObject();
                u = tConexioThread.getUsuariByCod(u.getCodi());
                try{
                    if(sb.getMaxOferta() == null){
                       if(importe.compareTo(sb.getPreupartida()) <= 0){
                           throw new RuntimeException("Error, la oferta es mes petita que el preu de sortida");
                       }
                    }else{
                        BigDecimal bd = sb.getMaxOferta().getImport1();
                        if(importe.compareTo(sb.getMaxOferta().getImport1()) <= 0){
                           throw new RuntimeException("Error, la oferta es mes petita que la oferta");
                       }
                    }
                    of = new Oferta(importe, CanalOferta.ONLINE, sb, u);
                    mConexio.insertOferta(of);
                    writer.writeObject(new Integer(0));
                }catch(Exception ex){
                    synchronized(mTxtLogZone){
                        tUI.afegirAlLog("Error: " + ex.getMessage() + " Cause:" + ex.getCause());
                    }
                    writer.writeObject(new Integer(-4));
                }
            } catch (GestioSubhastesException ex) {
                synchronized(mTxtLogZone){
                    tUI.afegirAlLog("Error: " + ex.getMessage() + " Cause:" + ex.getCause());
                }
                writer.writeObject(new Integer(-2));
            } catch (Exception ex){
                synchronized(mTxtLogZone){
                    tUI.afegirAlLog("Error: " + ex.getMessage() + " Cause:" + ex.getCause());
                }
                writer.writeObject(new Integer(-1));
            }
        }

        private void actualitzarUsuari() throws IOException, ClassNotFoundException {
            //Llegim usuari
            Usuari u = (Usuari) reader.readObject();
            try {
                tUI.afegirAlLog("Intent de actulitzar el usuari amb codi: " + u.getCodi());
                tUI.afegirAlLog("" + u);
                //Actualitzem
                tConexioThread.actualitzarUsuariBD(u);
                //Recuperem
                writer.writeObject(u.getCodi());
                tUI.afegirAlLog("Usuari actualitzat amb codi: " + u.getCodi());
            } catch (GestioSubhastesException ex) {
                synchronized(mTxtLogZone){
                    tUI.afegirAlLog("Error al actualitzat el usuari: " + u);
                    tUI.afegirAlLog("Error: " + ex.getMessage() + " Cause:" + ex.getCause());
                }
                if(u.getCodi() == null){
                    //En cas de error enviem error
                    writer.writeObject(-1);
                }
            }
        }

        private void getLlistaSubhastesFromUsuari() throws IOException {
            List<Subhasta> llSubhastes = null;
            ArrayList<DadesSubhasta> llDadesSubhasta = new ArrayList<>();
            try{
                int codi = (int) reader.readObject();
                llSubhastes = tConexioThread.getSubhastesFromUsuari(codi);
                for(Subhasta s:llSubhastes){
                    Oferta o = s.getMaxOferta();
                    BigDecimal maxOferta = null;
                    String alies = null;
                    if(o != null){
                        alies = o.getUsuariCod().getAlias();
                        maxOferta = o.getImport1();
                    }else{
                        maxOferta = s.getPreupartida();
                    }
                    Foto f = s.getProductecatalegCod().getFotoAt(0);
                    String url = null;
                    if(f != null){
                        url = f.getUrl();
                    }
                    DadesSubhasta ds = new DadesSubhasta(s.getProductecatalegCod().getNumerolot(),s.getPreupartida(), maxOferta,url , s.getProductecatalegCod().getNom(), alies, s.getDatafinalitzacio());
                    llDadesSubhasta.add(ds);
                }
                for(DadesSubhasta ds: llDadesSubhasta){
                    writer.writeObject(ds);
                }
                //Enviamos fin succesful
                writer.writeObject(0);
            }catch(Exception ex){
                synchronized(mTxtLogZone){
                    tUI.afegirAlLog("Error: " + ex.getMessage() + " Cause:" + ex.getCause());
                }
                writer.writeObject(-1);
                throw new RuntimeException("Error", ex);
            }
        }

        private void getInformacioOfertes() throws IOException, ClassNotFoundException {
            try{
                tUI.afegirAlLog("Recuperant codi de subhasta.. ");
                Integer codiSub = (Integer) reader.readObject();
                tUI.afegirAlLog("Codi de subhasta " + codiSub);
                Subhasta sb = tConexioThread.getSubhastaByCodi(codiSub);
                ArrayList<InfoOferta> llOfertes = new ArrayList<>();
                for(int i=0; i < sb.getNumOfertes(); i++){
                    Oferta o = sb.getOfertaAt(i);
                    Usuari u = o.getUsuariCod();
                    InfoOferta info = new InfoOferta(o.getImport1(),u.getAlias());
                    llOfertes.add(info);
                }
                tUI.afegirAlLog("Enviant la informacio relacionada amb les ofertes");
                writer.writeObject(llOfertes);
            }catch (GestioSubhastesException ex) {
                synchronized(mTxtLogZone){
                    tUI.afegirAlLog("Error: " + ex.getMessage() + " Cause:" + ex.getCause());
                }
            }
        }
    }
    // </editor-fold>
    
}
